package types

type NewChannelReq struct {
	Channelplan []struct {
		Frequency uint32 `json:"frequency"`
		Mindr     uint8  `json:"mindr"`
		Maxdr     uint8  `json:"maxdr"`
	} `json:"channelplan"`
}
