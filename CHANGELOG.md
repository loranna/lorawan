# CHANGELOG

## 0.11.157

- Add more tests
- Fix test related bugs

## 0.11.138

- Refactor some tests
- Add examples
- Fix message creation bugs

## 0.10.105

- Add tests
- Add CN779_787, EU433
- Improve documentation

## 0.9.86

- Improve documentation
- Add US902_928 region
- Various refactoring

## 0.8.58

- no changes

## 0.8.57

- Fix CFList handling

## 0.8.56

- Add tests
- Improve documentation
- Fix downlink parsing

## 0.7.48

- Add ChannelMaskCheck support

## 0.6.46

- Refactor EU863_870

## 0.6.44

- Add EU863_870 region support including CLI
- Improve CFList parsing

## 0.5.32

- Add Keys generation and CLI support

## 0.4.29

- Add tests
- Add more documentation
- Refactor `create message *` commands

## 0.3.22

- Further extend documentation
- Add parsing frame options
- Add tests

## 0.2.10

- Extend documentation

## 0.2.8

- Fix toolchain

## 0.1.2

- create uplinks, downlink, join request and accepts
- parse uplinks, downlink, join request and accepts
- CLI to access the API
