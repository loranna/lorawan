package version

//go:generate stringer -type=LoRaWANVersion
//go:generate stringer -type=RegionVersion


type LoRaWANVersion byte

const (
	LoRaWANUnknown LoRaWANVersion = iota
	LoRaWANV1_0_0
	LoRaWANV1_0_1
)

func ParseLoRaWAN(v string) LoRaWANVersion {
	switch v {
	case "v1.0.0", "V1.0.0":
		return LoRaWANV1_0_0
	default:
		return LoRaWANUnknown
	}
}

type RegionVersion byte

const (
	RegionUnknown RegionVersion = iota
	RegionV1_0_0
	RegionV1_0_1
)

func ParseRegion(v string) RegionVersion {
	switch v {
	case "v1.0.0", "V1.0.0":
		return RegionV1_0_0
	default:
		return RegionUnknown
	}
}