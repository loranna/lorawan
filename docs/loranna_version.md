## loranna version

Show version number

### Synopsis

Show version number

```
loranna version [flags]
```

### Options

```
  -h, --help   help for version
```

### Options inherited from parent commands

```
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
```

### SEE ALSO

* [loranna](loranna.md)	 - Loranna is an implementation of LoRaWAN protocol and a CLI to create and parse LoRaWAN messages

###### Auto generated by spf13/cobra on 26-Jan-2020
