## loranna create mac newchannelans

Create a LoRaWAN newchannel mac command

### Synopsis

Create a LoRaWAN newchannel mac command

```
loranna create mac newchannelans datarate channelfrequency [flags]
```

### Options

```
  -h, --help   help for newchannelans
```

### Options inherited from parent commands

```
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
  -s, --split                  fopts are split for each mac command
```

### SEE ALSO

* [loranna create mac](loranna_create_mac.md)	 - Create a LoRaWAN MAC command

###### Auto generated by spf13/cobra on 26-Jan-2020
