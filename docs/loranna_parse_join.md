## loranna parse join

parse a join related LoRaWAN message

### Synopsis

parse a join related LoRaWAN message

### Options

```
  -h, --help   help for join
```

### Options inherited from parent commands

```
  -b, --base64                 message is interpreted as base64 string
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
```

### SEE ALSO

* [loranna parse](loranna_parse.md)	 - Parse a LoRaWAN message
* [loranna parse join accept](loranna_parse_join_accept.md)	 - parses a join request LoRaWAN message
* [loranna parse join request](loranna_parse_join_request.md)	 - parses a join request LoRaWAN message

###### Auto generated by spf13/cobra on 26-Jan-2020
