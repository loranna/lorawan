## loranna parse

Parse a LoRaWAN message

### Synopsis

Parse a LoRaWAN message

### Options

```
  -b, --base64   message is interpreted as base64 string
  -h, --help     help for parse
```

### Options inherited from parent commands

```
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
```

### SEE ALSO

* [loranna](loranna.md)	 - Loranna is an implementation of LoRaWAN protocol and a CLI to create and parse LoRaWAN messages
* [loranna parse downlink](loranna_parse_downlink.md)	 - parse an downlink LoRaWAN message
* [loranna parse join](loranna_parse_join.md)	 - parse a join related LoRaWAN message
* [loranna parse uplink](loranna_parse_uplink.md)	 - parse an uplink LoRaWAN message

###### Auto generated by spf13/cobra on 26-Jan-2020
