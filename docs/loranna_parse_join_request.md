## loranna parse join request

parses a join request LoRaWAN message

### Synopsis

parses a join request LoRaWAN message

```
loranna parse join request message appkey [flags]
```

### Examples

```
loranna parse join request 00121a000000007abe262d000000007abe0f5d44441A3A 7B50A7A791A5F116533A3B5050BBB829
```

### Options

```
  -h, --help   help for request
```

### Options inherited from parent commands

```
  -b, --base64                 message is interpreted as base64 string
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
```

### SEE ALSO

* [loranna parse join](loranna_parse_join.md)	 - parse a join related LoRaWAN message

###### Auto generated by spf13/cobra on 26-Jan-2020
