## loranna region txpower

shows txpower

### Synopsis

shows txpower

```
loranna region txpower [id] [flags]
```

### Examples

```
loranna region txpower 0
```

### Options

```
  -h, --help   help for txpower
```

### Options inherited from parent commands

```
      --lorawan string         version of LoRaWAN protocol (default "v1.0.0")
      --region string          LoRaWAN region (default "EU863_870")
      --regionversion string   version of LoRaWAN region (default "v1.0.0")
```

### SEE ALSO

* [loranna region](loranna_region.md)	 - region related features

###### Auto generated by spf13/cobra on 26-Jan-2020
