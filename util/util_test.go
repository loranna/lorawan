package util

import (
	"reflect"
	"testing"
)

func TestBytereverse(t *testing.T) {
	type args struct {
		s []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{{
		"default",
		args{s: []byte{0x33, 0x44}},
		[]byte{0x44, 0x33},
	},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Bytereverse(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Bytereverse() = %v, want %v", got, tt.want)
			}
		})
	}
}
