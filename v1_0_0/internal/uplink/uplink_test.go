package uplink

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

var port0 = byte(0x03)
var uplinks = []struct {
	PHYPayload, ePHYPayload                     []byte
	nwkskey, appskey                            [16]byte
	payload, epayload                           []byte
	devaddr, mic                                [4]byte
	mtype                                       v1_0_0.MType
	major                                       v1_0_0.Major
	adr, adrackreq, ack                         bool
	fcnt                                        uint16
	fopts                                       []byte
	port                                        *uint8
	str, estr, portstr, payloadstr, epayloadstr string
}{
	{
		[]byte{0x40, 0x69, 0x7d, 0x3a, 0x49, 0x00, 0x00, 0x00, 0x03, 0x33},
		[]byte{0x40, 0x69, 0x7d, 0x3a, 0x49, 0x00, 0x00, 0x00, 0x03, 0x36, 44, 191, 236, 152},
		[16]byte{0xB4, 0x01, 0x1F, 0x25, 0xFE, 0xE6, 0xAC, 0x11, 0x38, 0x25, 0xF2, 0x6D, 0x08, 0xD1, 0x8D, 0x64},
		[16]byte{0x3E, 0xD3, 0xE6, 0x7B, 0x64, 0x8F, 0x9C, 0x3A, 0x6E, 0x41, 0x5F, 0xA3, 0xA8, 0x21, 0x8E, 0x30},
		[]byte{0x33},
		[]byte{0x36},
		[4]byte{0x49, 0x3a, 0x7d, 0x69},
		[4]byte{44, 191, 236, 152},
		v1_0_0.TypeUnconfirmedDataUp,
		v1_0_0.MajorLoRaWANR1,
		false, false, false,
		0,
		nil,
		&port0,
		`Message Type: TypeUnconfirmedDataUp
Major: MajorLoRaWANR1
DevAddr: 493A7D69
ADR: false
ADRACKReq: false
ACK: false
FCnt: 0
FOpts: 
FOpts length: 0
Port: 3
FRMPayload: 33
binary representation non-encrypted: 40697D3A490000000333
base64 representation non-encrypted: QGl9OkkAAAADMw==`,
		`Message Type: TypeUnconfirmedDataUp
Major: MajorLoRaWANR1
DevAddr: 493A7D69
ADR: false
ADRACKReq: false
ACK: false
FCnt: 0
FOpts: 
FOpts length: 0
Port: 3
EFRMPayload: 36
Mic: 2CBFEC98
binary representation encrypted: 40697D3A4900000003362CBFEC98
base64 representation encrypted: QGl9OkkAAAADNiy/7Jg=`,
		"3",
		"33",
		"36",
	},
	{
		[]byte{0x40, 0x69, 0x7d, 0x3a, 0x49, 0x00, 0x00, 0x00},
		[]byte{0x40, 0x69, 0x7d, 0x3a, 0x49, 0x00, 0x00, 0x00, 159, 156, 205, 124},
		[16]byte{0xB4, 0x01, 0x1F, 0x25, 0xFE, 0xE6, 0xAC, 0x11, 0x38, 0x25, 0xF2, 0x6D, 0x08, 0xD1, 0x8D, 0x64},
		[16]byte{0x3E, 0xD3, 0xE6, 0x7B, 0x64, 0x8F, 0x9C, 0x3A, 0x6E, 0x41, 0x5F, 0xA3, 0xA8, 0x21, 0x8E, 0x30},
		nil,
		nil,
		[4]byte{0x49, 0x3a, 0x7d, 0x69},
		[4]byte{159, 156, 205, 124},
		v1_0_0.TypeUnconfirmedDataUp,
		v1_0_0.MajorLoRaWANR1,
		false, false, false,
		0,
		nil,
		nil,
		`Message Type: TypeUnconfirmedDataUp
Major: MajorLoRaWANR1
DevAddr: 493A7D69
ADR: false
ADRACKReq: false
ACK: false
FCnt: 0
FOpts: 
FOpts length: 0
Port: none
FRMPayload: none
binary representation non-encrypted: 40697D3A49000000
base64 representation non-encrypted: QGl9OkkAAAA=`,
		`Message Type: TypeUnconfirmedDataUp
Major: MajorLoRaWANR1
DevAddr: 493A7D69
ADR: false
ADRACKReq: false
ACK: false
FCnt: 0
FOpts: 
FOpts length: 0
Port: none
EFRMPayload: none
Mic: 9F9CCD7C
binary representation encrypted: 40697D3A490000009F9CCD7C
base64 representation encrypted: QGl9OkkAAACfnM18`,
		"none",
		"none",
		"none",
	},
}

func TestUplink_Mtype(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want v1_0_0.MType
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.mtype
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Mtype(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Mtype() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Major(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want v1_0_0.Major
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = v1_0_0.MajorLoRaWANR1
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Major(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Major() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Devaddr(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want [4]byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.devaddr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Devaddr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Devaddr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Adr(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.adr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Adr(); got != tt.want {
				t.Errorf("Uplink.Adr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_AdrAckreq(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.adrackreq
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.AdrAckreq(); got != tt.want {
				t.Errorf("Uplink.AdrAckreq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Ack(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.ack
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Ack(); got != tt.want {
				t.Errorf("Uplink.Ack() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_FOptsLen(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = byte(len(up.fopts))
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.FOptsLen(); got != tt.want {
				t.Errorf("Uplink.FOptsLen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Fcnt(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want uint16
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.fcnt
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Fcnt(); got != tt.want {
				t.Errorf("Uplink.Fcnt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Fopts(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.fopts
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Fopts(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Fopts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Fport(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want *uint8
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.port
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.up.Fport() == nil {
				if got := tt.up.Fport(); got != tt.want {
					t.Errorf("Uplink.Fport() = %v, want %v", got, tt.want)
				}
			} else {
				if got := *tt.up.Fport(); got != *tt.want {
					t.Errorf("Uplink.Fport() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestUplink_Frmpayload(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.payload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Frmpayload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Frmpayload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_Encrypt(t *testing.T) {
	type args struct {
		nwkskey        [16]byte
		appskey        [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		up      Uplink
		args    args
		want    []byte
		wantErr bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].args = args{up.nwkskey, up.appskey, 0, 0}
		tests[i].want = up.ePHYPayload
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.up.Encrypt(tt.args.nwkskey, tt.args.appskey, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Uplink.Encrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Bytes(), tt.want) {
				t.Errorf("Uplink.Encrypt() = %v, want %v", got.Bytes(), tt.want)
			}
		})
	}
}

func TestUplink_Bytes(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.PHYPayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_String(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.str
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strings.TrimSpace(tt.up.String()); got != tt.want {
				t.Errorf("Uplink.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_portString(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.portstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.portString(); got != tt.want {
				t.Errorf("Uplink.portString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplink_frmpayloadString(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplink
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplink(up.PHYPayload)
		tests[i].want = up.payloadstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.frmpayloadString(); got != tt.want {
				t.Errorf("Uplink.frmpayloadString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Decrypt(t *testing.T) {
	type args struct {
		nwkskey        [16]byte
		appskey        [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		up      Uplinke
		args    args
		want    []byte
		wantErr bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].args = args{up.nwkskey, up.appskey, 0, 0}
		tests[i].want = up.payload
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.up.Decrypt(tt.args.nwkskey, tt.args.appskey, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Uplinke.Decrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Frmpayload(), tt.want) {
				t.Errorf("Uplinke.Decrypt() = %v, want %v", got.Frmpayload(), tt.want)
			}
		})
	}
}

func TestUplinke_FOptsLen(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.PHYPayload)
		tests[i].want = byte(len(up.fopts))
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.FOptsLen(); got != tt.want {
				t.Errorf("Uplinke.FOptsLen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Bytes(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.PHYPayload)
		tests[i].want = up.PHYPayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Efrmpayload(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.epayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Efrmpayload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Efrmpayload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_MIC(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want [4]byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.mic
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.MIC(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.MIC() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_MICValid(t *testing.T) {
	type args struct {
		key            [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		up      Uplinke
		args    args
		want    bool
		wantErr bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].args = args{up.nwkskey, 0, 0}
		tests[i].want = true
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.up.MICValid(tt.args.key, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Uplinke.MICValid() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Uplinke.MICValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Mtype(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want v1_0_0.MType
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.mtype
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Mtype(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Mtype() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Major(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want v1_0_0.Major
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.major
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Major(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Major() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Devaddr(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want [4]byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.devaddr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Devaddr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Devaddr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Adr(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.adr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Adr(); got != tt.want {
				t.Errorf("Uplinke.Adr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_AdrAckreq(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.adrackreq
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.AdrAckreq(); got != tt.want {
				t.Errorf("Uplinke.AdrAckreq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Ack(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.ack
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Ack(); got != tt.want {
				t.Errorf("Uplinke.Ack() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Fcnt(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want uint16
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.fcnt
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Fcnt(); got != tt.want {
				t.Errorf("Uplinke.Fcnt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Fopts(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want []byte
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.fopts
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.Fopts(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplinke.Fopts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_Fport(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want *uint8
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.port
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.up.Fport() == nil {
				if got := tt.up.Fport(); got != tt.want {
					t.Errorf("Uplink.Fport() = %v, want %v", got, tt.want)
				}
			} else {
				if got := *tt.up.Fport(); got != *tt.want {
					t.Errorf("Uplink.Fport() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestUplinke_String(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.estr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strings.TrimSpace(tt.up.String()); got != tt.want {
				t.Errorf("Uplinke.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_portString(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.portstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.portString(); got != tt.want {
				t.Errorf("Uplinke.portString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUplinke_efrmpayloadString(t *testing.T) {
	tests := make([]struct {
		name string
		up   Uplinke
		want string
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].up = Uplinke(up.ePHYPayload)
		tests[i].want = up.epayloadstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.up.efrmpayloadString(); got != tt.want {
				t.Errorf("Uplinke.efrmpayloadString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateUplinkMic(t *testing.T) {
	type args struct {
		message        []byte
		key            [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		args    args
		want    [4]byte
		wantErr bool
	}, len(uplinks))
	for i, up := range uplinks {
		tests[i].args = args{up.ePHYPayload[:len(up.ePHYPayload)-4], up.nwkskey, 0, 0}
		tests[i].want = up.mic
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := calculateUplinkMic(tt.args.message, tt.args.key, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("calculateUplinkMic() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateUplinkMic() = %v, want %v", got, tt.want)
			}
		})
	}
}
