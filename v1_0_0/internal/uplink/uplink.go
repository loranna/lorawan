package uplink

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/jacobsa/crypto/cmac"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type Uplink []byte

func (up Uplink) Mtype() v1_0_0.MType { return v1_0_0.MType((up[0] >> 5) & 0x07) }
func (up Uplink) Major() v1_0_0.Major { return v1_0_0.Major(up[0] & 0x0f) }
func (up Uplink) Devaddr() [4]byte {
	var devaddr [4]byte
	copy(devaddr[:], util.Bytereverse(up[1:5]))
	return devaddr
}
func (up Uplink) Adr() bool       { return (up[5] & 0x80) == 0x80 }
func (up Uplink) AdrAckreq() bool { return (up[5] & 0x40) == 0x40 }
func (up Uplink) Ack() bool       { return (up[5] & 0x20) == 0x20 }
func (up Uplink) FOptsLen() byte  { return up[5] & 0x0f }
func (up Uplink) Fcnt() uint16    { return binary.LittleEndian.Uint16(up[6:8]) }
func (up Uplink) Fopts() []byte {
	foptslen := up.FOptsLen()
	if foptslen == 0 {
		return nil
	}
	fopts := make([]byte, foptslen)
	copy(fopts, up[8:])
	return fopts
}
func (up Uplink) Fport() *uint8 {
	foptslen := up.FOptsLen()
	if byte(len(up))-foptslen > 8 {
		b := up[8+foptslen]
		return &b
	}
	return nil
}
func (up Uplink) Frmpayload() []byte {
	foptslen := up.FOptsLen()
	if byte(len(up))-foptslen > 8 {
		frmlen := byte(len(up[9+foptslen:]))
		b := make([]byte, frmlen)
		copy(b, up[9+foptslen:9+foptslen+frmlen])
		return b
	}
	return nil
}
func (up Uplink) Encrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (v1_0_0.UplinkEncrypted, error) {
	if (up.Fport() == nil && up.Frmpayload() != nil) ||
		(up.Fport() != nil && up.Frmpayload() == nil) {
		return nil, fmt.Errorf("both Fport and Frmpayload must have value or both must be empty")
	}
	if up.Fport() == nil {
		upp := make([]byte, len(up)+4)
		copy(upp, up)
		mic, err := calculateUplinkMic(upp[:len(upp)-4], nwkskey, MSBofcnt, MSBminus1ofcnt)
		if err != nil {
			return nil, err
		}
		copy(upp[len(up):], mic[:])
		return Uplinke(upp), nil
	}
	frmpayload := up.Frmpayload()
	B := []byte{
		0x01,
		0x00, 0x00, 0x00, 0x00,
		0x00,
		up[1], up[2], up[3], up[4],
		up[6], up[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		0}
	var cr cipher.Block
	if up.Fport() != nil {
		if *up.Fport() == 0 {
			cr, _ = aes.NewCipher(nwkskey[:])
		} else {
			cr, _ = aes.NewCipher(appskey[:])
		}
	} else {
		return nil, fmt.Errorf("Fport must be a number but nil")
	}
	size := len(frmpayload)
	r := make([]byte, size)
	bufferIndex := 0
	ctr := 1
	S := make([]byte, 16)

	for size >= 16 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1
		cr.Encrypt(S, B)

		for i := 0; i < 16; i++ {
			r[bufferIndex+i] = frmpayload[bufferIndex+i] ^ S[i]
		}
		size -= 16
		bufferIndex += 16
	}
	if size > 0 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1

		cr.Encrypt(S, B)
		for i := 0; i < size; i++ {
			r[bufferIndex+i] = frmpayload[bufferIndex+i] ^ S[i]
		}
	}
	upp := make([]byte, len(up)+4)
	copy(upp, up)
	foptslen := up.FOptsLen()
	copy(upp[9+foptslen:], r)
	frmpayloadlen := byte(len(frmpayload))
	mic, err := calculateUplinkMic(upp[:len(upp)-4], nwkskey, MSBofcnt, MSBminus1ofcnt)
	if err != nil {
		return nil, err
	}
	copy(upp[9+foptslen+frmpayloadlen:], mic[:])
	return Uplinke(upp), nil
}

func (up Uplink) Bytes() []byte {
	data := make([]byte, len(up))
	copy(data, up[:])
	return data
}
func (up Uplink) String() string {
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
DevAddr: %X
ADR: %t
ADRACKReq: %t
ACK: %t
FCnt: %d
FOpts: %X
FOpts length: %d
Port: %s
FRMPayload: %s
binary representation non-encrypted: %X
base64 representation non-encrypted: %s
`,
		up.Mtype(), up.Major(),
		up.Devaddr(),
		up.Adr(), up.AdrAckreq(), up.Ack(),
		up.Fcnt(),
		up.Fopts(),
		len(up.Fopts()),
		up.portString(),
		up.frmpayloadString(),
		up.Bytes(),
		base64.StdEncoding.EncodeToString(up.Bytes()))
}

func (up Uplink) portString() string {
	if up.Fport() == nil {
		return "none"
	}
	return fmt.Sprintf("%d", *up.Fport())
}

func (up Uplink) frmpayloadString() string {
	if up.Frmpayload() == nil {
		return "none"
	}
	return strings.ToUpper(hex.EncodeToString(up.Frmpayload()))
}

type Uplinke []byte

func (up Uplinke) Decrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (v1_0_0.Uplink, error) {
	if up.Efrmpayload() != nil && up.Fport() == nil {
		return nil, fmt.Errorf("missing fport")
	}
	if up.Fport() == nil {
		upp := make([]byte, len(up)-4)
		copy(upp, up)
		return Uplink(upp), nil
	}
	efrmpayload := up.Efrmpayload()
	B := []byte{
		0x01,
		0x00, 0x00, 0x00, 0x00,
		0x00,
		up[1], up[2], up[3], up[4],
		up[6], up[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		0}
	var cr cipher.Block
	var err error
	if *up.Fport() == 0 {
		cr, err = aes.NewCipher(nwkskey[:])
		if err != nil {
			return nil, err
		}
	} else {
		cr, err = aes.NewCipher(appskey[:])
		if err != nil {
			return nil, err
		}
	}
	size := len(efrmpayload)
	r := make([]byte, size)
	bufferIndex := 0
	ctr := 1
	S := make([]byte, 16)

	for size >= 16 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1
		cr.Encrypt(S, B)

		for i := 0; i < 16; i++ {
			r[bufferIndex+i] = efrmpayload[bufferIndex+i] ^ S[i]
		}
		size -= 16
		bufferIndex += 16
	}
	if size > 0 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1

		cr.Encrypt(S, B)
		for i := 0; i < size; i++ {
			r[bufferIndex+i] = efrmpayload[bufferIndex+i] ^ S[i]
		}
	}
	upp := make([]byte, len(up)-4)
	foptslen := up.FOptsLen()
	copy(upp[:9+foptslen], up)
	copy(upp[9+foptslen:], r)
	return Uplink(upp), nil
}
func (up Uplinke) FOptsLen() byte { return up[5] & 0x0f }
func (up Uplinke) Bytes() []byte {
	data := make([]byte, len(up))
	copy(data, up[:])
	return data
}
func (up Uplinke) Efrmpayload() []byte {
	foptslen := up.FOptsLen()
	if byte(len(up))-foptslen > 12 {
		frmlen := byte(len(up[9+foptslen:])) - 4
		b := make([]byte, frmlen)
		copy(b, up[9+foptslen:9+foptslen+frmlen])
		return b
	}
	return nil
}
func (up Uplinke) MIC() [4]byte {
	var r [4]byte
	copy(r[:], up[len(up)-4:])
	return r
}
func (up Uplinke) MICValid(key [16]byte, MSBofcnt, MSBminus1ofcnt byte) (bool, error) {
	mic, err := calculateUplinkMic(up[:len(up)-4], key, MSBofcnt, MSBminus1ofcnt)
	if err != nil {
		return false, err
	}
	l := len(up)
	for i := 0; i < 4; i++ {
		if mic[i] != up[l-4+i] {
			return false, nil
		}
	}
	return true, nil
}

func (up Uplinke) Mtype() v1_0_0.MType { return v1_0_0.MType((up[0] >> 5) & 0x07) }
func (up Uplinke) Major() v1_0_0.Major { return v1_0_0.Major(up[0] & 0x0f) }
func (up Uplinke) Devaddr() [4]byte {
	var devaddr [4]byte
	copy(devaddr[:], util.Bytereverse(up[1:5]))
	return devaddr
}
func (up Uplinke) Adr() bool       { return (up[5] & 0x80) == 0x80 }
func (up Uplinke) AdrAckreq() bool { return (up[5] & 0x40) == 0x40 }
func (up Uplinke) Ack() bool       { return (up[5] & 0x20) == 0x20 }

func (up Uplinke) Fcnt() uint16 { return binary.LittleEndian.Uint16(up[6:8]) }
func (up Uplinke) Fopts() []byte {
	foptslen := up.FOptsLen()
	if foptslen == 0 {
		return nil
	}
	fopts := make([]byte, foptslen)
	copy(fopts, up[8:])
	return fopts
}
func (up Uplinke) Fport() *uint8 {
	foptslen := up.FOptsLen()
	if byte(len(up))-foptslen > 12 {
		b := up[8+foptslen]
		return &b
	}
	return nil
}

func (up Uplinke) String() string {
	b := up.Bytes()
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
DevAddr: %X
ADR: %t
ADRACKReq: %t
ACK: %t
FCnt: %d
FOpts: %X
FOpts length: %d
Port: %s
EFRMPayload: %s
Mic: %X
binary representation encrypted: %X
base64 representation encrypted: %s
`,
		up.Mtype(), up.Major(),
		up.Devaddr(),
		up.Adr(), up.AdrAckreq(), up.Ack(),
		up.Fcnt(),
		up.Fopts(),
		len(up.Fopts()),
		up.portString(),
		up.efrmpayloadString(),
		up.MIC(),
		b,
		base64.StdEncoding.EncodeToString(b))
}

func (up Uplinke) portString() string {
	if up.Fport() == nil {
		return "none"
	}
	return fmt.Sprintf("%d", *up.Fport())
}

func (up Uplinke) efrmpayloadString() string {
	if up.Efrmpayload() == nil {
		return "none"
	}
	return strings.ToUpper(hex.EncodeToString(up.Efrmpayload()))
}

func calculateUplinkMic(message []byte, key [16]byte, MSBofcnt, MSBminus1ofcnt byte) ([4]byte, error) {
	B := []byte{
		0x49,
		0x00, 0x00, 0x00, 0x00,
		0x00,
		message[1], message[2], message[3], message[4],
		message[6], message[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		uint8(len(message))}

	B = append(B, message...)

	result, err := cmac.New(key[:])
	if err != nil {
		return [4]byte{}, err
	}
	_, err = result.Write(B[:])
	if err != nil {
		return [4]byte{}, err
	}
	rr := result.Sum([]byte{})
	var mic [4]byte
	copy(mic[:], rr[:4])
	return mic, nil
}
