package downlink

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

var port0 = byte(0x42)
var downlinks = []struct {
	PHYPayload, ePHYPayload                     []byte
	nwkskey, appskey                            [16]byte
	payload, epayload                           []byte
	devaddr, mic                                [4]byte
	mtype                                       v1_0_0.MType
	major                                       v1_0_0.Major
	adr, adrackreq, ack, fpending               bool
	fcnt                                        uint16
	fopts                                       []byte
	port                                        *uint8
	str, estr, portstr, payloadstr, epayloadstr string
}{
	{
		[]byte{0x60, 0x02, 0x43, 0x6f, 0x48, 0x00, 0x01, 0x00, 0x42, 0xaa, 0xaa},
		[]byte{0x60, 0x02, 0x43, 0x6f, 0x48, 0x00, 0x01, 0x00, 0x42, 0x4f, 0x66, 0xa3, 0x3d, 0x62, 0xa5},
		[16]byte{0xB5, 0x99, 0xE2, 0x93, 0x82, 0x37, 0xB2, 0x3E, 0x85, 0x8E, 0x6E, 0x7D, 0x0B, 0xB6, 0x15, 0x38},
		[16]byte{0x9A, 0x9D, 0xFC, 0x50, 0xEE, 0x34, 0x4E, 0x76, 0xD1, 0x25, 0x12, 0x2F, 0xF8, 0xD1, 0x34, 0x6E},
		[]byte{0xaa, 0xaa},
		[]byte{0x4f, 0x66},
		[4]byte{0x48, 0x6f, 0x43, 0x02},
		[4]byte{0xa3, 0x3d, 0x62, 0xa5},
		v1_0_0.TypeUnconfirmedDataDown,
		v1_0_0.MajorLoRaWANR1,
		false, false, false, false,
		1,
		nil,
		&port0,
		`Message Type: TypeUnconfirmedDataDown
Major: MajorLoRaWANR1
DevAddr: 486F4302
ADR: false
ADRACKReq: false
ACK: false
FPending: false
FCnt: 1
FOpts: 
FOpts length: 0
Port: 66
FRMPayload: AAAA
binary representation non-encrypted: 6002436F4800010042AAAA
base64 representation non-encrypted: YAJDb0gAAQBCqqo=`,
		`Message Type: TypeUnconfirmedDataDown
Major: MajorLoRaWANR1
DevAddr: 486F4302
ADR: false
ADRACKReq: false
ACK: false
FCnt: 1
FOpts: 
FOpts length: 0
Port: 66
EFRMPayload: 4F66
Mic: A33D62A5
binary representation encrypted: 6002436F48000100424F66A33D62A5
base64 representation encrypted: YAJDb0gAAQBCT2ajPWKl`,
		"66",
		"AAAA",
		"4F66",
	},
	{
		[]byte{0x60, 0xfc, 0x2a, 0xf5, 0x49, 0x20, 0x02, 0x00},
		[]byte{0x60, 0xfc, 0x2a, 0xf5, 0x49, 0x20, 0x02, 0x00, 0x3b, 0xe8, 0x4f, 0xf1},
		[16]byte{0x8F, 0x5F, 0xA6, 0xA1, 0xE0, 0x48, 0x8F, 0x77, 0x3E, 0x92, 0xDE, 0x2F, 0x36, 0xD4, 0x58, 0xE7},
		[16]byte{0x24, 0xF9, 0x31, 0x6A, 0xF7, 0x43, 0x66, 0x34, 0x6C, 0x8E, 0x8C, 0x4B, 0x42, 0xAE, 0x38, 0xF4},
		nil,
		nil,
		[4]byte{0x49, 0xf5, 0x2a, 0xfc},
		[4]byte{0x3b, 0xe8, 0x4f, 0xf1},
		v1_0_0.TypeUnconfirmedDataDown,
		v1_0_0.MajorLoRaWANR1,
		false, false, true, false,
		2,
		nil,
		nil,
		`Message Type: TypeUnconfirmedDataDown
Major: MajorLoRaWANR1
DevAddr: 49F52AFC
ADR: false
ADRACKReq: false
ACK: true
FPending: false
FCnt: 2
FOpts: 
FOpts length: 0
Port: none
FRMPayload: none
binary representation non-encrypted: 60FC2AF549200200
base64 representation non-encrypted: YPwq9UkgAgA=`,
		`Message Type: TypeUnconfirmedDataDown
Major: MajorLoRaWANR1
DevAddr: 49F52AFC
ADR: false
ADRACKReq: false
ACK: true
FCnt: 2
FOpts: 
FOpts length: 0
Port: none
EFRMPayload: none
Mic: 3BE84FF1
binary representation encrypted: 60FC2AF5492002003BE84FF1
base64 representation encrypted: YPwq9UkgAgA76E/x`,
		"none",
		"none",
		"none",
	},
}

func TestDownlink_Mtype(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want v1_0_0.MType
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.mtype
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Mtype(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Mtype() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Major(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want v1_0_0.Major
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.major
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Major(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Major() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Devaddr(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want [4]byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.devaddr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Devaddr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Devaddr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Adr(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.adr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Adr(); got != tt.want {
				t.Errorf("Downlink.Adr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_AdrAckreq(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.adrackreq
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.AdrAckreq(); got != tt.want {
				t.Errorf("Downlink.AdrAckreq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Ack(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.ack
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Ack(); got != tt.want {
				t.Errorf("Downlink.Ack() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_FPending(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.fpending
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.FPending(); got != tt.want {
				t.Errorf("Downlink.FPending() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_FOptsLen(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = byte(len(dl.fopts))
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.FOptsLen(); got != tt.want {
				t.Errorf("Downlink.FOptsLen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Fcnt(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want uint16
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.fcnt
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Fcnt(); got != tt.want {
				t.Errorf("Downlink.Fcnt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Fopts(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.fopts
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Fopts(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Fopts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Fport(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want *uint8
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.port
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.down.Fport() == nil {
				if got := tt.down.Fport(); got != tt.want {
					t.Errorf("Downlink.Fport() = %v, want %v", got, tt.want)
				}
			} else {
				if got := *tt.down.Fport(); got != *tt.want {
					t.Errorf("Downlink.Fport() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestDownlink_Frmpayload(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.payload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Frmpayload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Frmpayload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Encrypt(t *testing.T) {
	type args struct {
		nwkskey        [16]byte
		appskey        [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		down    Downlink
		args    args
		want    v1_0_0.DownlinkEncrypted
		wantErr bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].args = args{
			dl.nwkskey,
			dl.appskey,
			0, 0,
		}
		tests[i].want = Downlinke(downlinks[i].ePHYPayload)
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.down.Encrypt(tt.args.nwkskey, tt.args.appskey, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Downlink.Encrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Encrypt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_Bytes(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.PHYPayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_String(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.str
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strings.TrimSpace(tt.down.String()); got != strings.TrimSpace(tt.want) {
				t.Errorf("Downlink.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_portString(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.portstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.portString(); got != tt.want {
				t.Errorf("Downlink.portString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlink_frmpayloadString(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlink
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlink(dl.PHYPayload)
		tests[i].want = dl.payloadstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.frmpayloadString(); got != tt.want {
				t.Errorf("Downlink.frmpayloadString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Decrypt(t *testing.T) {
	type args struct {
		nwkskey        [16]byte
		appskey        [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		down    Downlinke
		args    args
		want    []byte
		wantErr bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].args = args{dl.nwkskey, dl.appskey, 0, 0}
		tests[i].want = Downlink(dl.payload)
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.down.Decrypt(tt.args.nwkskey, tt.args.appskey, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Downlinke.Decrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Frmpayload(), tt.want) {
				t.Errorf("Downlinke.Decrypt() = %v, want %v", got.Frmpayload(), tt.want)
			}
		})
	}
}

func TestDownlinke_FOptsLen(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = 0
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.FOptsLen(); got != tt.want {
				t.Errorf("Downlinke.FOptsLen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Bytes(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.ePHYPayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Efrmpayload(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.epayload
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Efrmpayload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Efrmpayload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_MIC(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want [4]byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.mic
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.MIC(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.MIC() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_MICValid(t *testing.T) {
	type args struct {
		key            [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		down    Downlinke
		args    args
		want    bool
		wantErr bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].args = args{key: dl.nwkskey, MSBofcnt: 0, MSBminus1ofcnt: 0}
		tests[i].want = true
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.down.MICValid(tt.args.key, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Downlinke.MICValid() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Downlinke.MICValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Mtype(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want v1_0_0.MType
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.mtype
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Mtype(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Mtype() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Major(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want v1_0_0.Major
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = v1_0_0.MajorLoRaWANR1
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Major(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Major() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Devaddr(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want [4]byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.devaddr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Devaddr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Devaddr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Adr(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.adr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Adr(); got != tt.want {
				t.Errorf("Downlinke.Adr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_AdrAckreq(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.adrackreq
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.AdrAckreq(); got != tt.want {
				t.Errorf("Downlinke.AdrAckreq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Ack(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.ack
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Ack(); got != tt.want {
				t.Errorf("Downlinke.Ack() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_FPending(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.fpending
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.FPending(); got != tt.want {
				t.Errorf("Downlinke.FPending() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Fcnt(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want uint16
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.fcnt
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Fcnt(); got != tt.want {
				t.Errorf("Downlinke.Fcnt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Fopts(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want []byte
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.fopts
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.Fopts(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlinke.Fopts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_Fport(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want *uint8
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.port
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.down.Fport() != nil && tt.want != nil {
				if got := *tt.down.Fport(); got != *tt.want {
					t.Errorf("Downlinke.Fport() = %v, want %v", got, tt.want)
				}
			} else if tt.down.Fport() == nil && tt.want == nil {

			} else {
				t.Errorf("Downlinke.Fport() = %v, want %v", tt.down.Fport(), tt.want)
			}
		})
	}
}

func TestDownlinke_String(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.estr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strings.TrimSpace(tt.down.String()); got != strings.TrimSpace(tt.want) {
				t.Errorf("Downlinke.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_portString(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.portstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.portString(); got != tt.want {
				t.Errorf("Downlinke.portString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinke_efrmpayloadString(t *testing.T) {
	tests := make([]struct {
		name string
		down Downlinke
		want string
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].down = Downlinke(dl.ePHYPayload)
		tests[i].want = dl.epayloadstr
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.down.efrmpayloadString(); got != tt.want {
				t.Errorf("Downlinke.efrmpayloadString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_calculateDownlinkMic(t *testing.T) {
	type args struct {
		message        []byte
		key            [16]byte
		MSBofcnt       byte
		MSBminus1ofcnt byte
	}
	tests := make([]struct {
		name    string
		args    args
		want    [4]byte
		wantErr bool
	}, len(downlinks))
	for i, dl := range downlinks {
		tests[i].args = args{dl.ePHYPayload[:len(dl.ePHYPayload)-4], dl.nwkskey, 0, 0}
		tests[i].want = dl.mic
		tests[i].wantErr = false
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := calculateDownlinkMic(tt.args.message, tt.args.key, tt.args.MSBofcnt, tt.args.MSBminus1ofcnt)
			if (err != nil) != tt.wantErr {
				t.Errorf("calculateDownlinkMic() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("calculateDownlinkMic() = %v, want %v", got, tt.want)
			}
		})
	}
}
