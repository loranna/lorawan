package downlink

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/jacobsa/crypto/cmac"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type Downlink []byte

func (down Downlink) Mtype() v1_0_0.MType { return v1_0_0.MType((down[0] >> 5) & 0x07) }
func (down Downlink) Major() v1_0_0.Major { return v1_0_0.Major(down[0] & 0x0f) }
func (down Downlink) Devaddr() [4]byte {
	var devaddr [4]byte
	copy(devaddr[:], util.Bytereverse(down[1:5]))
	return devaddr
}
func (down Downlink) Adr() bool       { return (down[5] & 0x80) == 0x80 }
func (down Downlink) AdrAckreq() bool { return (down[5] & 0x40) == 0x40 }
func (down Downlink) Ack() bool       { return (down[5] & 0x20) == 0x20 }
func (down Downlink) FPending() bool  { return (down[5] & 0x10) == 0x10 }
func (down Downlink) FOptsLen() byte  { return down[5] & 0x0f }
func (down Downlink) Fcnt() uint16    { return binary.LittleEndian.Uint16(down[6:8]) }
func (down Downlink) Fopts() []byte {
	foptslen := down.FOptsLen()
	if foptslen == 0 {
		return nil
	}
	fopts := make([]byte, foptslen)
	copy(fopts, down[8:])
	return fopts
}
func (down Downlink) Fport() *uint8 {
	foptslen := down.FOptsLen()
	if byte(len(down))-foptslen > 8 {
		b := down[8+foptslen]
		return &b
	}
	return nil
}
func (down Downlink) Frmpayload() []byte {
	foptslen := down.FOptsLen()
	if byte(len(down))-foptslen > 9 {
		frmlen := byte(len(down[9+foptslen:]))
		b := make([]byte, frmlen)
		copy(b, down[9+foptslen:9+foptslen+frmlen])
		return b
	}
	return nil
}
func (down Downlink) Encrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (v1_0_0.DownlinkEncrypted, error) {
	if (down.Fport() == nil && down.Frmpayload() != nil) ||
		(down.Fport() != nil && down.Frmpayload() == nil) {
		return nil, fmt.Errorf("missing fport")
	}
	if down.Fport() == nil {
		downp := make([]byte, len(down)+4)
		copy(downp, down)
		mic, err := calculateDownlinkMic(downp[:len(downp)-4], nwkskey, MSBofcnt, MSBminus1ofcnt)
		if err != nil {
			return nil, err
		}
		copy(downp[len(down):], mic[:])
		return Downlinke(downp), nil
	}
	frmpayload := down.Frmpayload()
	B := []byte{
		0x01,
		0x00, 0x00, 0x00, 0x00,
		0x01,
		down[1], down[2], down[3], down[4],
		down[6], down[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		uint8(len(down))}
	var cr cipher.Block
	if *down.Fport() == 0 {
		cr, _ = aes.NewCipher(nwkskey[:])
	} else {
		cr, _ = aes.NewCipher(appskey[:])
	}
	size := len(frmpayload)
	encfrmpayload := make([]byte, size)
	bufferIndex := 0
	ctr := 1
	S := make([]byte, 16)

	for size >= 16 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1
		cr.Encrypt(S, B)

		for i := 0; i < 16; i++ {
			encfrmpayload[bufferIndex+i] = frmpayload[bufferIndex+i] ^ S[i]
		}
		size -= 16
		bufferIndex += 16
	}
	if size > 0 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1

		cr.Encrypt(S, B)
		for i := 0; i < size; i++ {
			encfrmpayload[bufferIndex+i] = frmpayload[bufferIndex+i] ^ S[i]
		}
	}
	downp := make([]byte, len(down)+4)
	copy(downp, down)
	foptslen := down.FOptsLen()
	copy(downp[9+foptslen:], encfrmpayload)
	frmpayloadlen := byte(len(frmpayload))
	mic, err := calculateDownlinkMic(downp[:len(downp)-4], nwkskey, MSBofcnt, MSBminus1ofcnt)
	if err != nil {
		return nil, err
	}
	copy(downp[9+foptslen+frmpayloadlen:], mic[:])
	return Downlinke(downp), nil
}

func (down Downlink) Bytes() []byte {
	data := make([]byte, len(down))
	copy(data, down[:])
	return data
}
func (down Downlink) String() string {
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
DevAddr: %X
ADR: %t
ADRACKReq: %t
ACK: %t
FPending: %t
FCnt: %d
FOpts: %X
FOpts length: %d
Port: %s
FRMPayload: %s
binary representation non-encrypted: %X
base64 representation non-encrypted: %s
`,
		down.Mtype(), down.Major(),
		down.Devaddr(),
		down.Adr(), down.AdrAckreq(), down.Ack(), down.FPending(),
		down.Fcnt(),
		down.Fopts(),
		len(down.Fopts()),
		down.portString(),
		down.frmpayloadString(),
		down.Bytes(),
		base64.StdEncoding.EncodeToString(down.Bytes()))
}

func (down Downlink) portString() string {
	if down.Fport() == nil {
		return "none"
	}
	return fmt.Sprintf("%d", *down.Fport())
}

func (down Downlink) frmpayloadString() string {
	if down.Frmpayload() == nil {
		return "none"
	}
	return strings.ToUpper(hex.EncodeToString(down.Frmpayload()))
}

type Downlinke []byte

func (down Downlinke) Decrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (v1_0_0.Downlink, error) {
	if down.Fport() == nil {
		downp := make([]byte, len(down)-4)
		copy(downp, down)
		return Downlink(downp), nil
	}
	efrmpayload := down.Efrmpayload()
	B := []byte{
		0x01,
		0x00, 0x00, 0x00, 0x00,
		0x01,
		down[1], down[2], down[3], down[4],
		down[6], down[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		uint8(len(down))}
	var cr cipher.Block
	var err error
	if *down.Fport() == 0 {
		cr, err = aes.NewCipher(nwkskey[:])
		if err != nil {
			return nil, err
		}
	} else {
		cr, err = aes.NewCipher(appskey[:])
		if err != nil {
			return nil, err
		}
	}
	size := len(efrmpayload)
	r := make([]byte, size)
	bufferIndex := 0
	ctr := 1
	S := make([]byte, 16)

	for size >= 16 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1
		cr.Encrypt(S, B)

		for i := 0; i < 16; i++ {
			r[bufferIndex+i] = efrmpayload[bufferIndex+i] ^ S[i]
		}
		size -= 16
		bufferIndex += 16
	}
	if size > 0 {
		B[15] = byte(ctr & 0xFF)
		ctr += 1

		cr.Encrypt(S, B)
		for i := 0; i < size; i++ {
			r[bufferIndex+i] = efrmpayload[bufferIndex+i] ^ S[i]
		}
	}
	downp := make([]byte, len(down)-4)
	foptslen := down.FOptsLen()
	copy(downp[:9+foptslen], down)
	copy(downp[9+foptslen:], r)
	return Downlink(downp), nil
}
func (down Downlinke) FOptsLen() byte { return down[5] & 0x0f }
func (down Downlinke) Bytes() []byte {
	data := make([]byte, len(down))
	copy(data, down[:])
	return data
}
func (down Downlinke) Efrmpayload() []byte {
	foptslen := down.FOptsLen()
	if byte(len(down))-foptslen > 12 {
		frmlen := byte(len(down[9+foptslen:])) - 4
		b := make([]byte, frmlen)
		copy(b, down[9+foptslen:9+foptslen+frmlen])
		return b
	}
	return nil
}
func (down Downlinke) MIC() [4]byte {
	var r [4]byte
	copy(r[:], down[len(down)-4:])
	return r
}
func (down Downlinke) MICValid(key [16]byte, MSBofcnt, MSBminus1ofcnt byte) (bool, error) {
	mic, err := calculateDownlinkMic(down[:len(down)-4], key, MSBofcnt, MSBminus1ofcnt)
	if err != nil {
		return false, err
	}
	l := len(down)
	for i := 0; i < 4; i++ {
		if mic[i] != down[l-4+i] {
			return false, nil
		}
	}
	return true, nil
}

func (down Downlinke) Mtype() v1_0_0.MType { return v1_0_0.MType((down[0] >> 5) & 0x07) }
func (down Downlinke) Major() v1_0_0.Major { return v1_0_0.Major(down[0] & 0x0f) }
func (down Downlinke) Devaddr() [4]byte {
	var devaddr [4]byte
	copy(devaddr[:], util.Bytereverse(down[1:5]))
	return devaddr
}
func (down Downlinke) Adr() bool       { return (down[5] & 0x80) == 0x80 }
func (down Downlinke) AdrAckreq() bool { return (down[5] & 0x40) == 0x40 }
func (down Downlinke) Ack() bool       { return (down[5] & 0x20) == 0x20 }
func (down Downlinke) FPending() bool  { return (down[5] & 0x10) == 0x10 }

func (down Downlinke) Fcnt() uint16 { return binary.LittleEndian.Uint16(down[6:8]) }
func (down Downlinke) Fopts() []byte {
	foptslen := down.FOptsLen()
	if foptslen == 0 {
		return nil
	}
	fopts := make([]byte, foptslen)
	copy(fopts, down[8:])
	return fopts
}
func (down Downlinke) Fport() *uint8 {
	foptslen := down.FOptsLen()
	if byte(len(down))-foptslen > 12 {
		b := down[8+foptslen]
		return &b
	}
	return nil
}

func (down Downlinke) String() string {
	b := down.Bytes()
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
DevAddr: %X
ADR: %t
ADRACKReq: %t
ACK: %t
FCnt: %d
FOpts: %X
FOpts length: %d
Port: %s
EFRMPayload: %s
Mic: %X
binary representation encrypted: %X
base64 representation encrypted: %s
`,
		down.Mtype(), down.Major(),
		down.Devaddr(),
		down.Adr(), down.AdrAckreq(), down.Ack(),
		down.Fcnt(),
		down.Fopts(),
		len(down.Fopts()),
		down.portString(),
		down.efrmpayloadString(),
		down.MIC(),
		b,
		base64.StdEncoding.EncodeToString(b))
}

func (down Downlinke) portString() string {
	if down.Fport() == nil {
		return "none"
	}
	return fmt.Sprintf("%d", *down.Fport())
}

func (down Downlinke) efrmpayloadString() string {
	if down.Efrmpayload() == nil {
		return "none"
	}
	return strings.ToUpper(hex.EncodeToString(down.Efrmpayload()))
}

func calculateDownlinkMic(message []byte, key [16]byte, MSBofcnt, MSBminus1ofcnt byte) ([4]byte, error) {
	B := []byte{
		0x49,
		0x00, 0x00, 0x00, 0x00,
		0x01,
		message[1], message[2], message[3], message[4],
		message[6], message[7], MSBminus1ofcnt, MSBofcnt,
		0x00,
		uint8(len(message))}

	B = append(B, message...)

	result, err := cmac.New(key[:])
	if err != nil {
		return [4]byte{}, err
	}
	_, err = result.Write(B[:])
	if err != nil {
		return [4]byte{}, err
	}
	rr := result.Sum([]byte{})
	var mic [4]byte
	copy(mic[:], rr[:4])
	return mic, nil
}
