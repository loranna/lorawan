package v1_0_0

//JoinAccept represents a decrypted join accept message
type JoinAccept interface {
	Encrypt(key [16]byte) (JoinAcceptEncrypted, error)

	MType() MType
	Major() Major
	AppNonce() [3]byte
	NetID() [3]byte
	DevAddr() [4]byte
	RX1DRoffset() byte
	RX2DataRate() byte
	RxDelay() byte
	CFList() *[16]byte
	MIC() [4]byte
	Bytes() []byte
}

//JoinAcceptEncrypted represents a encrypted join accept message
type JoinAcceptEncrypted interface {
	Decrypt(key [16]byte) (JoinAccept, error)
	Bytes() []byte
}
