// Code generated by "stringer -type=MType"; DO NOT EDIT.

package v1_0_0

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[TypeJoinRequest-0]
	_ = x[TypeJoinAccept-1]
	_ = x[TypeUnconfirmedDataUp-2]
	_ = x[TypeUnconfirmedDataDown-3]
	_ = x[TypeConfirmedDataUp-4]
	_ = x[TypeConfirmedDataDown-5]
	_ = x[TypeProprietary-7]
}

const (
	_MType_name_0 = "TypeJoinRequestTypeJoinAcceptTypeUnconfirmedDataUpTypeUnconfirmedDataDownTypeConfirmedDataUpTypeConfirmedDataDown"
	_MType_name_1 = "TypeProprietary"
)

var (
	_MType_index_0 = [...]uint8{0, 15, 29, 50, 73, 92, 113}
)

func (i MType) String() string {
	switch {
	case 0 <= i && i <= 5:
		return _MType_name_0[_MType_index_0[i]:_MType_index_0[i+1]]
	case i == 7:
		return _MType_name_1
	default:
		return "MType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
}
