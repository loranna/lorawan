package v1_0_0

//JoinRequest represents a join request message
type JoinRequest interface {
	MType() MType
	Major() Major
	AppEUI() [8]byte
	DevEUI() [8]byte
	DevNonce() [2]byte
	MIC() [4]byte
	Bytes() [23]byte
}
