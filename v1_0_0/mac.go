package v1_0_0

import "sort"

//FOptID represents the ID of the Frame Option
type FOptID byte

const (
	//LinkCheckReqID is the id of the LinkCheckReq Frame Option
	LinkCheckReqID FOptID = 0x02
	//LinkCheckAnsID is the id of the LinkCheckAnsID Frame Option
	LinkCheckAnsID FOptID = 0x02
	//LinkADRReqID is the id of the LinkADRReqID Frame Option
	LinkADRReqID FOptID = 0x03
	//LinkADRAnsID is the id of the LinkADRAnsID Frame Option
	LinkADRAnsID FOptID = 0x03
	//DutyCycleReqID is the id of the DutyCycleReqID Frame Option
	DutyCycleReqID FOptID = 0x04
	//DutyCycleAnsID is the id of the DutyCycleAnsID Frame Option
	DutyCycleAnsID FOptID = 0x04
	//RXParamSetupReqID is the id of the RXParamSetupReqID Frame Option
	RXParamSetupReqID FOptID = 0x05
	//RXParamSetupAnsID is the id of the RXParamSetupAnsID Frame Option
	RXParamSetupAnsID FOptID = 0x05
	//DevStatusReqID is the id of the DevStatusReqID Frame Option
	DevStatusReqID FOptID = 0x06
	//DevStatusAnsID is the id of the DevStatusAnsID Frame Option
	DevStatusAnsID FOptID = 0x06
	//NewChannelReqID is the id of the NewChannelReqID Frame Option
	NewChannelReqID FOptID = 0x07
	//NewChannelAnsID is the id of the NewChannelAnsID Frame Option
	NewChannelAnsID FOptID = 0x07
	//RXTimingSetupReqID is the id of the RXTimingSetupReqID Frame Option
	RXTimingSetupReqID FOptID = 0x08
	//RXTimingSetupAnsID is the id of the RXTimingSetupAnsID Frame Option
	RXTimingSetupAnsID FOptID = 0x08
)

const (
	//LinkCheckReqLen is the length of the LinkCheckReq Frame Option
	LinkCheckReqLen = 1
	//LinkCheckAnsLen is the length of the LinkCheckAns Frame Option
	LinkCheckAnsLen = 3
	//LinkADRReqLen is the length of the LinkADRReq Frame Option
	LinkADRReqLen = 5
	//LinkADRAnsLen is the length of the LinkADRAns Frame Option
	LinkADRAnsLen = 2
	//DutyCycleReqLen is the length of the DutyCycleReq Frame Option
	DutyCycleReqLen = 2
	//DutyCycleAnsLen is the length of the DutyCycleAns Frame Option
	DutyCycleAnsLen = 1
	//RXParamSetupReqLen is the length of the RXParamSetupReq Frame Option
	RXParamSetupReqLen = 5
	//RXParamSetupAnsLen is the length of the RXParamSetupAns Frame Option
	RXParamSetupAnsLen = 2
	//DevStatusReqLen is the length of the DevStatusReq Frame Option
	DevStatusReqLen = 1
	//DevStatusAnsLen is the length of the DevStatusAns Frame Option
	DevStatusAnsLen = 3
	//NewChannelReqLen is the length of the NewChannelReq Frame Option
	NewChannelReqLen = 6
	//NewChannelAnsLen is the length of the NewChannelAns Frame Option
	NewChannelAnsLen = 2
	//RXTimingSetupReqLen is the length of the RXTimingSetupReq Frame Option
	RXTimingSetupReqLen = 2
	//RXTimingSetupAnsLen is the length of the RXTimingSetupAns Frame Option
	RXTimingSetupAnsLen = 1
)

//FrameOption represents a Frame Option
type FrameOption interface {
	ID() FOptID
	Payload() []byte
	Bytes() []byte
}

//LinkCheckReq represents a LinkCheckReq Frame Option
type LinkCheckReq interface {
	FrameOption
}

//LinkCheckAns represents a LinkCheckAns Frame Option
type LinkCheckAns interface {
	FrameOption
	Margin() byte
	GwCnt() byte
}

//LinkADRReq represents a LinkADRReq Frame Option
type LinkADRReq interface {
	FrameOption
	Datarate() byte
	TxPower() byte
	Chmask() [2]byte
	ChmaskCntl() byte
	Nbrep() byte
}

//LinkADRAns represents a LinkADRAns Frame Option
type LinkADRAns interface {
	FrameOption
	PowerACK() bool
	DataRateACK() bool
	ChannelMaskACK() bool
}

//DutyCycleReq represents a DutyCycleReq Frame Option
type DutyCycleReq interface {
	FrameOption
	MaxDCycle() uint8
}

//DutyCycleAns represents a DutyCycleAns Frame Option
type DutyCycleAns interface {
	FrameOption
}

//RXParamSetupReq represents a RXParamSetupReq Frame Option
type RXParamSetupReq interface {
	FrameOption
	RX1DRoffset() uint8
	RX2DataRate() uint8
	Frequency() uint32
}

//RXParamSetupAns represents a RXParamSetupAns Frame Option
type RXParamSetupAns interface {
	FrameOption
	RX1DRoffsetACK() bool
	RX2DataRateACK() bool
	ChannelACK() bool
}

//DevStatusReq represents a DevStatusReq Frame Option
type DevStatusReq interface {
	FrameOption
}

//DevStatusAns represents a DevStatusAns Frame Option
type DevStatusAns interface {
	FrameOption
	Battery() uint8
	Margin() uint8
}

//NewChannelReq represents a NewChannelReq Frame Option
type NewChannelReq interface {
	FrameOption
	ChIndex() uint8
	Freq() uint32
	MaxDR() uint8
	MinDR() uint8
}

//NewChannelAns represents a NewChannelAns Frame Option
type NewChannelAns interface {
	FrameOption
	DataRateRangeOK() bool
	ChannelFrequencyOK() bool
}

//RXTimingSetupReq represents a RXTimingSetupReq Frame Option
type RXTimingSetupReq interface {
	FrameOption
	Del() uint8
}

//RXTimingSetupAns represents a RXTimingSetupAns Frame Option
type RXTimingSetupAns interface {
	FrameOption
}

type frameOption []byte

//ByID represents a sorted Frame Option array
type ByID []FrameOption

func (a ByID) Len() int           { return len(a) }
func (a ByID) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByID) Less(i, j int) bool { return a[i].ID() < a[j].ID() }

//Fopts converts a Frame Option array to a byte array
func Fopts(fopts []FrameOption) ([]byte, error) {
	r := make([]byte, 0)
	sort.Sort(ByID(fopts))
	for _, fopt := range fopts {
		r = append(r, fopt.Bytes()...)
	}
	return r, nil
}
