package parse

import (
	"crypto/aes"

	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

// JoinAccept parses a join accept (response) message
func JoinAccept(message []byte, key [16]byte, re region.Region) (v1_0_0.JoinAccept, error) {
	var appnonce [3]byte
	var netid [3]byte
	var devaddr [4]byte
	var rx1droffset byte
	var rx2datarate byte
	var rxdelay byte
	var cflist *[16]byte = &[16]byte{}
	var mic *[4]byte = &[4]byte{}

	copy(appnonce[:], util.Bytereverse(message[1:4]))
	copy(netid[:], util.Bytereverse(message[4:7]))
	copy(devaddr[:], util.Bytereverse(message[7:11]))
	rx1droffset = (message[11] >> 4) & 0x07
	rx2datarate = message[11] & 0x0F
	rxdelay = message[12]

	if len(message) == 17 {
		copy((*mic)[:], message[13:])
		cflist = nil
	} else {
		copy((*cflist)[:], message[13:])
		copy((*mic)[:], message[29:])
	}
	ja, err := create.JoinAccept(appnonce, netid, devaddr, rx1droffset, rx2datarate, rxdelay, cflist, mic, key, re)
	if err != nil {
		return nil, err
	}
	return ja, nil
}

// JoinAcceptEncrypted parses an encrypted join accept (response) message
func JoinAcceptEncrypted(message []byte, key [16]byte, re region.Region) (v1_0_0.JoinAccept, error) {
	cipher, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}
	i := 1
	if len(message) > 17 {
		i++
	}
	r := make([]byte, 16*i)
	for index := 0; index < i; index++ {
		cipher.Encrypt(r[16*index:16*index+16],
			message[16*index+1:16*index+17])
	}
	a := []byte{message[0]}
	a = append(a, r...)
	ja, err := JoinAccept(a, key, re)
	if err != nil {
		return nil, err
	}
	return ja, nil
}
