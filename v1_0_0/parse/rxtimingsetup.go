package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type rxtimingsetupreq []byte

// RXTimingSetupReq parses an RX Parameter Setup request frame option payload
func RXTimingSetupReq(payload [1]byte) (v1_0_0.RXTimingSetupReq, error) {
	r := make([]byte, v1_0_0.RXTimingSetupReqLen)
	r[0] = byte(v1_0_0.RXTimingSetupReqID)
	copy(r[1:], payload[:])
	return rxtimingsetupreq(r), nil
}

func (mac rxtimingsetupreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxtimingsetupreq) Payload() []byte {
	return mac[1:]
}

func (mac rxtimingsetupreq) Bytes() []byte {
	return mac[:]
}

func (mac rxtimingsetupreq) Del() uint8 {
	return mac[1] & 0x0f
}

type rxtimingsetupans []byte

// RXTimingSetupAns parses an RX Parameter Setup anser frame option payload
func RXTimingSetupAns() (v1_0_0.RXTimingSetupAns, error) {
	r := make([]byte, v1_0_0.RXTimingSetupAnsLen)
	r[0] = byte(v1_0_0.RXTimingSetupAnsID)
	return rxtimingsetupans(r), nil
}

func (mac rxtimingsetupans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxtimingsetupans) Payload() []byte {
	return mac[1:]
}

func (mac rxtimingsetupans) Bytes() []byte {
	return mac[:]
}
