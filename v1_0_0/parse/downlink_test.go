package parse

import (
	"reflect"
	"testing"
)

func TestDownlink(t *testing.T) {
	type args struct {
		message []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Downlink(tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("Downlink() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Bytes(), tt.want) {
				t.Errorf("Downlink() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDownlinkEncrypted(t *testing.T) {
	type args struct {
		message []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{"with fopts",
			args{message: []byte{0x60, 0xAE, 0x63, 0xFD, 0x49, 0xA5, 0x0C, 0x00, 0x03, 0x11, 0xFF, 0x00, 0x00, 0x36, 0xEA, 0xC5, 0xC7}},
			[]byte{0x60, 0xAE, 0x63, 0xFD, 0x49, 0xA5, 0x0C, 0x00, 0x03, 0x11, 0xFF, 0x00, 0x00, 0x36, 0xEA, 0xC5, 0xC7},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DownlinkEncrypted(tt.args.message)
			if (err != nil) != tt.wantErr {
				t.Errorf("DownlinkEncrypted() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got.Bytes(), tt.want) {
				t.Errorf("DownlinkEncrypted() = %v, want %v", got, tt.want)
			}
		})
	}
}
