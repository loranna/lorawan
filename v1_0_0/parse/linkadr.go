package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type linkadrans []byte

// LinkADRAns parses a link ADR answer (response) frame option payload
func LinkADRAns(payload [1]byte) (v1_0_0.LinkADRAns, error) {
	r := make([]byte, v1_0_0.LinkADRAnsLen)
	r[0] = byte(v1_0_0.LinkADRAnsID)
	r[1] = payload[0]
	return linkadrans(r), nil
}

func (mac linkadrans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac linkadrans) Payload() []byte {
	return mac[1:]
}

func (mac linkadrans) Bytes() []byte {
	return mac[:]
}

func (mac linkadrans) PowerACK() bool {
	return (mac[1] & 0x04) == 0x04
}

func (mac linkadrans) DataRateACK() bool {
	return (mac[1] & 0x02) == 0x02
}

func (mac linkadrans) ChannelMaskACK() bool {
	return (mac[1] & 0x01) == 0x01
}

type linkadrreq []byte

// LinkADRReq parses a link ADR request frame option payload
func LinkADRReq(payload [4]byte) (v1_0_0.LinkADRReq, error) {
	r := make([]byte, v1_0_0.LinkADRReqLen)
	r[0] = byte(v1_0_0.LinkADRReqID)
	copy(r[1:], payload[:])
	return linkadrreq(r), nil
}
func (mac linkadrreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac linkadrreq) Payload() []byte {
	return mac[1:]
}

func (mac linkadrreq) Bytes() []byte {
	return mac[:]
}

func (mac linkadrreq) Datarate() byte {
	return mac[1] >> 4
}
func (mac linkadrreq) TxPower() byte {
	return mac[1] & 0x0f
}
func (mac linkadrreq) Chmask() [2]byte {
	b := [2]byte{}
	copy(b[:], mac[2:])
	return b
}
func (mac linkadrreq) ChmaskCntl() byte {
	return (mac[4] >> 4) & 0x07
}
func (mac linkadrreq) Nbrep() byte {
	return mac[4] & 0x0f
}
