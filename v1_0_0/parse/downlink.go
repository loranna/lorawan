package parse

import (
	"fmt"

	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/internal/downlink"
)

// Downlink parses a LoRaWAN downlink byte array
func Downlink(message []byte) (v1_0_0.Downlink, error) {
	m := make([]byte, len(message))
	dn := downlink.Downlink(m)
	mtype := dn.Mtype()
	switch mtype {
	case v1_0_0.TypeUnconfirmedDataDown, v1_0_0.TypeConfirmedDataDown:
	default:
		return nil, fmt.Errorf("message mtype is invalid")
	}
	if dn.Major() != v1_0_0.MajorLoRaWANR1 {
		return nil, fmt.Errorf("message major is invalid")
	}
	if dn.FOptsLen() != byte(len(dn.Fopts())) {
		return nil, fmt.Errorf("message foptslen is invalid")
	}
	return downlink.Downlink(m), nil
}

// DownlinkEncrypted parses an encrypted LoRaWAN downlink byte array
func DownlinkEncrypted(message []byte) (v1_0_0.DownlinkEncrypted, error) {
	m := make([]byte, len(message))
	copy(m, message)
	return downlink.Downlinke(m), nil
}
