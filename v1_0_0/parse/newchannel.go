package parse

import (
	"encoding/binary"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

type newchannelreq []byte

// NewChannelReq parses a new channel request frame option payload
func NewChannelReq(payload [5]byte) (v1_0_0.NewChannelReq, error) {
	r := make([]byte, v1_0_0.NewChannelReqLen)
	r[0] = byte(v1_0_0.NewChannelReqID)
	copy(r[1:], payload[:])
	return newchannelreq(r), nil
}

func (mac newchannelreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac newchannelreq) Payload() []byte {
	return mac[1:]
}

func (mac newchannelreq) Bytes() []byte {
	return mac[:]
}

func (mac newchannelreq) ChIndex() uint8 {
	return mac[1]
}

func (mac newchannelreq) Freq() uint32 {
	b := [4]byte{}
	copy(b[:], mac[2:5])
	return binary.LittleEndian.Uint32(b[:]) * 100
}

func (mac newchannelreq) MaxDR() uint8 {
	return mac[5] >> 4
}

func (mac newchannelreq) MinDR() uint8 {
	return mac[5] & 0x0f
}

type newchannelans []byte

// NewChannelAns parses a new channel answer frame option payload
func NewChannelAns(payload [1]byte) (v1_0_0.NewChannelAns, error) {
	r := make([]byte, v1_0_0.NewChannelAnsLen)
	r[0] = byte(v1_0_0.NewChannelAnsID)
	copy(r[1:], payload[:])
	return newchannelans(r), nil
}

func (mac newchannelans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac newchannelans) Payload() []byte {
	return mac[1:]
}

func (mac newchannelans) Bytes() []byte {
	return mac[:]
}

func (mac newchannelans) DataRateRangeOK() bool {
	h := byte(0x02)
	return (mac[1] & h) == h
}

func (mac newchannelans) ChannelFrequencyOK() bool {
	h := byte(0x01)
	return (mac[1] & h) == h
}
