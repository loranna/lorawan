package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

// FoptsDown parses downlink frame options byte array
func FoptsDown(fopts []byte) ([]v1_0_0.FrameOption, error) {
	foptions := make([]v1_0_0.FrameOption, 0)
	for i := 0; i < len(fopts); {
		switch v1_0_0.FOptID(fopts[i]) {
		case v1_0_0.LinkCheckAnsID:
			var op [v1_0_0.LinkCheckAnsLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := LinkCheckAns(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.LinkCheckAnsLen
		case v1_0_0.LinkADRReqID:
			var op [v1_0_0.LinkADRReqLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := LinkADRReq(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.LinkADRReqLen
		case v1_0_0.DutyCycleReqID:
			var op [v1_0_0.DutyCycleReqLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := DutyCycleReq(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.DutyCycleReqLen
		case v1_0_0.RXParamSetupReqID:
			var op [v1_0_0.RXParamSetupReqLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := RXParamSetupReq(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.RXParamSetupReqLen
		case v1_0_0.DevStatusReqID:
			option, err := DevStatusReq()
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.DevStatusReqLen
		case v1_0_0.NewChannelReqID:
			var op [v1_0_0.NewChannelReqLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := NewChannelReq(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.NewChannelReqLen
		case v1_0_0.RXTimingSetupReqID:
			var op [v1_0_0.RXTimingSetupReqLen - 1]byte
			copy(op[:], fopts[i+1:])
			option, err := RXTimingSetupReq(op)
			if err != nil {
				return nil, err
			}
			foptions = append(foptions, option)
			i += v1_0_0.RXTimingSetupReqLen
		}
	}
	return foptions, nil
}
