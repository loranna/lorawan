package parse

import (
	"encoding/binary"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

type rxparamsetupreq []byte

// RXParamSetupReq parses an RX Parameter Setup request frame option payload
func RXParamSetupReq(payload [4]byte) (v1_0_0.RXParamSetupReq, error) {
	r := make([]byte, v1_0_0.RXParamSetupReqLen)
	r[0] = byte(v1_0_0.RXParamSetupReqID)
	copy(r[1:], payload[:])
	return rxparamsetupreq(r), nil
}

func (mac rxparamsetupreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxparamsetupreq) Payload() []byte {
	return mac[1:]
}

func (mac rxparamsetupreq) Bytes() []byte {
	return mac[:]
}

func (mac rxparamsetupreq) RX1DRoffset() uint8 {
	return (mac[1] >> 4) & 0x07
}
func (mac rxparamsetupreq) RX2DataRate() uint8 {
	return mac[1] & 0x0f
}
func (mac rxparamsetupreq) Frequency() uint32 {
	b := [4]byte{}
	copy(b[:], mac[2:])
	return binary.LittleEndian.Uint32(b[:]) * 100
}

type rxparamsetupans []byte

// RXParamSetupAns parses an RX Parameter Setup answer frame option payload
func RXParamSetupAns(payload [1]byte) (v1_0_0.RXParamSetupAns, error) {
	r := make([]byte, v1_0_0.RXParamSetupAnsLen)
	r[0] = byte(v1_0_0.RXParamSetupAnsID)
	copy(r[1:], payload[:])
	return rxparamsetupans(r), nil
}

func (mac rxparamsetupans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxparamsetupans) Payload() []byte {
	return mac[1:]
}

func (mac rxparamsetupans) Bytes() []byte {
	return mac[:]
}
func (mac rxparamsetupans) RX1DRoffsetACK() bool {
	h := byte(0x04)
	return (mac[1] & h) == h
}
func (mac rxparamsetupans) RX2DataRateACK() bool {
	h := byte(0x02)
	return (mac[1] & h) == h
}
func (mac rxparamsetupans) ChannelACK() bool {
	h := byte(0x01)
	return (mac[1] & h) == h
}
