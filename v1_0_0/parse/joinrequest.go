package parse

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

// JoinRequest parses a join request message
func JoinRequest(message [23]byte, key [16]byte) (v1_0_0.JoinRequest, error) {
	var appeui [8]byte
	var deveui [8]byte
	var devnonce [2]byte

	copy(appeui[:], util.Bytereverse(message[1:9]))
	copy(deveui[:], util.Bytereverse(message[9:17]))
	copy(devnonce[:], util.Bytereverse(message[17:19]))
	jr, err := create.JoinRequest(appeui, deveui, devnonce, key)
	if err != nil {
		return nil, err
	}
	mic := jr.MIC()
	if mic[0] != message[19] || mic[1] != message[20] || mic[2] != message[21] || mic[3] != message[22] {
		logrus.Warningf("MICs do not match! got: %X, calculated %X", message[19:], mic[:])
	}
	return jr, nil
}
