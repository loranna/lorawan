package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/internal/uplink"
)

// Uplink parses a LoRaWAN uplink byte array
func Uplink(message []byte) (v1_0_0.Uplink, error) {

	m := make([]byte, len(message))
	copy(m, message)
	return uplink.Uplink(m), nil
}

// UplinkEncrypted parses an encrypted LoRaWAN uplink byte array
func UplinkEncrypted(message []byte) (v1_0_0.UplinkEncrypted, error) {
	m := make([]byte, len(message))
	copy(m, message)
	return uplink.Uplinke(m), nil
}
