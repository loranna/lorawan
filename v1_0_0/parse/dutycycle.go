package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type dutycyclereq []byte

// DutyCycleReq parses a duty cycle req frame option payload
func DutyCycleReq(payload [1]byte) (v1_0_0.DutyCycleReq, error) {
	r := make([]byte, v1_0_0.DutyCycleReqLen)
	r[0] = byte(v1_0_0.DutyCycleReqID)
	r[1] = payload[0]
	return dutycyclereq(r), nil
}

func (mac dutycyclereq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac dutycyclereq) Payload() []byte {
	return mac[1:]
}

func (mac dutycyclereq) Bytes() []byte {
	return mac[:]
}

func (mac dutycyclereq) MaxDCycle() uint8 {
	return mac[1]
}

type dutycycleans []byte

// DutyCycleAns parses a duty cycle ans frame option payload
func DutyCycleAns() (v1_0_0.DutyCycleAns, error) {
	r := make([]byte, v1_0_0.DutyCycleAnsLen)
	r[0] = byte(v1_0_0.DutyCycleAnsID)
	return dutycycleans(r), nil
}

func (mac dutycycleans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac dutycycleans) Payload() []byte {
	return mac[1:]
}

func (mac dutycycleans) Bytes() []byte {
	return mac[:]
}
