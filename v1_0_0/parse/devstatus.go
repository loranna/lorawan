package parse

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type devstatusreq []byte

//DevStatusReq create a DevStatusReq Frame Option
func DevStatusReq() (v1_0_0.DevStatusReq, error) {
	r := make([]byte, v1_0_0.DevStatusReqLen)
	r[0] = byte(v1_0_0.DevStatusReqID)
	return devstatusreq(r), nil
}

func (mac devstatusreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac devstatusreq) Payload() []byte {
	return mac[1:]
}

func (mac devstatusreq) Bytes() []byte {
	return mac[:]
}

type devstatusans []byte

//DevStatusAns create a DevStatusAns Frame Option
func DevStatusAns(payload [2]byte) (v1_0_0.DevStatusAns, error) {
	r := make([]byte, v1_0_0.DevStatusAnsLen)
	r[0] = byte(v1_0_0.DevStatusAnsID)
	r[1] = payload[0]
	r[2] = payload[1]
	return devstatusans(r), nil
}

func (mac devstatusans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac devstatusans) Payload() []byte {
	return mac[1:]
}

func (mac devstatusans) Bytes() []byte {
	return mac[:]
}

func (mac devstatusans) Battery() uint8 {
	return mac[1]
}
func (mac devstatusans) Margin() uint8 {
	return mac[2]
}
