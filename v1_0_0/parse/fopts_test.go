package parse

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

func TestFoptsDown(t *testing.T) {
	type args struct {
		fopts []byte
	}
	adrreq, err := LinkADRReq([...]byte{0x11, 0xFF, 0x00, 0x00})
	if err != nil {
		t.Errorf("LinkADRReq() error = %v", err)
		return
	}
	tests := []struct {
		name    string
		args    args
		want    []v1_0_0.FrameOption
		wantErr bool
	}{

		{"linkadrreq",
			args{fopts: []byte{0x03, 0x11, 0xFF, 0x00, 0x00}},
			[]v1_0_0.FrameOption{adrreq},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := FoptsDown(tt.args.fopts)
			if (err != nil) != tt.wantErr {
				t.Errorf("FoptsDown() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FoptsDown() = %v, want %v", got, tt.want)
			}
		})
	}
}
