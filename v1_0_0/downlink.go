package v1_0_0

//Downlink represents a decrypted downlink message
type Downlink interface {
	Encrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (DownlinkEncrypted, error)
	Bytes()[]byte

	Mtype() MType
	Major() Major
	Devaddr() [4]byte
	Adr() bool
	AdrAckreq() bool
	Ack() bool
	FPending() bool
	FOptsLen() byte
	Fcnt() uint16
	Fopts() []byte
	Fport() *uint8
	Frmpayload() []byte
}

//DownlinkEncrypted represents a encrypted downlink message
type DownlinkEncrypted interface {
	Decrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (Downlink, error)
	Bytes() []byte
	MICValid(key [16]byte, MSBofcnt, MSBminus1ofcnt byte) (bool, error)

	Mtype() MType
	Major() Major
	Devaddr() [4]byte
	Adr() bool
	AdrAckreq() bool
	Ack() bool
	FPending() bool
	FOptsLen() byte
	Fcnt() uint16
	Fopts() []byte
	Fport() *uint8
	Efrmpayload() []byte
	MIC() [4]byte
}
