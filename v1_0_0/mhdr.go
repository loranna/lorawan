package v1_0_0

//go:generate stringer -type=MType
//go:generate stringer -type=Major

//MType represents the type of the message
type MType byte

const (
	//TypeJoinRequest message type
	TypeJoinRequest MType = iota
	//TypeJoinAccept message type
	TypeJoinAccept
	//TypeUnconfirmedDataUp message type
	TypeUnconfirmedDataUp
	//TypeUnconfirmedDataDown message type
	TypeUnconfirmedDataDown
	//TypeConfirmedDataUp message type
	TypeConfirmedDataUp
	//TypeConfirmedDataDown message type
	TypeConfirmedDataDown
	_
	//TypeProprietary message type
	TypeProprietary
)

//Major represents the major version of the message
type Major byte

const (
	//MajorLoRaWANR1 message major version
	MajorLoRaWANR1 Major = iota
)
