package create

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/internal/downlink"
)

func TestDownlink(t *testing.T) {
	type args struct {
		confirmed  bool
		devaddr    [4]byte
		enableadr  bool
		adrackreq  bool
		ack        bool
		fpending   bool
		fcnt       uint16
		fopts      []byte
		fport      *uint8
		frmpayload []byte
		mic        *[4]byte
	}
	port := uint8(12)
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.Downlink
		wantErr bool
	}{
		{"default",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				false, false, false, false,
				55,
				nil,
				nil,
				nil,
				nil,
			},
			downlink.Downlink([]byte{0xA0, 0x33, 0x33, 0x44, 0x33, 0x00, 0x37, 0x00}),
			false,
		},
		{"fopts",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				false, false, false, false,
				55,
				[]byte{0x06},
				nil,
				nil,
				nil,
			},
			downlink.Downlink([]byte{0xA0, 0x33, 0x33, 0x44, 0x33, 0x01, 0x37, 0x00, 0x06}),
			false,
		},
		{"fport",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				false, false, false, false,
				55,
				[]byte{0x06},
				&port,
				nil,
				nil,
			},
			nil, //downlink.Downlink([]byte{0xA0, 0x33, 0x33, 0x44, 0x33, 0x01, 0x37, 0x00, 0x06, 0x0c,0x00, 0x00, 0x00, 0x00}),
			true,
		},
		{"fport2",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				false, false, false, false,
				55,
				[]byte{0x06},
				&port,
				[]byte{0xFF},
				nil,
			},
			downlink.Downlink([]byte{0xA0, 0x33, 0x33, 0x44, 0x33, 0x01, 0x37, 0x00, 0x06, 0x0c, 0xFF}),
			false,
		},
		{"bits",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				true, true, true, true,
				55,
				nil,
				nil,
				nil,
				nil,
			},
			downlink.Downlink([]byte{0xA0, 0x33, 0x33, 0x44, 0x33, 0xF0, 0x37, 0x00}),
			false,
		},
		{"overfopts",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				true, true, true, true,
				55,
				[]byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
				nil,
				nil,
				nil,
			},
			nil,
			true,
		},
		{"payloadwithoutport",
			args{
				true,
				[4]byte{0x33, 0x44, 0x33, 0x33},
				true, true, true, true,
				55,
				nil,
				nil,
				[]byte{0x00},
				nil,
			},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Downlink(tt.args.confirmed, tt.args.devaddr, tt.args.enableadr, tt.args.adrackreq, tt.args.ack, tt.args.fpending, tt.args.fcnt, tt.args.fopts, tt.args.fport, tt.args.frmpayload, tt.args.mic)
			if (err != nil) != tt.wantErr {
				t.Errorf("Downlink() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Downlink() = %v, want %v", got, tt.want)
			}
		})
	}
}
