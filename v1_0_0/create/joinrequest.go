package create

import (
	"encoding/base64"
	"fmt"

	"github.com/jacobsa/crypto/cmac"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type joinRequest []byte

//JoinRequest creates a join request message
//Relevant LoRaWAN specification chapers:
// - 6.2.4
// - 4.4
func JoinRequest(appeui, deveui [8]byte, devnonce [2]byte, appkey [16]byte) (v1_0_0.JoinRequest, error) {
	data := make([]byte, 1+8+8+2+4)
	data[0] = 0
	copy(data[1:], util.Bytereverse(appeui[:]))
	copy(data[9:], util.Bytereverse(deveui[:]))
	copy(data[17:], util.Bytereverse(devnonce[:]))
	jr := joinRequest(data)
	err := jr.calculateMic(appkey)
	if err != nil {
		return nil, err
	}
	return jr, nil
}

func (jr joinRequest) calculateMic(key [16]byte) error {
	data := []byte(jr)
	result, err := cmac.New(key[:])
	if err != nil {
		return err
	}
	_, err = result.Write(data[:19])
	if err != nil {
		return err
	}
	rr := result.Sum(nil)
	copy(data[19:], rr[:4])
	return nil
}

func (jr joinRequest) MType() v1_0_0.MType {
	data := joinRequest(jr)
	return v1_0_0.MType((data[0] >> 5) & 0x07)
}

func (jr joinRequest) Major() v1_0_0.Major {
	data := joinRequest(jr)
	return v1_0_0.Major(data[0] & 0x03)
}

func (jr joinRequest) AppEUI() [8]byte {
	data := joinRequest(jr)
	var r [8]byte
	copy(r[:], util.Bytereverse(data[1:9]))
	return r
}

func (jr joinRequest) DevEUI() [8]byte {
	data := joinRequest(jr)
	var r [8]byte
	copy(r[:], util.Bytereverse(data[9:17]))
	return r
}

func (jr joinRequest) DevNonce() [2]byte {
	data := joinRequest(jr)
	var r [2]byte
	copy(r[:], util.Bytereverse(data[17:19]))
	return r
}

func (jr joinRequest) MIC() [4]byte {
	data := joinRequest(jr)
	var r [4]byte
	copy(r[:], data[19:])
	return r
}

func (jr joinRequest) Bytes() [23]byte {
	data := joinRequest(jr)
	var r [23]byte
	copy(r[:], data)
	return r
}

func (jr joinRequest) String() string {
	data := []byte(jr)
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
DevEUI: %X
AppEUI: %X
DevNonce: %X
Mic: %X
binary representation: %X
base64 representation: %s
`,
		jr.MType(), jr.Major(),
		jr.DevEUI(),
		jr.AppEUI(),
		jr.DevNonce(),
		jr.MIC(),
		data[:],
		base64.StdEncoding.EncodeToString(data[:]))
}
