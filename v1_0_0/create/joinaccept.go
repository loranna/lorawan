package create

import (
	"crypto/aes"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/jacobsa/crypto/cmac"
	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
	"gitlab.com/loranna/lorawan/region/v1_0_0/US902_928"
	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type joinAccept struct {
	r    region.Region
	data []byte
}
type joinAccepte struct {
	r    region.Region
	data []byte
}

//JoinAccept creates a join accept message
//Relevant LoRaWAN specification chapers:
// - 6.2.5
// - 4.4
func JoinAccept(appnonce [3]byte, netid [3]byte, devaddr [4]byte, rx1droffset byte,
	rx2datarate byte, rxdelay byte, cflist *[16]byte, mic *[4]byte, key [16]byte, r region.Region) (v1_0_0.JoinAccept, error) {
	j := make([]byte, 17)
	ja := joinAccept{r: r, data: j}
	ja.data[0] = 0x20
	copy(ja.data[1:4], util.Bytereverse(appnonce[:]))
	copy(ja.data[4:7], util.Bytereverse(netid[:]))
	copy(ja.data[7:11], util.Bytereverse(devaddr[:]))
	ja.data[11] = (rx1droffset & 0x07) << 4
	ja.data[11] = rx2datarate & 0x0f
	ja.data[12] = rxdelay
	if cflist != nil {
		cf := make([]byte, 16)
		ja.data = append(ja.data, cf...)
		copy(ja.data[13:], (*cflist)[:])
	}
	err := ja.calculateMic(key)
	if err != nil {
		return nil, err
	}
	if mic != nil {
		s := len(ja.data)
		for i := 0; i < 4; i++ {
			if mic[i] != ja.data[s-4+i] {
				return nil, fmt.Errorf("predefined and calculated MIC differs: %X, %X", *mic, ja.data[s-4:])
			}
		}
	}
	return ja, nil
}

func (ja joinAccept) calculateMic(key [16]byte) error {
	//ja includes message type
	data := []byte(ja.data)
	l := len(data)
	result, err := cmac.New(key[:])
	if err != nil {
		return err
	}
	_, err = result.Write(data[:l-4])
	if err != nil {
		return err
	}
	rr := result.Sum([]byte{})
	copy(data[l-4:], rr[:4])
	return nil
}

func (ja joinAccept) Bytes() []byte {
	data := joinAccept(ja)
	r := make([]byte, len(data.data))
	copy(r[:], data.data)
	return r
}

func (ja joinAccept) Encrypt(key [16]byte) (v1_0_0.JoinAcceptEncrypted, error) {
	b := ja.Bytes()
	cipher, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}
	i := 1
	if len(b) > 17 {
		i++
	}
	r := make([]byte, 16*i)
	for index := 0; index < i; index++ {
		cipher.Decrypt(r[16*index:16*index+16],
			b[16*index+1:16*index+17])
	}
	a := []byte{b[0]}
	a = append(a, r...)
	return joinAccepte{data: a, r: ja.r}, nil
}

func (ja joinAccept) AppNonce() [3]byte {
	var p [3]byte
	copy(p[:], util.Bytereverse(ja.data[1:4]))
	return p
}
func (ja joinAccept) NetID() [3]byte {
	var p [3]byte
	copy(p[:], util.Bytereverse(ja.data[4:7]))
	return p
}
func (ja joinAccept) DevAddr() [4]byte {
	var p [4]byte
	copy(p[:], util.Bytereverse(ja.data[7:11]))
	return p
}
func (ja joinAccept) RX1DRoffset() byte {
	return (ja.data[11] >> 4) & 0x07
}
func (ja joinAccept) RX2DataRate() byte {
	return ja.data[11] & 0x0F
}
func (ja joinAccept) RxDelay() byte {
	p := ja.data[12]
	return p
}
func (ja joinAccept) CFList() *[16]byte {
	if len(ja.data) == 17 {
		return nil
	}
	var p [16]byte
	copy(p[:], ja.data[13:])
	return &p
}
func (ja joinAccept) MIC() [4]byte {
	var p [4]byte
	if len(ja.data) == 17 {
		copy(p[:], ja.data[13:])
	} else {
		copy(p[:], ja.data[29:])
	}
	return p
}

func (ja joinAccept) MType() v1_0_0.MType {
	data := joinAccept(ja)
	return v1_0_0.MType((data.data[0] >> 5) & 0x07)
}

func (ja joinAccept) Major() v1_0_0.Major {
	data := joinAccept(ja)
	return v1_0_0.Major(data.data[0] & 0x03)
}

func (ja joinAccept) cFListString() string {
	if len(ja.data) == 17 {
		return "none"
	}
	switch ja.r {
	case region.EU863_870:
		freqs, err := getfreqs(ja.data[13:29])
		if err != nil {
			return fmt.Sprintf("error: %s", err)
		}
		return hex.EncodeToString(ja.data[13:29]) + " parsed: " + freqs
	case region.US902_928:
		chids, err := getchids(ja.data[13:29])
		if err != nil {
			return fmt.Sprintf("error: %s", err)
		}
		return hex.EncodeToString(ja.data[13:29]) + " enabled channels: " + chids
	default:
		return fmt.Sprintf("unknown region %s", ja.r)
	}
}

func getfreqs(cflist []byte) (string, error) {
	list := [16]byte{}
	copy(list[:], cflist)
	f1, f2, f3, f4, f5, err := EU863_870.ParseCFList(list)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%.3f MHz, %.3f MHz, %.3f MHz, %.3f MHz, %.3f MHz",
		float64(f1)/1000000, float64(f2)/1000000, float64(f3)/1000000,
		float64(f4)/1000000, float64(f5)/1000000), nil
}

func getchids(cflist []byte) (string, error) {
	list := [16]byte{}
	copy(list[:], cflist)
	channels, err := US902_928.ParseCFList(list)
	if err != nil {
		return "", err
	}
	returnstring := strings.Builder{}
	for i, channel := range channels {
		if channel.Frequency > 0 {
			returnstring.WriteString(fmt.Sprintf("%d,", i))
		}
	}

	return returnstring.String(), nil
}

func (ja joinAccept) String() string {
	b := ja.Bytes()
	return fmt.Sprintf(
		`Message Type: %s
Major: %s
AppNonce: %X
NetID: %X
DevAddr: %X
RX1DRoffset: %X
RX2 Data rate: %X
RxDelay: %X
CFList: %s
Mic: %X
binary representation non-encrypted: %X
base64 representation non-encrypted: %s
`,
		ja.MType(), ja.Major(),
		ja.AppNonce(),
		ja.NetID(),
		ja.DevAddr(),
		ja.RX1DRoffset(),
		ja.RX2DataRate(),
		ja.RxDelay(),
		ja.cFListString(),
		ja.MIC(),
		b,
		base64.StdEncoding.EncodeToString(b[:]))
}

func (jae joinAccepte) Bytes() []byte {
	r := make([]byte, len(jae.data))
	copy(r[:], jae.data)
	return r
}

func (jae joinAccepte) Decrypt(key [16]byte) (v1_0_0.JoinAccept, error) {
	b := jae.Bytes()
	cipher, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}
	i := 1
	if len(b) > 17 {
		i++
	}
	r := make([]byte, 16*i)
	for index := 0; index < i; index++ {
		cipher.Encrypt(r[16*index:16*index+16],
			b[16*index+1:16*index+17])
	}
	a := []byte{b[0]}
	a = append(a, r...)
	return joinAccept{data: a, r: jae.r}, nil
}

//Keys generates keys based on join parameters
//Relevant LoRaWAN specification chapers:
// - 6.2
func Keys(appkey [16]byte, appnonce, netid [3]byte, devnonce [2]byte) ([16]byte, [16]byte, error) {
	cr, err := aes.NewCipher(appkey[:])
	if err != nil {
		return [16]byte{}, [16]byte{}, err
	}
	var A, nwkskey, appskey [16]byte
	A[0] = 0x01
	copy(A[1:], util.Bytereverse(appnonce[:]))
	copy(A[4:], util.Bytereverse(netid[:]))
	copy(A[7:], util.Bytereverse(devnonce[:]))
	cr.Encrypt(nwkskey[:], A[:])
	A[0] = 0x02
	cr.Encrypt(appskey[:], A[:])

	return nwkskey, appskey, nil
}
