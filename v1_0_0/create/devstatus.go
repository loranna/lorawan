package create

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type devstatusreq []byte

// DevStatusReq creates a device status request from an end-device.
func DevStatusReq() (v1_0_0.DevStatusReq, error) {
	r := make([]byte, v1_0_0.DevStatusReqLen)
	r[0] = byte(v1_0_0.DevStatusReqID)
	return devstatusreq(r), nil
}

func (mac devstatusreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac devstatusreq) Payload() []byte {
	return mac[1:]
}

func (mac devstatusreq) Bytes() []byte {
	return mac[:]
}

type devstatusans []byte

// DevStatusAns creates a device status answer
// The battery level reported is encoded as follows:
//
// +---------+----------------------------------------------------------------+
// | Battery |                          Description                           |
// +---------+----------------------------------------------------------------+
// |       0 | The end-device is connected to an external power source.       |
// |  1..254 | The battery level, 1 being at minimum and 254 being at maximum |
// |     255 | The end-device was not able to measure the battery level.      |
// +---------+----------------------------------------------------------------+
//
// The margin is the demodulation signal-to-noise ratio in dB rounded to the
// nearest integer value for the last successfully received DevStatusReq command.
// It is a signed integer of 6 bits with a minimum value of -32 and a maximum
// value of 31.
func DevStatusAns(battery, margin uint8) (v1_0_0.DevStatusAns, error) {
	r := make([]byte, v1_0_0.DevStatusAnsLen)
	r[0] = byte(v1_0_0.DevStatusAnsID)
	r[1] = battery
	r[2] = margin
	return devstatusans(r), nil
}

func (mac devstatusans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac devstatusans) Payload() []byte {
	return mac[1:]
}

func (mac devstatusans) Bytes() []byte {
	return mac[:]
}

func (mac devstatusans) Battery() uint8 {
	return mac[1]
}
func (mac devstatusans) Margin() uint8 {
	return mac[2]
}
