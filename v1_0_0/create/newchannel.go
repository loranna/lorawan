package create

import (
	"encoding/binary"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

type newchannelreq []byte

//NewChannelReq creates a request for new channel
//Relevant LoRaWAN specification chapers:
// - 5.6
func NewChannelReq(chindex uint8, frequency uint32, maxdr, mindr uint8) (v1_0_0.NewChannelReq, error) {
	r := make([]byte, v1_0_0.NewChannelReqLen)
	r[0] = byte(v1_0_0.NewChannelReqID)
	r[1] = chindex
	binary.LittleEndian.PutUint32(r[2:], frequency/100)
	r[5] = (maxdr << 4) | (mindr & 0x0f)
	return newchannelreq(r), nil
}

func (mac newchannelreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac newchannelreq) Payload() []byte {
	return mac[1:]
}

func (mac newchannelreq) Bytes() []byte {
	return mac[:]
}

func (mac newchannelreq) ChIndex() uint8 {
	return mac[1]
}
func (mac newchannelreq) Freq() uint32 {
	b := [4]byte{}
	copy(b[:], mac[2:5])
	return binary.LittleEndian.Uint32(b[:]) * 100
}
func (mac newchannelreq) MaxDR() uint8 {
	return mac[5] >> 4
}
func (mac newchannelreq) MinDR() uint8 {
	return mac[5] & 0x0f
}

type newchannelans []byte

//NewChannelAns creates an answer for new channel request
//Relevant LoRaWAN specification chapers:
// - 5.6
func NewChannelAns(dataraterange, channelfrequency bool) (v1_0_0.NewChannelAns, error) {
	r := make([]byte, v1_0_0.NewChannelAnsLen)
	r[0] = byte(v1_0_0.NewChannelAnsID)
	if dataraterange {
		r[1] |= 0x02
	}
	if channelfrequency {
		r[1] |= 0x01
	}
	return newchannelans(r), nil
}

func (mac newchannelans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac newchannelans) Payload() []byte {
	return mac[1:]
}

func (mac newchannelans) Bytes() []byte {
	return mac[:]
}

func (mac newchannelans) DataRateRangeOK() bool {
	h := byte(0x02)
	return (mac[1] & h) == h
}
func (mac newchannelans) ChannelFrequencyOK() bool {
	h := byte(0x01)
	return (mac[1] & h) == h
}
