package create

import (
	"encoding/binary"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

type rxparamsetupreq []byte

//RXParamSetupReq creates a request for RX parameter setup
//Relevant LoRaWAN specification chapers:
// - 5.4
func RXParamSetupReq(RX1DRoffset, RX2DataRate uint8, frequency uint32) (v1_0_0.RXParamSetupReq, error) {
	r := make([]byte, v1_0_0.RXParamSetupReqLen)
	r[0] = byte(v1_0_0.RXParamSetupReqID)
	r[1] = ((RX1DRoffset & 0x07) << 4) | (RX2DataRate & 0x0f)
	binary.LittleEndian.PutUint32(r[2:], frequency/100)
	return rxparamsetupreq(r), nil
}

func (mac rxparamsetupreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxparamsetupreq) Payload() []byte {
	return mac[1:]
}

func (mac rxparamsetupreq) Bytes() []byte {
	return mac[:]
}

func (mac rxparamsetupreq) RX1DRoffset() uint8 {
	return (mac[1] >> 4) & 0x07
}
func (mac rxparamsetupreq) RX2DataRate() uint8 {
	return mac[1] & 0x0f
}
func (mac rxparamsetupreq) Frequency() uint32 {
	b := [4]byte{}
	copy(b[:], mac[2:])
	return binary.LittleEndian.Uint32(b[:]) * 100
}

type rxparamsetupans []byte

//RXParamSetupAns creates an answer for RX parameter setup request
//Relevant LoRaWAN specification chapers:
// - 5.4
func RXParamSetupAns(rx1droffsetack, rx2datarate, channelack bool) (v1_0_0.RXParamSetupAns, error) {
	r := make([]byte, v1_0_0.RXParamSetupAnsLen)
	r[0] = byte(v1_0_0.RXParamSetupAnsID)
	if rx1droffsetack {
		r[1] |= 0x04
	}
	if rx2datarate {
		r[1] |= 0x02
	}
	if channelack {
		r[1] |= 0x01
	}
	return rxparamsetupans(r), nil
}

func (mac rxparamsetupans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxparamsetupans) Payload() []byte {
	return mac[1:]
}

func (mac rxparamsetupans) Bytes() []byte {
	return mac[:]
}
func (mac rxparamsetupans) RX1DRoffsetACK() bool {
	h := byte(0x04)
	return (mac[1] & h) == h
}
func (mac rxparamsetupans) RX2DataRateACK() bool {
	h := byte(0x02)
	return (mac[1] & h) == h
}
func (mac rxparamsetupans) ChannelACK() bool {
	h := byte(0x01)
	return (mac[1] & h) == h
}
