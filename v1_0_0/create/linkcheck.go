package create

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type linkcheckreq []byte

//LinkCheckReq creates a request for link check
//Relevant LoRaWAN specification chapers:
// - 5.1
func LinkCheckReq() (v1_0_0.LinkCheckReq, error) {
	r := make([]byte, v1_0_0.LinkCheckReqLen)
	r[0] = byte(v1_0_0.LinkCheckReqID)
	return linkcheckreq(r), nil
}

func (mac linkcheckreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac linkcheckreq) Payload() []byte {
	return mac[1:]
}

func (mac linkcheckreq) Bytes() []byte {
	return mac[:]
}

type linkcheckans []byte

//LinkCheckAns creates an answer for link check request
//Relevant LoRaWAN specification chapers:
// - 5.1
func LinkCheckAns(margin, gwcnt uint8) (v1_0_0.LinkCheckAns, error) {
	r := make([]byte, v1_0_0.LinkCheckAnsLen)
	r[0] = byte(v1_0_0.LinkCheckAnsID)
	r[1] = margin
	r[2] = gwcnt
	return linkcheckans(r), nil
}

func (mac linkcheckans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac linkcheckans) Payload() []byte {
	return mac[1:]
}

func (mac linkcheckans) Bytes() []byte {
	return mac[:]
}

func (mac linkcheckans) Margin() byte {
	return mac[1]
}

func (mac linkcheckans) GwCnt() byte {
	return mac[2]
}
