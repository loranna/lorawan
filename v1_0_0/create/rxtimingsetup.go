package create

import (
	"gitlab.com/loranna/lorawan/v1_0_0"
)

type rxtimingsetupreq []byte

//RXTimingSetupReq creates a request for RX timing setup
//Relevant LoRaWAN specification chapers:
// - 5.7
func RXTimingSetupReq(del uint8) (v1_0_0.RXTimingSetupReq, error) {
	r := make([]byte, v1_0_0.RXTimingSetupReqLen)
	r[0] = byte(v1_0_0.RXTimingSetupReqID)
	r[1] = del & 0x0f
	return rxtimingsetupreq(r), nil
}

func (mac rxtimingsetupreq) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxtimingsetupreq) Payload() []byte {
	return mac[1:]
}

func (mac rxtimingsetupreq) Bytes() []byte {
	return mac[:]
}

func (mac rxtimingsetupreq) Del() uint8 {
	return mac[1] & 0x0f
}

type rxtimingsetupans []byte

//RXTimingSetupAns creates an answer for RX timing setup request
//Relevant LoRaWAN specification chapers:
// - 5.7
func RXTimingSetupAns() (v1_0_0.RXTimingSetupAns, error) {
	r := make([]byte, v1_0_0.RXTimingSetupAnsLen)
	r[0] = byte(v1_0_0.RXTimingSetupAnsID)
	return rxtimingsetupans(r), nil
}

func (mac rxtimingsetupans) ID() v1_0_0.FOptID {
	return v1_0_0.FOptID(mac[0])
}

func (mac rxtimingsetupans) Payload() []byte {
	return mac[1:]
}

func (mac rxtimingsetupans) Bytes() []byte {
	return mac[:]
}
