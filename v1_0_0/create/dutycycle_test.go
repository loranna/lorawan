package create

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

func TestDutyCycleReq(t *testing.T) {
	type args struct {
		maxdcycle uint8
	}
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.DutyCycleReq
		wantErr bool
	}{
		{"default", args{maxdcycle: 4}, dutycyclereq([]byte{4, 4}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DutyCycleReq(tt.args.maxdcycle)
			if (err != nil) != tt.wantErr {
				t.Errorf("DutyCycleReq() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DutyCycleReq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycyclereq_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycyclereq
		want v1_0_0.FOptID
	}{
		{"default", dutycyclereq([]byte{4, 4}), v1_0_0.DutyCycleReqID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycyclereq.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycyclereq_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycyclereq
		want []byte
	}{
		{"default", dutycyclereq([]byte{4, 4}), []byte{4}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycyclereq.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycyclereq_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycyclereq
		want []byte
	}{
		{"default", dutycyclereq([]byte{4, 4}), []byte{4, 4}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycyclereq.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycyclereq_MaxDCycle(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycyclereq
		want uint8
	}{
		{"default", dutycyclereq([]byte{4, 4}), 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.MaxDCycle(); got != tt.want {
				t.Errorf("dutycyclereq.MaxDCycle() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDutyCycleAns(t *testing.T) {
	tests := []struct {
		name    string
		want    v1_0_0.DutyCycleAns
		wantErr bool
	}{
		{"default", dutycycleans([]byte{4}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DutyCycleAns()
			if (err != nil) != tt.wantErr {
				t.Errorf("DutyCycleAns() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DutyCycleAns() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycycleans_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycycleans
		want v1_0_0.FOptID
	}{
		{"default", dutycycleans([]byte{4, 4}), v1_0_0.DutyCycleAnsID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycycleans.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycycleans_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycycleans
		want []byte
	}{
		{"default", dutycycleans([]byte{4, 4}), []byte{4}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycycleans.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dutycycleans_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  dutycycleans
		want []byte
	}{
		{"default", dutycycleans([]byte{4, 4}), []byte{4, 4}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dutycycleans.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}
