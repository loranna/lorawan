package create

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

func TestDevStatusReq(t *testing.T) {
	tests := []struct {
		name    string
		want    v1_0_0.DevStatusReq
		wantErr bool
	}{
		{"default", devstatusreq([]byte{byte(v1_0_0.DevStatusReqID)}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DevStatusReq()
			if (err != nil) != tt.wantErr {
				t.Errorf("DevStatusReq() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DevStatusReq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusreq_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusreq
		want v1_0_0.FOptID
	}{
		{"default", devstatusreq([]byte{byte(v1_0_0.DevStatusReqID)}), v1_0_0.DevStatusReqID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusreq.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusreq_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusreq
		want []byte
	}{
		{"default", devstatusreq([]byte{byte(v1_0_0.DevStatusReqID)}), []byte{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusreq.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusreq_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusreq
		want []byte
	}{
		{"default", devstatusreq([]byte{byte(v1_0_0.DevStatusReqID)}), []byte{byte(v1_0_0.DevStatusReqID)}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusreq.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDevStatusAns(t *testing.T) {
	type args struct {
		battery uint8
		margin  uint8
	}
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.DevStatusAns
		wantErr bool
	}{
		{"default", args{battery: 22, margin: 6}, devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DevStatusAns(tt.args.battery, tt.args.margin)
			if (err != nil) != tt.wantErr {
				t.Errorf("DevStatusAns() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DevStatusAns() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusans_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusans
		want v1_0_0.FOptID
	}{
		{"default", devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), v1_0_0.DevStatusAnsID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusans.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusans_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusans
		want []byte
	}{
		{"default", devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), []byte{22, 6}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusans.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusans_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusans
		want []byte
	}{
		{"default", devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), []byte{6, 22, 6}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("devstatusans.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusans_Battery(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusans
		want uint8
	}{
		{"default", devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), 22},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Battery(); got != tt.want {
				t.Errorf("devstatusans.Battery() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_devstatusans_Margin(t *testing.T) {
	tests := []struct {
		name string
		mac  devstatusans
		want uint8
	}{
		{"default", devstatusans([]byte{byte(v1_0_0.DevStatusAnsID), 22, 6}), 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Margin(); got != tt.want {
				t.Errorf("devstatusans.Margin() = %v, want %v", got, tt.want)
			}
		})
	}
}
