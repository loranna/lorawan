package create

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/internal/uplink"
)

func TestUplink(t *testing.T) {
	type args struct {
		confirmed  bool
		devaddr    [4]byte
		enableadr  bool
		adrackreq  bool
		ack        bool
		fcnt       uint16
		fopts      []byte
		fport      *uint8
		frmpayload []byte
		mic        *[4]byte
	}
	ports := []uint8{
		55,
	}
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.Uplink
		wantErr bool
	}{
		{
			args: args{
				false,
				[4]byte{0xa9, 0x86, 0xe8, 0x1f},
				true, false, false,
				3,
				nil,
				&ports[0],
				[]byte{0, 0},
				nil,
			},
			want:    uplink.Uplink([]byte{0x40, 0x1f, 0xe8, 0x86, 0xa9, 0x80, 0x03, 0x00, 0x37, 0x0, 0}),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Uplink(tt.args.confirmed, tt.args.devaddr, tt.args.enableadr, tt.args.adrackreq, tt.args.ack, tt.args.fcnt, tt.args.fopts, tt.args.fport, tt.args.frmpayload, tt.args.mic)
			if (err != nil) != tt.wantErr {
				t.Errorf("Uplink() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uplink() = %v, want %v", got, tt.want)
			}
		})
	}
}
