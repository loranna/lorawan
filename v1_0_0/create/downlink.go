package create

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/loranna/lorawan/util"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/internal/downlink"
)

//Downlink creates a downlink message
//Relevant LoRaWAN specification chapers:
// - 4.2.1.2
// - 4.3.1
// - 4.3.2
// - 4.4
// - 5
func Downlink(confirmed bool, devaddr [4]byte, enableadr, adrackreq, ack, fpending bool,
	fcnt uint16, fopts []byte, fport *uint8, frmpayload []byte, mic *[4]byte) (v1_0_0.Downlink, error) {
	portlen := 0
	if fport != nil {
		portlen = 1
	}
	data := make([]byte, 1+4+1+2+len(fopts)+portlen+len(frmpayload))
	mhdr := byte(0x60)
	if confirmed {
		mhdr = 0xa0
	}
	data[0] = mhdr
	copy(data[1:5], util.Bytereverse(devaddr[:]))
	if enableadr {
		data[5] |= 0x80
	}
	if adrackreq {
		data[5] |= 0x40
	}
	if ack {
		data[5] |= 0x20
	}
	if fpending {
		data[5] |= 0x10
	}
	foptslen := len(fopts)
	if foptslen > 15 {
		return nil, fmt.Errorf("fopts length must be lower then 16 but: %d", foptslen)
	}
	data[5] |= byte(foptslen)
	binary.LittleEndian.PutUint16(data[6:8], fcnt)
	copy(data[8:], fopts)
	if len(frmpayload) > 0 && fport == nil {
		return nil, fmt.Errorf("frmpayload has length %d but fport is nil", len(frmpayload))
	}
	if len(frmpayload) == 0 && fport != nil {
		return nil, fmt.Errorf("frmpayload has 0 length but fport is %d", *fport)
	}
	if fport != nil {
		data[8+foptslen] = *fport
		copy(data[8+len(fopts)+1:], frmpayload)
	}
	if mic != nil {
		copy(data[len(data)-4:], (*mic)[:])
	}
	return downlink.Downlink(data), nil
}
