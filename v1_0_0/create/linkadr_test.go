package create

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/v1_0_0"
)

func TestLinkADRAns(t *testing.T) {
	type args struct {
		powerack      bool
		datarateack   bool
		channelmasack bool
	}
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.LinkADRAns
		wantErr bool
	}{
		{"default", args{powerack: true, datarateack: true, channelmasack: true}, linkadrans([]byte{3, 7}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LinkADRAns(tt.args.powerack, tt.args.datarateack, tt.args.channelmasack)
			if (err != nil) != tt.wantErr {
				t.Errorf("LinkADRAns() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LinkADRAns() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want v1_0_0.FOptID
	}{
		{"default", linkadrans([]byte{3, 7}), v1_0_0.LinkADRAnsID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrans.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want []byte
	}{
		{"default", linkadrans([]byte{3, 7}), []byte{7}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrans.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want []byte
	}{
		{"default", linkadrans([]byte{3, 7}), []byte{3, 7}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrans.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_PowerACK(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want bool
	}{
		{"default", linkadrans([]byte{3, 7}), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.PowerACK(); got != tt.want {
				t.Errorf("linkadrans.PowerACK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_DataRateACK(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want bool
	}{
		{"default", linkadrans([]byte{3, 7}), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.DataRateACK(); got != tt.want {
				t.Errorf("linkadrans.DataRateACK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrans_ChannelMaskACK(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrans
		want bool
	}{
		{"default", linkadrans([]byte{3, 7}), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ChannelMaskACK(); got != tt.want {
				t.Errorf("linkadrans.ChannelMaskACK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLinkADRReq(t *testing.T) {
	type args struct {
		datarate   uint8
		txpower    uint8
		chmask     uint16
		chmaskcntl uint8
		nbrep      uint8
	}
	tests := []struct {
		name    string
		args    args
		want    v1_0_0.LinkADRReq
		wantErr bool
	}{
		{"default", args{datarate: 3, txpower: 2, chmask: 0xff00, chmaskcntl: 0, nbrep: 0}, linkadrreq([]byte{3, 50, 0, 255, 0}), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LinkADRReq(tt.args.datarate, tt.args.txpower, tt.args.chmask, tt.args.chmaskcntl, tt.args.nbrep)
			if (err != nil) != tt.wantErr {
				t.Errorf("LinkADRReq() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LinkADRReq() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_ID(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want v1_0_0.FOptID
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), v1_0_0.LinkADRReqID},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ID(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrreq.ID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_Payload(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want []byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), []byte{50, 0, 255, 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Payload(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrreq.Payload() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_Bytes(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want []byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), []byte{3, 50, 0, 255, 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Bytes(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrreq.Bytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_Datarate(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Datarate(); got != tt.want {
				t.Errorf("linkadrreq.Datarate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_TxPower(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.TxPower(); got != tt.want {
				t.Errorf("linkadrreq.TxPower() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_Chmask(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want [2]byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), [2]byte{0, 0xff}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Chmask(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("linkadrreq.Chmask() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_ChmaskCntl(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.ChmaskCntl(); got != tt.want {
				t.Errorf("linkadrreq.ChmaskCntl() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_linkadrreq_Nbrep(t *testing.T) {
	tests := []struct {
		name string
		mac  linkadrreq
		want byte
	}{
		{"default", linkadrreq([]byte{3, 50, 0, 255, 0}), 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.mac.Nbrep(); got != tt.want {
				t.Errorf("linkadrreq.Nbrep() = %v, want %v", got, tt.want)
			}
		})
	}
}
