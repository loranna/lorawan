package v1_0_0

//Uplink represents a decrypted uplink message
type Uplink interface {
	Encrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (UplinkEncrypted, error)

	Mtype() MType
	Major() Major
	Devaddr() [4]byte
	Adr() bool
	AdrAckreq() bool
	Ack() bool
	FOptsLen() byte
	Fcnt() uint16
	Fopts() []byte
	Fport() *uint8
	Frmpayload() []byte
}

//UplinkEncrypted represents an encrypted uplink message
type UplinkEncrypted interface {
	Decrypt(nwkskey, appskey [16]byte, MSBofcnt, MSBminus1ofcnt byte) (Uplink, error)
	Bytes() []byte
	MICValid(key [16]byte, MSBofcnt, MSBminus1ofcnt byte) (bool, error)

	Mtype() MType
	Major() Major
	Devaddr() [4]byte
	Adr() bool
	AdrAckreq() bool
	Ack() bool
	FOptsLen() byte
	Fcnt() uint16
	Fopts() []byte
	Fport() *uint8
	Efrmpayload() []byte
	MIC() [4]byte
}
