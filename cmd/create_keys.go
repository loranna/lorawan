package cmd

import (
	"encoding/hex"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	createCmd.AddCommand(keysCmd)
}

var keysCmd = &cobra.Command{
	Use:     "keys appnonce devnonce netid appkey",
	Short:   "Create LoRaWAN keys",
	Example: "loranna create keys 289696 4bae 000024 36E39083CFFDAC18F5727BB004160F58",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 4 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			appnonceb, err := hex.DecodeString(args[0])
			if err != nil {
				return fmt.Errorf("could not parse appnonce: %v", err)
			}
			if l := len(appnonceb); l != 3 {
				return fmt.Errorf("invalid input appnonce: %s, input length %d", args[0], l)
			}
			var appnonce [3]byte
			copy(appnonce[:], appnonceb)

			devnonceb, err := hex.DecodeString(args[1])
			if err != nil {
				return fmt.Errorf("could not parse devnonce: %v", err)
			}
			if l := len(devnonceb); l != 2 {
				return fmt.Errorf("invalid input devnonce: %s, input length %d", args[1], l)
			}
			var devnonce [2]byte
			copy(devnonce[:], devnonceb)

			netidb, err := hex.DecodeString(args[2])
			if err != nil {
				return fmt.Errorf("could not parse netid: %v", err)
			}
			if l := len(netidb); l != 3 {
				return fmt.Errorf("invalid input netid: %s, input length %d", args[2], l)
			}
			var netid [3]byte
			copy(netid[:], netidb)

			appkeyb, err := hex.DecodeString(args[3])
			if err != nil {
				return fmt.Errorf("could not parse appkey: %v", err)
			}
			if l := len(appkeyb); l != 16 {
				return fmt.Errorf("invalid input appkey: %s, input length %d", args[3], l)
			}
			var appkey [16]byte
			copy(appkey[:], appkeyb)

			nwkskey, appskey, err := create.Keys(appkey, appnonce, netid, devnonce)
			if err != nil {
				return err
			}

			cmd.Printf("nwkskey: %X\n", nwkskey)
			cmd.Printf("appskey: %X\n", appskey)
		default:
			return fmt.Errorf("unimplemented LoRaWAN version: %s", lorawan)
		}
		return nil
	},
}
