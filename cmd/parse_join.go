package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	parseCmd.AddCommand(pjoinCmd)
}

var pjoinCmd = &cobra.Command{
	Use:   "join",
	Short: "parse a join related LoRaWAN message",
}
