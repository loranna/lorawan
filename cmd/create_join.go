package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	createCmd.AddCommand(joinCmd)
}

var joinCmd = &cobra.Command{
	Use:   "join",
	Short: "Create a LoRaWAN join message",
}
