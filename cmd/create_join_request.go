package cmd

import (
	"encoding/hex"
	"fmt"
	"math/rand"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	joinCmd.AddCommand(requestCmd)
}

var requestCmd = &cobra.Command{
	Use:     "request deveui appeui appkey [devnonce]",
	Short:   "Create a LoRaWAN join request message",
	Example: "loranna create join request BE7A000000002D26 BE7A000000001A12 7B50A7A791A5F116533A3B5050BBB829 5D0F",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 3 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			deveuib, err := hex.DecodeString(args[0])
			if err != nil {
				return fmt.Errorf("could not parse deveui: %v", err)
			}
			if l := len(deveuib); l != 8 {
				return fmt.Errorf("invalid input deveui: %s, input length %d", args[2], l)
			}
			var deveui [8]byte
			copy(deveui[:], deveuib)

			appeuib, err := hex.DecodeString(args[1])
			if err != nil {
				return fmt.Errorf("could not parse appeui: %v", err)
			}
			if l := len(appeuib); l != 8 {
				return fmt.Errorf("invalid input appeui: %s, input length %d", args[2], l)
			}
			var appeui [8]byte
			copy(appeui[:], appeuib)

			var devnonce [2]byte
			if len(args) > 3 {
				devnonceb, err := hex.DecodeString(args[3])
				if err != nil {
					return fmt.Errorf("could not parse devnonce: %v", err)
				}
				if l := len(devnonceb); l != 2 {
					return fmt.Errorf("invalid input devnonce: %s, input length %d", args[2], l)
				}
				copy(devnonce[:], devnonceb)
			} else {
				number := rand.Int31()
				devnonce[0] = byte(number)
				devnonce[1] = byte(number >> 8)
			}

			appkeyb, err := hex.DecodeString(args[2])
			if err != nil {
				return fmt.Errorf("could not parse appkey: %v", err)
			}
			if l := len(appkeyb); l != 16 {
				return fmt.Errorf("invalid input appkey: %s, input length %d", args[2], l)
			}
			var appkey [16]byte
			copy(appkey[:], appkeyb)
			jr, err := create.JoinRequest(appeui, deveui, devnonce, appkey)
			if err != nil {
				return err
			}

			cmd.Printf("%s\n", jr)
		default:
			return fmt.Errorf("unimplemented LoRaWAN version: %s", lorawan)
		}
		return nil
	},
}
