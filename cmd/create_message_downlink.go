package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	messageCmd.AddCommand(downlinkCmd)
}

var downlinkCmd = &cobra.Command{
	Use:     "downlink devaddr adr adrackreq ack fpending fcnt [port payload]",
	Short:   "Create a LoRaWAN downlink message",
	Example: "loranna create message downlink  --fopts 06 --nwkskey F76D0BA8DADB6847EA274F0225997007 --appskey FB702E6D8EB49EEBFBBA7DAD05086916 49a4ca03 false false false false 45 7 7777",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 6 {
			return fmt.Errorf("need more arguments")
		}
		if len(args) == 7 {
			return fmt.Errorf("if port is set, payload must be set as well")
		}
		switch lorawan {
		case "v1.0.0":
			devaddrs, err := hex.DecodeString(args[0])
			if err != nil {
				return err
			}
			var devaddr [4]byte
			copy(devaddr[:], devaddrs)
			adr, err := strconv.ParseBool(args[1])
			if err != nil {
				return err
			}
			adrackreq, err := strconv.ParseBool(args[2])
			if err != nil {
				return err
			}
			ack, err := strconv.ParseBool(args[3])
			if err != nil {
				return err
			}
			fpending, err := strconv.ParseBool(args[4])
			if err != nil {
				return err
			}
			fcnt, err := strconv.ParseUint(args[5], 10, 16)
			if err != nil {
				return err
			}
			var port *uint8
			var payload []byte
			if len(args) > 6 {
				var err error
				p, err := strconv.ParseUint(args[6], 10, 8)
				if err != nil {
					return err
				}
				pp := byte(p)
				port = &pp

				payload, err = hex.DecodeString(args[7])
				if err != nil {
					return err
				}
				if appskey == nil {
					return fmt.Errorf("appskey flag must be set to encrypt the message")
				}
			}
			down, err := create.Downlink(confirmed, devaddr, adr, adrackreq, ack, fpending, uint16(fcnt), fopts, port, payload, nil)
			if err != nil {
				return err
			}
			var nskey [16]byte
			copy(nskey[:], nwkskey)
			var askey [16]byte
			copy(askey[:], appskey)

			encdata, err := down.Encrypt(nskey, askey, mSBofcnt, mSBminus1ofcnt)
			if err != nil {
				return err
			}
			cmd.Printf("binary representation encrypted:     %X\n", encdata.Bytes())
			cmd.Printf("base64 representation encrypted:     %s\n", base64.StdEncoding.EncodeToString(encdata.Bytes()))
		}
		return nil
	},
}
