package cmd

import (
	"bytes"
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

func TestCommand(t *testing.T) {
	tests := []struct {
		name    string
		args    []string
		want    string
		wantErr bool
	}{
		{
			"create cflist",
			[]string{"create", "cflist", "867100000", "867300000", "867500000", "867700000", "867900000"},
			"CFList: 184F84E85684B85E84886684586E8400",
			false,
		},
		{
			"create join accept",
			[]string{"create", "join", "accept", "000024", "04050607", "00", "00", "00", "02020202020202020202020202020202", "030303"},
			`Message Type: TypeJoinAccept
Major: MajorLoRaWANR1
AppNonce: 030303
NetID: 000024
DevAddr: 04050607
RX1DRoffset: 0
RX2 Data rate: 0
RxDelay: 0
CFList: none
Mic: 9519F0B1
binary representation non-encrypted: 200303032400000706050400009519F0B1
base64 representation non-encrypted: IAMDAyQAAAcGBQQAAJUZ8LE=
binary representation encrypted:     20C33E292CE14D074768F26DD10084CEFB
base64 representation encrypted:     IMM+KSzhTQdHaPJt0QCEzvs=`,
			false,
		},
		{
			"create join request",
			[]string{"create", "join", "request", "BE7A000000002D26", "BE7A000000001A12", "7B50A7A791A5F116533A3B5050BBB829", "5D0F"},
			`Message Type: TypeJoinRequest
Major: MajorLoRaWANR1
DevEUI: BE7A000000002D26
AppEUI: BE7A000000001A12
DevNonce: 5D0F
Mic: 44441A3A
binary representation: 00121A000000007ABE262D000000007ABE0F5D44441A3A
base64 representation: ABIaAAAAAHq+Ji0AAAAAer4PXUREGjo=`,
			false,
		},
		{
			"create join request",
			[]string{"create", "join", "request", "BE7A000000002D26", "BE7A000000001A12"},
			"need more arguments",
			true,
		},
		{
			"create newchannelans",
			[]string{"create", "mac", "newchannelans", "true", "true"},
			`0703`,
			false,
		},
		{
			"create newchannelreq",
			[]string{"create", "mac", "newchannelreq", "2", "868500000", "0", "5"},
			`0702C8858405`,
			false,
		},
		{
			"create keys",
			[]string{"create", "keys", "289696", "4bae", "000024", "36E39083CFFDAC18F5727BB004160F58"},
			`nwkskey: B7A5AECD8D6C8501BB06CA84ABC88BA9
appskey: 43BB52D51119C8A5820690E69EB130C8`,
			false,
		},
		{
			"create downlink",
			[]string{"create", "message", "downlink", "--fopts", "06", "49a4ca03", "false", "false", "false", "false", "45", "7", "7777", "--nwkskey", "F76D0BA8DADB6847EA274F0225997007", "--appskey", "FB702E6D8EB49EEBFBBA7DAD05086916"},
			`binary representation encrypted:     6003CAA449012D000607A7EE4F4B0108
base64 representation encrypted:     YAPKpEkBLQAGB6fuT0sBCA==`,
			false,
		},
		{
			"create uplink",
			strings.Split("create message uplink 03030404 false false false 45 7 7766 --fopts 020304 --nwkskey 03030303030303030303030303030303 --appskey 040404040404040404040404040404", " "),
			`binary representation encrypted:     4004040303032D0002030407964FFBF3D4C7
base64 representation encrypted:     QAQEAwMDLQACAwQHlk/789TH`,
			false,
		},
		{
			"parse downlink",
			strings.Split("parse downlink A003CAA449010100060759B9DBDA3D08 F76D0BA8DADB6847EA274F0225997007 FB702E6D8EB49EEBFBBA7DAD05086916", " "),
			`MIC valid
Message Type: TypeConfirmedDataDown
Major: MajorLoRaWANR1
DevAddr: 49A4CA03
ADR: false
ADRACKReq: false
ACK: false
FPending: false
FCnt: 1
FOpts: 06
FOpts length: 1
Port: 7
FRMPayload: 7777
binary representation non-encrypted: A003CAA44901010006077777
base64 representation non-encrypted: oAPKpEkBAQAGB3d3`,
			false,
		},
		{
			"parse uplink",
			strings.Split("parse uplink 4002436f48000000077fC5918EB4 B599E2938237B23E858E6E7D0BB61538 9A9DFC50EE344E76D125122FF8D1346E", " "),
			`MIC valid
Message Type: TypeUnconfirmedDataUp
Major: MajorLoRaWANR1
DevAddr: 486F4302
ADR: false
ADRACKReq: false
ACK: false
FCnt: 0
FOpts: 
FOpts length: 0
Port: 7
FRMPayload: 77
binary representation non-encrypted: 4002436F480000000777
base64 representation non-encrypted: QAJDb0gAAAAHdw==`,
			false,
		},
		{
			"parse join request",
			strings.Split("parse join request 00121a000000007abe262d000000007abe0f5d44441A3A 7B50A7A791A5F116533A3B5050BBB829", " "),
			`Message Type: TypeJoinRequest
Major: MajorLoRaWANR1
DevEUI: BE7A000000002D26
AppEUI: BE7A000000001A12
DevNonce: 5D0F
Mic: 44441A3A
binary representation: 00121A000000007ABE262D000000007ABE0F5D44441A3A
base64 representation: ABIaAAAAAHq+Ji0AAAAAer4PXUREGjo=`,
			false,
		},
		{
			"parse join accept",
			strings.Split("parse join accept IEWEHDhRUIfXLRacPrMBf2H8+y6A6IfPrTeYnZf6cEtd 7B50A7A791A5F116533A3B5050BBB829 -b", " "),
			`Message Type: TypeJoinAccept
Major: MajorLoRaWANR1
AppNonce: F69C38
NetID: 000024
DevAddr: 492D8367
RX1DRoffset: 0
RX2 Data rate: 0
RxDelay: 0
CFList: 184f84e85684b85e84886684586e8400 parsed: 867.100 MHz, 867.300 MHz, 867.500 MHz, 867.700 MHz, 867.900 MHz
Mic: DA09CF00
binary representation non-encrypted: 20389CF624000067832D490000184F84E85684B85E84886684586E8400DA09CF00
base64 representation non-encrypted: IDic9iQAAGeDLUkAABhPhOhWhLhehIhmhFhuhADaCc8A`,
			false,
		},
		{
			"parse join accept2",
			strings.Split("parse join accept 2045841c38515087d72d169c3eb3017f61fcfb2e80e887cfad37989d97fa704b5d 7B50A7A791A5F116533A3B5050BBB829", " "),
			`Message Type: TypeJoinAccept
Major: MajorLoRaWANR1
AppNonce: F69C38
NetID: 000024
DevAddr: 492D8367
RX1DRoffset: 0
RX2 Data rate: 0
RxDelay: 0
CFList: 184f84e85684b85e84886684586e8400 parsed: 867.100 MHz, 867.300 MHz, 867.500 MHz, 867.700 MHz, 867.900 MHz
Mic: DA09CF00
binary representation non-encrypted: 20389CF624000067832D490000184F84E85684B85E84886684586E8400DA09CF00
base64 representation non-encrypted: IDic9iQAAGeDLUkAABhPhOhWhLhehIhmhFhuhADaCc8A`,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := executeCommand(rootCmd, tt.args[:]...)
			if tt.wantErr {
				if err != nil{
					if strings.TrimSpace(err.Error())!=strings.TrimSpace(tt.want){
					t.Errorf("Result error = %s, want %s", strings.TrimSpace(err.Error()), tt.want)}
				} else{
					t.Errorf("Error wanted but err is nil")
				}
			} else {
				got = strings.TrimSpace(got)
				if !reflect.DeepEqual(got, strings.TrimSpace(tt.want)) {
					t.Errorf("Result = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func reset(f *pflag.Flag) {
	if f.Changed {
		f.Value.Set(f.DefValue)
	}
}

func executeCommand(root *cobra.Command, args ...string) (string, error) {
	cmd := root

	for _, arg := range args {
		flags := cmd.Flags()
		flags.VisitAll(reset)
		if cmd.HasSubCommands() {
			cmd = findsubcommand(cmd, arg)
		} else {
			break
		}
	}
	buf := new(bytes.Buffer)
	root.SetOutput(buf)
	root.SetArgs(args)
	err := root.Execute()
	return buf.String(), err
}

func findsubcommand(cmd *cobra.Command, arg string) *cobra.Command {
	cmds := cmd.Commands()
	for _, cmd := range cmds {
		if cmd.Name() == arg {
			return cmd
		}
	}
	return nil
}
