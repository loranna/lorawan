package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/parse"
)

func init() {
	parseCmd.AddCommand(pupCmd)
}

var pupCmd = &cobra.Command{
	Use:     "uplink message nwkskey [appskey] [msbofcnt] [msbminus1ofcnt]",
	Short:   "parse an uplink LoRaWAN message",
	Example: "loranna parse uplink 4004040303032D0002030407964FFBF3D4C7 03030303030303030303030303030303 040404040404040404040404040404",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 3 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			var message []byte
			if base64m {
				var err error
				message, err = base64.StdEncoding.DecodeString(args[0])
				if err != nil {
					return err
				}
			} else {
				var err error
				message, err = hex.DecodeString(args[0])
				if err != nil {
					return err
				}
			}
			nskey, err := hex.DecodeString(args[1])
			if err != nil {
				return err
			}
			nwkskey := [16]byte{}
			copy(nwkskey[:], nskey)
			askey, err := hex.DecodeString(args[2])
			if err != nil {
				return err
			}
			appskey := [16]byte{}
			copy(appskey[:], askey)
			ofcnt := uint64(0)
			if len(args) > 3 {
				ofcnt, err = strconv.ParseUint(args[3], 10, 8)
				if err != nil {
					return err
				}
			}
			minus1ofcnt := uint64(0)
			if len(args) > 4 {
				minus1ofcnt, err = strconv.ParseUint(args[4], 10, 8)
				if err != nil {
					return err
				}
			}
			up, err := parse.UplinkEncrypted(message)
			if err != nil {
				return err
			}

			valid, err := up.MICValid(nwkskey, byte(ofcnt), byte(minus1ofcnt))
			if err != nil {
				return err
			}
			
			if valid {
				cmd.Println("MIC valid")
			} else {
				cmd.Println("MIC invalid")
			}
			if len(args) > 2 {
				askey, err := hex.DecodeString(args[2])
				if err != nil {
					return err
				}
				appskey := [16]byte{}
				copy(appskey[:], askey)
				d, err := up.Decrypt(nwkskey, appskey, byte(ofcnt), byte(minus1ofcnt))
				if err != nil {
					return err
				}
				cmd.Printf("%s", d)
			} else if valid {
				cmd.Printf("%s", up)
			}
		}
		return nil
	},
}
