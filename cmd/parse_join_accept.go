package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"github.com/spf13/cobra"
	r "gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/v1_0_0/parse"
)

func init() {
	pjoinCmd.AddCommand(pacceptCmd)
}

var pacceptCmd = &cobra.Command{
	Use:   "accept message appkey",
	Short: "parses a join request LoRaWAN message",
	Example: `loranna parse join accept IEWEHDhRUIfXLRacPrMBf2H8+y6A6IfPrTeYnZf6cEtd 7B50A7A791A5F116533A3B5050BBB829 -b
loranna parse join accept 2045841c38515087d72d169c3eb3017f61fcfb2e80e887cfad37989d97fa704b5d 7B50A7A791A5F116533A3B5050BBB829`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			var jabytes []byte
			if base64m {
				var err error

				jabytes, err = base64.StdEncoding.DecodeString(args[0])
				if err != nil {
					return err
				}
			} else {
				var err error
				jabytes, err = hex.DecodeString(args[0])
				if err != nil {
					return err
				}
			}
			if len(jabytes) != 17 && len(jabytes) != 33 {
				return fmt.Errorf("join accept message must be 17 or 33 bytes long but received length %d", len(jabytes))
			}

			var keybytes [16]byte
			keyb, err := hex.DecodeString(args[1])
			if err != nil {
				return err
			}
			if len(keyb) != 16 {
				return fmt.Errorf("key must be 16 bytes long but received length %d", len(keyb))
			}
			copy(keybytes[:], keyb[:])
			regi := r.Parse(region)
			ja, err := parse.JoinAcceptEncrypted(jabytes, keybytes, regi)
			if err != nil {
				return err
			}
			cmd.Printf("%s\n", ja)
		default:
			return fmt.Errorf("unimplemented LoRaWAN version: %s", lorawan)
		}
		return nil
	},
}
