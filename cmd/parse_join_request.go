package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/parse"
)

func init() {
	pjoinCmd.AddCommand(prequestCmd)
}

var prequestCmd = &cobra.Command{
	Use:     "request message appkey",
	Short:   "parses a join request LoRaWAN message",
	Example: "loranna parse join request 00121a000000007abe262d000000007abe0f5d44441A3A 7B50A7A791A5F116533A3B5050BBB829",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			var jrbytes [23]byte
			var jrb []byte
			if base64m {
				var err error
				jrb, err = base64.StdEncoding.DecodeString(args[0])
				if err != nil {
					return err
				}
			} else {
				var err error
				jrb, err = hex.DecodeString(args[0])
				if err != nil {
					return err
				}
			}
			if len(jrb) != 23 {
				return fmt.Errorf("join request message must be 23 bytes long but received length %d", len(jrb))
			}
			copy(jrbytes[:], jrb[:])

			var keybytes [16]byte
			keyb, err := hex.DecodeString(args[1])
			if err != nil {
				return err
			}
			if len(keyb) != 16 {
				return fmt.Errorf("key must be 16 bytes long but received length %d", len(keyb))
			}
			copy(keybytes[:], keyb[:])
			jr, err := parse.JoinRequest(jrbytes, keybytes)
			if err != nil {
				return err
			}
			cmd.Printf("%s\n", jr)
		default:
			return fmt.Errorf("unimplemented LoRaWAN version: %s", lorawan)
		}
		return nil
	},
}
