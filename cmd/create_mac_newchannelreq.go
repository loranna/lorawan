package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	macCmd.AddCommand(newchannelreqCmd)
}

var newchannelreqCmd = &cobra.Command{
	Use:     "newchannelreq chindex frequency mindr maxdr",
	Short:   "Create a LoRaWAN newchannelreq mac command",
	Example: "loranna create newchannelreq 2 868500000 0 5",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 4 {
			return fmt.Errorf("need more arguments")
		}
		chindex, err := strconv.ParseInt(args[0], 10, 32)
		if err != nil {
			return err
		}
		frequency, err := strconv.ParseInt(args[1], 10, 32)
		if err != nil {
			return err
		}
		mindr, err := strconv.ParseInt(args[2], 10, 32)
		if err != nil {
			return err
		}
		maxdr, err := strconv.ParseInt(args[3], 10, 32)
		if err != nil {
			return err
		}
		fopt, err := create.NewChannelReq(uint8(chindex), uint32(frequency), uint8(mindr), uint8(maxdr))
		if err != nil {
			return err
		}
		f, err := v1_0_0.Fopts([]v1_0_0.FrameOption{fopt})
		if err != nil {
			return err
		}
		cmd.Printf("%X\n", f)
		return nil
	},
}
