package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	r "gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

var cflist string

func init() {
	acceptCmd.Flags().StringVar(&cflist, "cflist", "", "cflist of a join accept")
	viper.BindPFlag("cflist", acceptCmd.Flags().Lookup("cflist"))
	joinCmd.AddCommand(acceptCmd)
}

var acceptCmd = &cobra.Command{
	Use:     "accept netid devaddr rx1droffset rx2datarate rxdelay key [appnonce]",
	Short:   "Create a LoRaWAN join accept message",
	Example: "loranna create join accept 000024 04050607 00 00 00 02020202020202020202020202020202 030303",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 6 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			netidb, err := hex.DecodeString(args[0])
			if err != nil {
				return fmt.Errorf("badly formatted netid: %s", args[0])
			}
			var netid [3]byte
			copy(netid[:], netidb)

			devaddrb, err := hex.DecodeString(args[1])
			if err != nil {
				return fmt.Errorf("badly formatted devaddr: %s", args[1])
			}
			var devaddr [4]byte
			copy(devaddr[:], devaddrb)

			rx1droffsetstr := args[2]
			if len(rx1droffsetstr) == 1 {
				rx1droffsetstr = "0" + rx1droffsetstr
			}
			rx1droffset, err := hex.DecodeString(rx1droffsetstr)
			if err != nil {
				return fmt.Errorf("badly formatted rx1droffset: %s", rx1droffsetstr)
			}

			rx2dataratestr := args[3]
			if len(rx2dataratestr) == 1 {
				rx2dataratestr = "0" + rx2dataratestr
			}
			rx2datarate, err := hex.DecodeString(rx2dataratestr)
			if err != nil {
				return fmt.Errorf("badly formatted rx2datarate: %s", rx2dataratestr)
			}

			rxdelaystr := args[4]
			if len(rxdelaystr) == 1 {
				rxdelaystr = "0" + rxdelaystr
			}
			rxdelay, err := hex.DecodeString(rxdelaystr)
			if err != nil {
				return fmt.Errorf("badly formatted rxdelay: %s", rxdelaystr)
			}

			var appnonce [3]byte
			if len(args) > 6 {
				appnonceb, err := hex.DecodeString(args[6])
				if err != nil {
					return fmt.Errorf("badly formatted appnonce: %s", args[6])
				}
				copy(appnonce[:], appnonceb)
			} else {
				number := rand.Int31()
				appnonce[0] = byte(number)
				appnonce[1] = byte(number >> 8)
				appnonce[2] = byte(number >> 16)
			}
			cflistbytes := &[16]byte{}
			if cflist != "" {
				cflistb, err := hex.DecodeString(cflist)
				if err != nil {
					return fmt.Errorf("badly formatted cflist: %s", args[5])
				}
				copy((*cflistbytes)[:], cflistb)
			} else {
				cflistbytes = nil
			}

			appkeyb, err := hex.DecodeString(args[5])
			if err != nil {
				return fmt.Errorf("badly formatted appkey: %s", args[5])
			}
			var appkey [16]byte
			copy(appkey[:], appkeyb)
			regi := r.Parse(region)
			ja, err := create.JoinAccept(appnonce, netid, devaddr, rx1droffset[0], rx2datarate[0], rxdelay[0], cflistbytes, nil, appkey, regi)
			if err != nil {
				return err
			}
			cmd.Print(ja)
			encdata, err := ja.Encrypt(appkey)
			if err != nil {
				return err
			}
			cmd.Printf("binary representation encrypted:     %X\n", encdata.Bytes())
			cmd.Printf("base64 representation encrypted:     %s\n", base64.StdEncoding.EncodeToString(encdata.Bytes()))
		default:
			return fmt.Errorf("unimplemented LoRaWAN version: %s", lorawan)
		}
		return nil
	},
}
