package cmd

import (
	"github.com/spf13/cobra"
)

var split bool

func init() {
	macCmd.PersistentFlags().BoolVarP(&split, "split", "s", false, "fopts are split for each mac command")
	createCmd.AddCommand(macCmd)
}

var macCmd = &cobra.Command{
	Use:   "mac",
	Short: "Create a LoRaWAN MAC command",
}
