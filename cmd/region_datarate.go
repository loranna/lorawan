package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/region/v1_0_0/CN779_787"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU433"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
	"gitlab.com/loranna/lorawan/region/v1_0_0/US902_928"
)

func init() {
	regionCmd.AddCommand(datarateCmd)
}

var datarateCmd = &cobra.Command{
	Use:     "datarate [id]",
	Short:   "shows datarate",
	Example: "loranna region datarate 0",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			switch regionversion {
			case "v1.0.0":
				switch region {
				case "EU863_870":
					datarates := EU863_870.DataRates()
					for i, datarate := range datarates {
						cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
							i, datarate.SpreadingFactor, datarate.Bandwidth)
					}
				case "US902_928":
					datarates := US902_928.DataRates()
					for i, datarate := range datarates {
						cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
							i, datarate.SpreadingFactor, datarate.Bandwidth)
					}
				case "CN779_787":
					datarates := CN779_787.DataRates()
					for i, datarate := range datarates {
						cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
							i, datarate.SpreadingFactor, datarate.Bandwidth)
					}
				case "EU433":
					datarates := EU433.DataRates()
					for i, datarate := range datarates {
						cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
							i, datarate.SpreadingFactor, datarate.Bandwidth)
					}
				default:
					return fmt.Errorf("unknown region: %s", region)
				}
			default:
				return fmt.Errorf("unknown region version: %s", regionversion)
			}
		} else {
			switch regionversion {
			case "v1.0.0":
				switch region {
				case "EU863_870":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					datarate, err := EU863_870.DataRate2SFBW(uint8(i))
					if err != nil {
						return err
					}
					cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
						i, datarate.SpreadingFactor, datarate.Bandwidth)
				case "US902_928":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					datarate, err := US902_928.DataRate2SFBW(uint8(i))
					if err != nil {
						return err
					}
					cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
						i, datarate.SpreadingFactor, datarate.Bandwidth)
				case "CN779_787":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					datarate, err := CN779_787.DataRate2SFBW(uint8(i))
					if err != nil {
						return err
					}
					cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
						i, datarate.SpreadingFactor, datarate.Bandwidth)
				case "EU433":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					datarate, err := EU433.DataRate2SFBW(uint8(i))
					if err != nil {
						return err
					}
					cmd.Printf("Datarate: %2d, Spreading Factor: %2d, Bandwidth: %6d Hz\n",
						i, datarate.SpreadingFactor, datarate.Bandwidth)
				default:
					return fmt.Errorf("unknown region: %s", region)
				}
			default:
				return fmt.Errorf("unknown region version: %s", regionversion)
			}
		}
		return nil
	},
}
