package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

func init() {
	rootCmd.AddCommand(generatedocCmd)
}

var generatedocCmd = &cobra.Command{
	Use:    "generatedoc",
	Short:  "generate docs",
	Hidden: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		err := doc.GenMarkdownTree(rootCmd, "./docs")
		if err != nil {
			return err
		}
		err = rootCmd.GenBashCompletionFile("completions/bash.sh")
		if err != nil {
			return err
		}

		files, err := ioutil.ReadDir("docs")
		if err != nil {
			return err
		}
		readme, err := os.Create("available_commands.md")
		if err != nil {
			return err
		}
		readme.WriteString("# Available commands\n\n")
		for _, f := range files {
			filenamenoext := strings.TrimSuffix(f.Name(), filepath.Ext(f.Name()))
			readme.WriteString(fmt.Sprintf("* [%s](docs/%s)\n",
				strings.ReplaceAll(filenamenoext, "_", " "), f.Name()))
		}
		readme.Close()

		return nil
	},
}
