package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/region/v1_0_0/CN779_787"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU433"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
	"gitlab.com/loranna/lorawan/region/v1_0_0/US902_928"
)

func init() {
	regionCmd.AddCommand(txpowerCmd)
}

var txpowerCmd = &cobra.Command{
	Use:     "txpower [id]",
	Short:   "shows txpower",
	Example: "loranna region txpower 0",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			switch regionversion {
			case "v1.0.0":
				switch region {
				case "EU863_870":
					txpowers := EU863_870.Txpowers()
					for i, txpower := range txpowers {
						cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, txpower)
					}
				case "US902_928":
					txpowers := US902_928.Txpowers()
					for i, txpower := range txpowers {
						cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, txpower)
					}
				case "CN779_787":
					txpowers := CN779_787.Txpowers()
					for i, txpower := range txpowers {
						cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, txpower)
					}
				case "EU433":
					txpowers := EU433.Txpowers()
					for i, txpower := range txpowers {
						cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, txpower)
					}
				default:
					return fmt.Errorf("unknown region: %s", region)
				}
			default:
				return fmt.Errorf("unknown region version: %s", regionversion)
			}
		} else {
			switch regionversion {
			case "v1.0.0":
				switch region {
				case "EU863_870":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					power, err := EU863_870.Txpower2dBm(int(i))
					if err != nil {
						return err
					}
					cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, power)
				case "US902_928":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					power, err := US902_928.Txpower2dBm(int(i))
					if err != nil {
						return err
					}
					cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, power)
				case "CN779_787":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					power, err := CN779_787.Txpower2dBm(int(i))
					if err != nil {
						return err
					}
					cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, power)
				case "EU433":
					i, err := strconv.ParseInt(args[0], 10, 8)
					if err != nil {
						return err
					}
					power, err := EU433.Txpower2dBm(int(i))
					if err != nil {
						return err
					}
					cmd.Printf("Txpower: %2d, Power: %2d dBm\n", i, power)
				default:
					return fmt.Errorf("unknown region: %s", region)
				}
			default:
				return fmt.Errorf("unknown region version: %s", regionversion)
			}
		}
		return nil
	},
}
