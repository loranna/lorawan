package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/parse"
)

func init() {
	parseCmd.AddCommand(pdownCmd)
}

var pdownCmd = &cobra.Command{
	Use:     "downlink message nwkskey [appskey] [msbofcnt] [msbminus1ofcnt]",
	Short:   "parse an downlink LoRaWAN message",
	Example: "loranna parse downlink A003CAA449010100060759B9DBDA3D08 F76D0BA8DADB6847EA274F0225997007 FB702E6D8EB49EEBFBBA7DAD05086916",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("need more arguments")
		}
		switch lorawan {
		case "v1.0.0":
			var message []byte
			if base64m {
				var err error
				message, err = base64.StdEncoding.DecodeString(args[0])
				if err != nil {
					return err
				}
			} else {
				var err error
				message, err = hex.DecodeString(args[0])
				if err != nil {
					return err
				}
			}
			nskey, err := hex.DecodeString(args[1])
			if err != nil {
				return err
			}
			nwkskey := [16]byte{}
			copy(nwkskey[:], nskey)
			ofcnt := uint64(0)
			if len(args) > 3 {
				ofcnt, err = strconv.ParseUint(args[3], 10, 8)
				if err != nil {
					return err
				}
			}
			minus1ofcnt := uint64(0)
			if len(args) > 4 {
				minus1ofcnt, err = strconv.ParseUint(args[4], 10, 8)
				if err != nil {
					return err
				}
			}
			down, err := parse.DownlinkEncrypted(message)
			if err != nil {
				return err
			}

			valid, err := down.MICValid(nwkskey, byte(ofcnt), byte(minus1ofcnt))
			if err != nil {
				return err
			}
			if valid {
				cmd.Println("MIC valid")
			} else {
				cmd.Println("MIC invalid")
			}
			if len(args) > 2 {
				askey, err := hex.DecodeString(args[2])
				if err != nil {
					return err
				}
				appskey := [16]byte{}
				copy(appskey[:], askey)
				d, err := down.Decrypt(nwkskey, appskey, byte(ofcnt), byte(minus1ofcnt))
				if err != nil {
					return err
				}
				cmd.Printf("%s", d)
			} else if valid {
				cmd.Printf("%s", down)
			}
		}
		return nil
	},
}
