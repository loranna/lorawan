package cmd

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	messageCmd.AddCommand(uplinkCmd)
}

var uplinkCmd = &cobra.Command{
	Use:   "uplink devaddr adr adrackreq ack fcnt [port payload]",
	Short: "Create a LoRaWAN uplink message",
	Long: `Uplink (loranna create uplink) create a complete
LoRaWAN uplink message based on the supplied parameters`,
	Example: "loranna create message uplink 03030404 false false false 45 7 7766 --fopts 020304 --nwkskey 03030303030303030303030303030303 --appskey 040404040404040404040404040404",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 5 {
			return fmt.Errorf("need more arguments")
		}
		if len(args) == 6 {
			return fmt.Errorf("if port is set, payload must be set as well")
		}
		switch lorawan {
		case "v1.0.0":
			devaddrs, err := hex.DecodeString(args[0])
			if err != nil {
				return err
			}
			var devaddr [4]byte
			copy(devaddr[:], devaddrs)
			adr, err := strconv.ParseBool(args[1])
			if err != nil {
				return err
			}
			adrackreq, err := strconv.ParseBool(args[2])
			if err != nil {
				return err
			}
			ack, err := strconv.ParseBool(args[3])
			if err != nil {
				return err
			}
			fcnt, err := strconv.ParseUint(args[4], 10, 16)
			if err != nil {
				return err
			}
			var port *uint8
			var payload []byte
			if len(args) > 5 {
				var err error
				p, err := strconv.ParseUint(args[5], 10, 8)
				if err != nil {
					return err
				}
				pp := byte(p)
				port = &pp

				payload, err = hex.DecodeString(args[6])
				if err != nil {
					return err
				}
				if appskey == nil {
					return fmt.Errorf("appskey flag must be set to encrypt the message")
				}
			}
			up, err := create.Uplink(confirmed, devaddr, adr, adrackreq, ack, uint16(fcnt), fopts, port, payload, nil)
			if err != nil {
				return err
			}

			var nskey [16]byte
			copy(nskey[:], nwkskey)
			var askey [16]byte
			copy(askey[:], appskey)

			encdata, err := up.Encrypt(nskey, askey, mSBofcnt, mSBminus1ofcnt)
			if err != nil {
				return err
			}
			cmd.Printf("binary representation encrypted:     %X\n", encdata.Bytes())
			cmd.Printf("base64 representation encrypted:     %s\n", base64.StdEncoding.EncodeToString(encdata.Bytes()))
		}
		return nil
	},
}
