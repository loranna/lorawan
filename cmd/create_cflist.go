package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/region/v1_0_0/CN779_787"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU433"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
)

func init() {
	createCmd.AddCommand(cflistCmd)
}

var cflistCmd = &cobra.Command{
	Use:     "cflist freq4 freq5 freq6 freq7 freq8",
	Short:   "Create CFList",
	Example: "loranna create cflist 867100000 867300000 867500000 867700000 867900000 ",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 5 {
			return fmt.Errorf("5 arguments allowed only")
		}
		switch regionversion {
		case "v1.0.0":
			switch region {
			case "EU863_870":
				freqs := [5]uint64{}
				var err error
				for i, freq := range args {
					freqs[i], err = strconv.ParseUint(freq, 10, 32)
					if err != nil {
						return err
					}
				}
				cflist := EU863_870.CFList(uint32(freqs[0]), uint32(freqs[1]),
					uint32(freqs[2]), uint32(freqs[3]), uint32(freqs[4]))
				cmd.Printf("CFList: %X\n", cflist)
			case "US902_928":
				return fmt.Errorf("CFList is not supported for region US902_928")
			case "CN779_787":
				freqs := [5]uint64{}
				var err error
				for i, freq := range args {
					freqs[i], err = strconv.ParseUint(freq, 10, 32)
					if err != nil {
						return err
					}
				}
				cflist := CN779_787.CFList(uint32(freqs[0]), uint32(freqs[1]),
					uint32(freqs[2]), uint32(freqs[3]), uint32(freqs[4]))
				cmd.Printf("CFList: %X\n", cflist)
			case "EU433":
				freqs := [5]uint64{}
				var err error
				for i, freq := range args {
					freqs[i], err = strconv.ParseUint(freq, 10, 32)
					if err != nil {
						return err
					}
				}
				cflist := EU433.CFList(uint32(freqs[0]), uint32(freqs[1]),
					uint32(freqs[2]), uint32(freqs[3]), uint32(freqs[4]))
				cmd.Printf("CFList: %X\n", cflist)
			default:
				return fmt.Errorf("unknown region: %s", region)
			}
		default:
			return fmt.Errorf("unknown region version: %s", regionversion)
		}
		return nil
	},
}
