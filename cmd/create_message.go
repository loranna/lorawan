package cmd

import (
	"github.com/spf13/cobra"
)

var fopts []byte
var nwkskey []byte
var appskey []byte
var mSBofcnt uint8
var mSBminus1ofcnt uint8
var confirmed bool

func init() {
	messageCmd.PersistentFlags().BytesHexVar(&fopts, "fopts", nil, "fopts of the message")
	messageCmd.PersistentFlags().BytesHexVar(&nwkskey, "nwkskey", nil, "fopts of the message")
	messageCmd.MarkFlagRequired("nwkskey")
	messageCmd.PersistentFlags().BytesHexVar(&appskey, "appskey", nil, "appskey of the message")
	messageCmd.PersistentFlags().Uint8Var(&mSBofcnt, "msbofcnt", 0, "most significant byte of 4 bytes counter of the message")
	messageCmd.PersistentFlags().Uint8Var(&mSBminus1ofcnt, "msbminus1ofcnt", 0, "second most significant byte of 4 bytes counter of the message")
	messageCmd.PersistentFlags().BoolVar(&confirmed, "confirmed", false, "message should include confirmation request")
	createCmd.AddCommand(messageCmd)
}

var messageCmd = &cobra.Command{
	Use:   "message",
	Short: "Create a LoRaWAN message",
}
