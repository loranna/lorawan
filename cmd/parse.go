package cmd

import (
	"github.com/spf13/cobra"
)

var base64m bool

func init() {
	parseCmd.PersistentFlags().BoolVarP(&base64m, "base64", "b", false, "message is interpreted as base64 string")
	rootCmd.AddCommand(parseCmd)
}

var parseCmd = &cobra.Command{
	Use:   "parse",
	Short: "Parse a LoRaWAN message",
}
