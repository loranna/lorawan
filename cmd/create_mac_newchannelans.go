package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
)

func init() {
	macCmd.AddCommand(newchannelansCmd)
}

var newchannelansCmd = &cobra.Command{
	Use:     "newchannelans datarate channelfrequency",
	Short:   "Create a LoRaWAN newchannel mac command",
	Example: "loranna create mac newchannelsans 3 868500000",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 2 {
			return fmt.Errorf("need more arguments")
		}
		dr, err := strconv.ParseBool(args[0])
		if err != nil {
			return err
		}
		freq, err := strconv.ParseBool(args[1])
		if err != nil {
			return err
		}
		fopt, err := create.NewChannelAns(dr, freq)
		if err != nil {
			return err
		}
		f, err := v1_0_0.Fopts([]v1_0_0.FrameOption{fopt})
		if err != nil {
			return err
		}
		cmd.Printf("%X\n", f)
		return nil
	},
}
