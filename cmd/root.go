package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var lorawan, regionversion, region string

func init() {
	rootCmd.PersistentFlags().StringVar(&lorawan, "lorawan", "v1.0.0", "version of LoRaWAN protocol")
	viper.BindPFlag("lorawan", rootCmd.PersistentFlags().Lookup("lorawan"))
	rootCmd.PersistentFlags().StringVar(&region, "region", "EU863_870", "LoRaWAN region")
	viper.BindPFlag("region", rootCmd.PersistentFlags().Lookup("region"))
	rootCmd.PersistentFlags().StringVar(&regionversion, "regionversion", "v1.0.0", "version of LoRaWAN region")
	viper.BindPFlag("regionversion", rootCmd.PersistentFlags().Lookup("regionversion"))
}

var rootCmd = &cobra.Command{
	Use:           "loranna",
	Short:         "Loranna is an implementation of LoRaWAN protocol and a CLI to create and parse LoRaWAN messages",
	Long:          "Loranna is both a library and a CLI tool to create and parse messages according to the LoRaWAN specification governed by the LoRa Alliance.",
	SilenceErrors: false,
	SilenceUsage:  true,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
}
