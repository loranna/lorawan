package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(regionCmd)
}

var regionCmd = &cobra.Command{
	Use:   "region",
	Short: "region related features",
}
