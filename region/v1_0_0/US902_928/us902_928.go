package US902_928

import (
	"fmt"
	"math/bits"
	"math/rand"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func SyncWordLoRa() byte {
	return 0x34
}

func PreambleLengthLoRaSymbol() uint {
	return 8
}

func DefaultChannels() []region.Channel {
	return []region.Channel{
		{Frequency: 902_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 904_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 906_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 907_800_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 909_400_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 911_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 912_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 914_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
	}
}

func DefaultTxPower() int {
	return 20
}

var datarates [14]radio.DataRate = [14]radio.DataRate{
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 500_000},
	radio.DataRate{},
	radio.DataRate{},
	radio.DataRate{},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 500_000},
}

var txpowers [11]int = [11]int{
	30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10,
}

func DataRates() [14]radio.DataRate {
	return datarates
}

func DataRate2SFBW(datarate uint8) (radio.DataRate, error) {
	if !(0 <= datarate && datarate <= uint8(len(datarates)-1)) {
		return radio.DataRate{}, fmt.Errorf("illegal datarate: %d", datarate)
	}
	return datarates[datarate], nil
}

func SFBW2Datarate(mod radio.Modulation, sf uint8, bw uint32) (uint8, error) {
	for i, dr := range datarates {
		if dr.Bandwidth == bw && dr.Modulation == mod && dr.SpreadingFactor == sf {
			return uint8(i), nil
		}
	}
	return 0, fmt.Errorf("datarate is not found")
}

func Txpowers() [11]int {
	return txpowers
}

func Txpower2dBm(txpower int) (int, error) {
	if !(0 <= txpower && txpower <= (len(txpowers)-1)) {
		return 0, fmt.Errorf("illegal txpower: %d", txpower)
	}
	return txpowers[txpower], nil
}

var upfrequencies [72]uint32 = [...]uint32{
	902_300_000,
	902_500_000,
	902_700_000,
	902_900_000,
	903_100_000,
	903_300_000,
	903_500_000,
	903_700_000,
	903_900_000,
	904_100_000,
	904_300_000,
	904_500_000,
	904_700_000,
	904_900_000,
	905_100_000,
	905_300_000,
	905_500_000,
	905_700_000,
	905_900_000,
	906_100_000,
	906_300_000,
	906_500_000,
	906_700_000,
	906_900_000,
	907_100_000,
	907_300_000,
	907_500_000,
	907_700_000,
	907_900_000,
	908_100_000,
	908_300_000,
	908_500_000,
	908_700_000,
	908_900_000,
	909_100_000,
	909_300_000,
	909_500_000,
	909_700_000,
	909_900_000,
	910_100_000,
	910_300_000,
	910_500_000,
	910_700_000,
	910_900_000,
	911_100_000,
	911_300_000,
	911_500_000,
	911_700_000,
	911_900_000,
	912_100_000,
	912_300_000,
	912_500_000,
	912_700_000,
	912_900_000,
	913_100_000,
	913_300_000,
	913_500_000,
	913_700_000,
	913_900_000,
	914_100_000,
	914_300_000,
	914_500_000,
	914_700_000,
	914_900_000,
	903_000_000,
	904_600_000,
	906_200_000,
	907_800_000,
	909_400_000,
	911_000_000,
	912_600_000,
	914_200_000,
}

var downfrequecies [8]uint32 = [...]uint32{
	923_300_000,
	923_900_000,
	924_500_000,
	925_100_000,
	925_700_000,
	926_300_000,
	926_900_000,
	927_500_000,
}

func RX1(freq uint32, dr, rx1droffset uint8) (frequency uint32, datarate uint8) {
	for i, f := range upfrequencies {
		if freq == f {
			frequency = downfrequecies[i%8]
		}
	}
	if 0 <= dr && dr <= 4 {
		dr += 10
		if 8 >= dr-rx1droffset {
			datarate = 8
		} else if 13 <= dr-rx1droffset {
			datarate = 13
		} else {
			datarate = dr - rx1droffset
		}
	} else {
		if 8 >= dr-rx1droffset {
			datarate = 8
		} else if 13 <= dr-rx1droffset {
			datarate = 13
		} else {
			datarate = dr - rx1droffset
		}
	}
	return
}

func RX2() (frequency uint32, datarate uint8) {
	frequency = 923_300_000
	datarate = 8
	return
}

func RECEIVE_DELAY1() uint {
	return 1
}

func RECEIVE_DELAY2() uint {
	return 1 + RECEIVE_DELAY1()
}

func JOIN_ACCEPT_DELAY1() uint {
	return 5
}

func JOIN_ACCEPT_DELAY2() uint {
	return 6
}

func MAX_FCNT_GAP() uint {
	return 16384
}

func ADR_ACK_LIMIT() uint {
	return 64
}

func ADR_ACK_DELAY() uint {
	return 32
}

func ACK_TIMEOUT() float64 {

	return 1 + (rand.Float64() * 2)
}

func ChannelMaskCheck(definedchannels []byte, enabledchannels []byte, channelmask [2]byte, channelmaskctrl uint8) (bool, []byte, error) {
	if enabledchannels == nil {
		return false, nil, nil
	}
	switch channelmaskctrl {
	case 0:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[0] && m2 == enabledchannels[1] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 1:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[2] && m2 == enabledchannels[3] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 2:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[4] && m2 == enabledchannels[5] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 3:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[6] && m2 == enabledchannels[7] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 4:
		m1 := bits.Reverse8(channelmask[0])
		if m1 == enabledchannels[8] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 6:
		for i := 0; i < 8; i++ {
			if enabledchannels[i] != 0xFF {
				return false, enabledchannels, nil
			}
		}
		m1 := bits.Reverse8(channelmask[0])
		if m1 == enabledchannels[8] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	default:
		return false, nil, fmt.Errorf("invalid channel mask control: %d", channelmaskctrl)
	}
}

//M
var maxmacpayloadnofoptrepeater [14]uint = [...]uint{
	19, 61, 137, 250, 250, 0, 0, 0, 41, 117, 230, 230, 230, 230,
}

//N
var maxmacpayloadfoptrepeater [14]uint = [...]uint{
	11, 53, 129, 242, 242, 0, 0, 0, 33, 109, 222, 222, 222, 222,
}

//M
var maxmacpayloadnofoptnorepeater [14]uint = [...]uint{
	19, 61, 137, 250, 250, 0, 0, 0, 61, 137, 250, 250, 250, 250,
}

//N
var maxmacpayloadfoptnorepeater [14]uint = [...]uint{
	11, 53, 129, 242, 242, 0, 0, 0, 53, 129, 242, 242, 242, 242,
}

func MaxPayload(fopt, repeater bool) [14]uint {
	if fopt {
		if repeater {
			return maxmacpayloadfoptrepeater
		}
		return maxmacpayloadfoptnorepeater
	}
	if repeater {
		return maxmacpayloadnofoptrepeater
	}
	return maxmacpayloadnofoptnorepeater
}

func ParseCFList(cflist [16]byte) ([]region.Channel, error) {
	if cftype := cflist[15]; cftype != 0x01 {
		return nil, fmt.Errorf("cflisttype must be one but %X", cftype)
	}
	channels := []region.Channel{
		{Frequency: 902_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 902_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 904_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 905_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 906_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 907_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 908_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 909_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 910_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 911_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 912_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 913_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 914_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
		{Frequency: 903_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 904_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 906_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 907_800_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 909_400_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 911_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 912_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
		{Frequency: 914_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
	}
	for j, cfmask := range cflist {
		if j > 8 {
			break
		}
		for i := 0; i < 8; i++ {
			if (cfmask>>i)&0x01 != 0x01 {
				channels[(j*8)+i].Frequency = 0
			}
		}
	}
	return channels, nil
}
