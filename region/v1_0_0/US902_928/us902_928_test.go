package US902_928

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func TestSyncWordLoRa(t *testing.T) {
	tests := []struct {
		name string
		want byte
	}{
		{
			"default",
			0x34,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SyncWordLoRa(); got != tt.want {
				t.Errorf("SyncWordLoRa() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPreambleLengthLoRaSymbol(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PreambleLengthLoRaSymbol(); got != tt.want {
				t.Errorf("PreambleLengthLoRaSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultChannels(t *testing.T) {
	tests := []struct {
		name string
		want []region.Channel
	}{
		{
			"default",
			[]region.Channel{
				{Frequency: 902_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 902_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 902_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 902_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 904_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 904_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 904_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 904_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 904_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 905_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 905_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 905_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 905_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 905_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 906_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 906_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 906_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 906_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 906_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 907_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 907_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 907_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 907_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 907_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 908_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 908_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 908_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 908_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 908_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 909_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 909_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 909_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 909_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 909_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 910_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 910_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 910_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 910_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 910_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 911_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 911_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 911_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 911_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 911_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 912_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 912_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 912_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 912_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 912_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 913_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 913_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 913_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 913_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 913_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 914_100_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 914_300_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 914_500_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 914_700_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 914_900_000, MinimumDataRate: 0, MaximumDataRate: 3},
				{Frequency: 903_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 904_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 906_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 907_800_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 909_400_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 911_000_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 912_600_000, MinimumDataRate: 4, MaximumDataRate: 4},
				{Frequency: 914_200_000, MinimumDataRate: 4, MaximumDataRate: 4},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultChannels(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DefaultChannels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultTxPower(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			"default",
			20,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultTxPower(); got != tt.want {
				t.Errorf("DefaultTxPower() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRates(t *testing.T) {
	tests := []struct {
		name string
		want [14]radio.DataRate
	}{
		{
			"default",
			[14]radio.DataRate{
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 500_000},
				radio.DataRate{},
				radio.DataRate{},
				radio.DataRate{},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 500_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 500_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 500_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 500_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 500_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 500_000},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DataRates(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRates() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRate2SFBW(t *testing.T) {
	type args struct {
		datarate uint8
	}
	tests := []struct {
		name    string
		args    args
		want    radio.DataRate
		wantErr bool
	}{
		{
			"default",
			args{datarate: 3},
			radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DataRate2SFBW(tt.args.datarate)
			if (err != nil) != tt.wantErr {
				t.Errorf("DataRate2SFBW() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRate2SFBW() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpowers(t *testing.T) {
	tests := []struct {
		name string
		want [11]int
	}{
		{
			"default",
			[11]int{30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Txpowers(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Txpowers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpower2dBm(t *testing.T) {
	type args struct {
		txpower int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"default",
			args{txpower: 4},
			22,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Txpower2dBm(tt.args.txpower)
			if (err != nil) != tt.wantErr {
				t.Errorf("Txpower2dBm() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Txpower2dBm() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRX1(t *testing.T) {
	type args struct {
		freq        uint32
		dr          uint8
		rx1droffset uint8
	}
	tests := []struct {
		name          string
		args          args
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			args{903_300_000, 0, 1},
			926_300_000,
			9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX1(tt.args.freq, tt.args.dr, tt.args.rx1droffset)
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX1() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX1() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRX2(t *testing.T) {
	tests := []struct {
		name          string
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			923_300_000,
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX2()
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX2() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX2() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRECEIVE_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY1(); got != tt.want {
				t.Errorf("RECEIVE_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRECEIVE_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY2(); got != tt.want {
				t.Errorf("RECEIVE_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY1(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY2(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMAX_FCNT_GAP(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			16384,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MAX_FCNT_GAP(); got != tt.want {
				t.Errorf("MAX_FCNT_GAP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_LIMIT(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			64,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_LIMIT(); got != tt.want {
				t.Errorf("ADR_ACK_LIMIT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_DELAY(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			32,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_DELAY(); got != tt.want {
				t.Errorf("ADR_ACK_DELAY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestACK_TIMEOUT(t *testing.T) {
	tests := []struct {
		name string
		want float64
	}{
		{
			"min",
			1,
		},
		{
			"max",
			3,
		},
	}
	if got := ACK_TIMEOUT(); got < tests[0].want || got > tests[1].want {
		t.Errorf("ACK_TIMEOUT() = %v, want <%v and >%v", got, tests[0].want, tests[1].want)
	}
}

func TestChannelMaskCheck(t *testing.T) {
	type args struct {
		definedchannels []byte
		enabledchannels []byte
		channelmask     [2]byte
		channelmaskctrl uint8
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		want1   []byte
		wantErr bool
	}{
		{
			"default",
			args{nil, nil, [2]byte{0xFF, 0x00}, 0},
			false,
			nil,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := ChannelMaskCheck(tt.args.definedchannels, tt.args.enabledchannels, tt.args.channelmask, tt.args.channelmaskctrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("ChannelMaskCheck() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ChannelMaskCheck() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("ChannelMaskCheck() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestMaxPayload(t *testing.T) {
	type args struct {
		fopt     bool
		repeater bool
	}
	tests := []struct {
		name string
		args args
		want [14]uint
	}{
		{
			"default",
			args{false, false},
			[14]uint{19, 61, 137, 250, 250, 0, 0, 0, 61, 137, 250, 250, 250, 250},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MaxPayload(tt.args.fopt, tt.args.repeater); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MaxPayload() = %v, want %v", got, tt.want)
			}
		})
	}
}
