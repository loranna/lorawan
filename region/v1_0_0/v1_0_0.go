package v1_0_0

import (
	"fmt"
	"math"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
	"gitlab.com/loranna/lorawan/region/v1_0_0/US902_928"
)

func DefaultChannels(r region.Region) []region.Channel {
	switch r {
	case region.EU863_870:
		channels := EU863_870.DefaultChannels()
		return channels[:]
	case region.US902_928:
		channels := US902_928.DefaultChannels()
		return channels
	default:
		return nil
	}
}

func DefaultTxpower(r region.Region) int {
	switch r {
	case region.EU863_870:
		return EU863_870.DefaultTxPower()
	case region.US902_928:
		return US902_928.DefaultTxPower()
	default:
		return math.MinInt32
	}
}

func Datarate2sfbw(r region.Region, dr uint8) (radio.DataRate, error) {
	switch r {
	case region.EU863_870:
		return EU863_870.DataRate2SFBW(dr)
	case region.US902_928:
		return US902_928.DataRate2SFBW(dr)
	default:
		return radio.DataRate{}, fmt.Errorf("unhandled region %s", r)
	}
}

func TxpowerIndexToTxpower(r region.Region, txpower byte) (int, error) {
	switch r {
	case region.EU863_870:
		return EU863_870.Txpower2dBm(int(txpower))
	case region.US902_928:
		return US902_928.Txpower2dBm(int(txpower))
	default:
		return 0, fmt.Errorf("unhandled region %s", r)
	}
}
