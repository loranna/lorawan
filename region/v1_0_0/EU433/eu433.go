package EU433

import (
	"encoding/binary"
	"fmt"
	"math/bits"
	"math/rand"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/util"
)

func SyncWordLoRa() byte {
	return 0x34
}

func SyncWordGFSK() [3]byte {
	return [3]byte{0xC1, 0x94, 0xC1}
}

func PreambleLengthLoRaSymbol() uint {
	return 8
}

func PreambleLengthGFSKBytes() uint {
	return 5
}

func DefaultChannels() [3]region.Channel {
	return [3]region.Channel{
		region.Channel{Frequency: 433_175_000, MinimumDataRate: 0, MaximumDataRate: 5},
		region.Channel{Frequency: 433_375_000, MinimumDataRate: 0, MaximumDataRate: 5},
		region.Channel{Frequency: 433_575_000, MinimumDataRate: 0, MaximumDataRate: 5},
	}
}

func DefaultTxPower() int {
	return 10
}

var datarates [8]radio.DataRate = [8]radio.DataRate{
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 250_000},
	radio.DataRate{Modulation: radio.ModulationFSK, SpreadingFactor: 0, Bandwidth: 50_000},
}

var txpowers [6]int = [6]int{
	10, 7, 4, 1, -2, -5,
}

func DataRates() [8]radio.DataRate {
	return datarates
}

func DataRate2SFBW(datarate uint8) (radio.DataRate, error) {
	if !(0 <= datarate && datarate <= uint8(len(datarates)-1)) {
		return radio.DataRate{}, fmt.Errorf("illegal datarate: %d", datarate)
	}
	return datarates[datarate], nil
}

func SFBW2Datarate(mod radio.Modulation, sf uint8, bw uint32) (uint8, error) {
	for i, dr := range datarates {
		if dr.Bandwidth == bw && dr.Modulation == mod && dr.SpreadingFactor == sf {
			return uint8(i), nil
		}
	}
	return 0, fmt.Errorf("datarate is not found")
}

func Txpowers() [6]int {
	return txpowers
}

func Txpower2dBm(txpower int) (int, error) {
	if !(0 <= txpower && txpower <= (len(txpowers)-1)) {
		return 0, fmt.Errorf("illegal txpower: %d", txpower)
	}
	return txpowers[txpower], nil
}

func CFList(freq4, freq5, freq6, freq7, freq8 uint32) [16]byte {
	freqs := [5]uint32{
		freq4, freq5, freq6, freq7, freq8,
	}
	cflist := [16]byte{}
	for i := 0; i < 5; i++ {
		var bb [4]byte
		binary.LittleEndian.PutUint32(bb[:], uint32(freqs[i])/100)
		copy(cflist[i*3:], bb[:3])
	}
	return cflist
}

func ParseCFList(cflist [16]byte) (uint32, uint32, uint32, uint32, uint32, error) {
	if t := cflist[15]; t != 0 {
		return 0, 0, 0, 0, 0, fmt.Errorf("cflist type must be 0 but %d", t)
	}
	nums := [5]uint32{}
	for i := 0; i < 5; i++ {
		ii := i * 3
		bytes := util.Bytereverse(cflist[ii : ii+3])
		num := uint32(bytes[0])<<16 | uint32(bytes[1])<<8 | uint32(bytes[2])
		nums[i] = num * 100
	}
	return nums[0], nums[1], nums[2], nums[3], nums[4], nil
}

func RX1(freq uint32, dr, rx1droffset uint8) (frequency uint32, datarate uint8) {
	frequency = freq
	if rx1droffset > dr {
		datarate = 0
	} else {
		datarate = dr - rx1droffset
	}
	return
}

func RX2() (frequency uint32, datarate uint8) {
	frequency = 434_665_000
	datarate = 0
	return
}

func RECEIVE_DELAY1() uint {
	return 1
}

func RECEIVE_DELAY2() uint {
	return 1 + RECEIVE_DELAY1()
}

func JOIN_ACCEPT_DELAY1() uint {
	return 5
}

func JOIN_ACCEPT_DELAY2() uint {
	return 6
}

func MAX_FCNT_GAP() uint {
	return 16384
}

func ADR_ACK_LIMIT() uint {
	return 64
}

func ADR_ACK_DELAY() uint {
	return 32
}

func ACK_TIMEOUT() float64 {

	return 1 + (rand.Float64() * 2)
}

func ChannelMaskCheck(definedchannels []byte, enabledchannels []byte, channelmask [2]byte, channelmaskctrl uint8) (bool, []byte, error) {
	if enabledchannels == nil {
		return false, nil, nil
	}
	switch channelmaskctrl {
	case 0:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[0] && m2 == enabledchannels[1] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 6:
		return true, definedchannels, nil
	default:
		return false, nil, fmt.Errorf("invalid channel mask control: %d", channelmaskctrl)
	}
}

//M
var maxmacpayloadnofoptrepeater [8]uint = [...]uint{
	59, 59, 59, 123, 230, 230, 230, 230,
}

//N
var maxmacpayloadfoptrepeater [8]uint = [...]uint{
	51, 51, 51, 115, 222, 222, 222, 222,
}

//M
var maxmacpayloadnofoptnorepeater [8]uint = [...]uint{
	59, 59, 59, 123, 250, 250, 250, 250,
}

//N
var maxmacpayloadfoptnorepeater [8]uint = [...]uint{
	51, 51, 51, 115, 242, 242, 242, 242,
}

func MaxPayload(fopt, repeater bool) [8]uint {
	if fopt {
		if repeater {
			return maxmacpayloadfoptrepeater
		}
		return maxmacpayloadfoptnorepeater
	}
	if repeater {
		return maxmacpayloadnofoptrepeater
	}
	return maxmacpayloadnofoptnorepeater
}
