package EU433

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func TestSyncWordLoRa(t *testing.T) {
	tests := []struct {
		name string
		want byte
	}{
		{
			"default",
			0x34,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SyncWordLoRa(); got != tt.want {
				t.Errorf("SyncWordLoRa() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSyncWordGFSK(t *testing.T) {
	tests := []struct {
		name string
		want [3]byte
	}{
		{
			"default",
			[3]byte{0xC1, 0x94, 0xC1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SyncWordGFSK(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SyncWordGFSK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPreambleLengthLoRaSymbol(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PreambleLengthLoRaSymbol(); got != tt.want {
				t.Errorf("PreambleLengthLoRaSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPreambleLengthGFSKBytes(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PreambleLengthGFSKBytes(); got != tt.want {
				t.Errorf("PreambleLengthGFSKBytes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultChannels(t *testing.T) {
	tests := []struct {
		name string
		want [3]region.Channel
	}{
		{
			"default",
			[3]region.Channel{
				region.Channel{Frequency: 433_175_000, MinimumDataRate: 0, MaximumDataRate: 5},
				region.Channel{Frequency: 433_375_000, MinimumDataRate: 0, MaximumDataRate: 5},
				region.Channel{Frequency: 433_575_000, MinimumDataRate: 0, MaximumDataRate: 5},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultChannels(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DefaultChannels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultTxPower(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			"default",
			10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultTxPower(); got != tt.want {
				t.Errorf("DefaultTxPower() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRates(t *testing.T) {
	tests := []struct {
		name string
		want [8]radio.DataRate
	}{
		{
			"default",
			[8]radio.DataRate{
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 250_000},
				radio.DataRate{Modulation: radio.ModulationFSK, SpreadingFactor: 0, Bandwidth: 50_000},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DataRates(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRates() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRate2SFBW(t *testing.T) {
	type args struct {
		datarate uint8
	}
	tests := []struct {
		name    string
		args    args
		want    radio.DataRate
		wantErr bool
	}{
		{
			"default",
			args{datarate: 3},
			radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DataRate2SFBW(tt.args.datarate)
			if (err != nil) != tt.wantErr {
				t.Errorf("DataRate2SFBW() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRate2SFBW() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpowers(t *testing.T) {
	tests := []struct {
		name string
		want [6]int
	}{
		{
			"default",
			[6]int{10, 7, 4, 1, -2, -5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Txpowers(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Txpowers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpower2dBm(t *testing.T) {
	type args struct {
		txpower int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"default",
			args{txpower: 4},
			-2,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Txpower2dBm(tt.args.txpower)
			if (err != nil) != tt.wantErr {
				t.Errorf("Txpower2dBm() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Txpower2dBm() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCFList(t *testing.T) {
	type args struct {
		freq4 uint32
		freq5 uint32
		freq6 uint32
		freq7 uint32
		freq8 uint32
	}
	tests := []struct {
		name string
		args args
		want [16]byte
	}{
		{
			"default",
			args{
				867_100_000,
				867_300_000,
				867_500_000,
				867_700_000,
				867_900_000,
			},
			[16]byte{24, 79, 132, 232, 86, 132, 184, 94, 132, 136, 102, 132, 88, 110, 132, 0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CFList(tt.args.freq4, tt.args.freq5, tt.args.freq6, tt.args.freq7, tt.args.freq8); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CFList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseCFList(t *testing.T) {
	type args struct {
		cflist [16]byte
	}
	tests := []struct {
		name    string
		args    args
		want    uint32
		want1   uint32
		want2   uint32
		want3   uint32
		want4   uint32
		wantErr bool
	}{
		{
			"default",
			args{[16]byte{24, 79, 132, 232, 86, 132, 184, 94, 132, 136, 102, 132, 88, 110, 132, 0}},
			867_100_000,
			867_300_000,
			867_500_000,
			867_700_000,
			867_900_000,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2, got3, got4, err := ParseCFList(tt.args.cflist)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseCFList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseCFList() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ParseCFList() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("ParseCFList() got2 = %v, want %v", got2, tt.want2)
			}
			if got3 != tt.want3 {
				t.Errorf("ParseCFList() got3 = %v, want %v", got3, tt.want3)
			}
			if got4 != tt.want4 {
				t.Errorf("ParseCFList() got4 = %v, want %v", got4, tt.want4)
			}
		})
	}
}

func TestRX1(t *testing.T) {
	type args struct {
		freq        uint32
		dr          uint8
		rx1droffset uint8
	}
	tests := []struct {
		name          string
		args          args
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			args{869_525_000, 0, 1},
			869_525_000,
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX1(tt.args.freq, tt.args.dr, tt.args.rx1droffset)
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX1() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX1() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRX2(t *testing.T) {
	tests := []struct {
		name          string
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			434_665_000,
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX2()
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX2() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX2() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRECEIVE_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY1(); got != tt.want {
				t.Errorf("RECEIVE_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRECEIVE_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY2(); got != tt.want {
				t.Errorf("RECEIVE_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY1(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY2(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMAX_FCNT_GAP(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			16384,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MAX_FCNT_GAP(); got != tt.want {
				t.Errorf("MAX_FCNT_GAP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_LIMIT(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			64,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_LIMIT(); got != tt.want {
				t.Errorf("ADR_ACK_LIMIT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_DELAY(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			32,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_DELAY(); got != tt.want {
				t.Errorf("ADR_ACK_DELAY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestACK_TIMEOUT(t *testing.T) {
	tests := []struct {
		name string
		want float64
	}{
		{
			"min",
			1,
		},
		{
			"max",
			3,
		},
	}
	if got := ACK_TIMEOUT(); got < tests[0].want || got > tests[1].want {
		t.Errorf("ACK_TIMEOUT() = %v, want <%v and >%v", got, tests[0].want, tests[1].want)
	}
}

func TestChannelMaskCheck(t *testing.T) {
	type args struct {
		definedchannels []byte
		enabledchannels []byte
		channelmask     [2]byte
		channelmaskctrl uint8
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		want1   []byte
		wantErr bool
	}{
		{
			"default",
			args{nil, nil, [2]byte{0xFF, 0x00}, 0},
			false,
			nil,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := ChannelMaskCheck(tt.args.definedchannels, tt.args.enabledchannels, tt.args.channelmask, tt.args.channelmaskctrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("ChannelMaskCheck() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ChannelMaskCheck() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("ChannelMaskCheck() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestMaxPayload(t *testing.T) {
	type args struct {
		fopt     bool
		repeater bool
	}
	tests := []struct {
		name string
		args args
		want [8]uint
	}{
		{
			"default",
			args{false, false},
			[8]uint{59, 59, 59, 123, 250, 250, 250, 250},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MaxPayload(tt.args.fopt, tt.args.repeater); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MaxPayload() = %v, want %v", got, tt.want)
			}
		})
	}
}
