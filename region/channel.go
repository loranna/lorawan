package region

type Channel struct {
	Frequency uint32
	MinimumDataRate uint8
	MaximumDataRate uint8
}