package v1_0_1

import (
	"fmt"
	"math"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/region/v1_0_1/AU915_928"
)

func DefaultChannels(r region.Region) []region.Channel {
	switch r {
	case region.AU915_928:
		channels := AU915_928.DefaultChannels()
		return channels
	default:
		return nil
	}
}

func DefaultTxpower(r region.Region) int{
	switch r {
	case region.AU915_928:
		return AU915_928.DefaultTxPower()
	default:
		return math.MinInt32
	}
}

func Datarate2sfbw(r region.Region, dr uint8) (radio.DataRate, error) {
	switch r {
	case region.AU915_928:
		return AU915_928.DataRate2SFBW(dr)
	default:
		return radio.DataRate{}, fmt.Errorf("unhandled region %s", r)
	}
}

func TxpowerIndexToTxpower(r region.Region,txpower byte) (int, error){
	switch r {
	case region.AU915_928:
		return AU915_928.Txpower2dBm(int(txpower))
	default:
		return 0, fmt.Errorf("unhandled region %s", r)
	}
}