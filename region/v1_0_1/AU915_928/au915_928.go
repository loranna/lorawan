package AU915_928

import (
	"fmt"
	"math/bits"
	"math/rand"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func SyncWordLoRa() byte {
	return 0x34
}

func PreambleLengthLoRaSymbol() uint {
	return 8
}

func DefaultChannels() []region.Channel {
	return []region.Channel{
		{Frequency: 915_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_900_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 917_500_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 919_100_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 920_700_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 922_300_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 923_900_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 925_500_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 927_100_000, MinimumDataRate: 6, MaximumDataRate: 6},
	}
}

func DefaultTxPower() int {
	return 20
}

var datarates [14]radio.DataRate = [14]radio.DataRate{
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 500_000},
	radio.DataRate{},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 500_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 500_000},
}

var txpowers [11]int = [11]int{
	30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10,
}

func DataRates() [14]radio.DataRate {
	return datarates
}

func DataRate2SFBW(datarate uint8) (radio.DataRate, error) {
	if !(0 <= datarate && datarate <= uint8(len(datarates)-1)) {
		return radio.DataRate{}, fmt.Errorf("illegal datarate: %d", datarate)
	}
	return datarates[datarate], nil
}

func SFBW2Datarate(mod radio.Modulation, sf uint8, bw uint32) (uint8, error) {
	for i, dr := range datarates {
		if dr.Bandwidth == bw && dr.Modulation == mod && dr.SpreadingFactor == sf {
			return uint8(i), nil
		}
	}
	return 0, fmt.Errorf("datarate is not found")
}

func Txpowers() [11]int {
	return txpowers
}

func Txpower2dBm(txpower int) (int, error) {
	if !(0 <= txpower && txpower <= (len(txpowers)-1)) {
		return 0, fmt.Errorf("illegal txpower: %d", txpower)
	}
	return txpowers[txpower], nil
}

var upfrequencies [72]uint32 = [...]uint32{
	915_200_000,
	915_400_000,
	915_600_000,
	915_800_000,
	916_000_000,
	916_200_000,
	916_400_000,
	916_600_000,
	916_800_000,
	917_000_000,
	917_200_000,
	917_400_000,
	917_600_000,
	917_800_000,
	918_000_000,
	918_200_000,
	918_400_000,
	918_600_000,
	918_800_000,
	919_000_000,
	919_200_000,
	919_400_000,
	919_600_000,
	919_800_000,
	920_000_000,
	920_200_000,
	920_400_000,
	920_600_000,
	920_800_000,
	921_000_000,
	921_200_000,
	921_400_000,
	921_600_000,
	921_800_000,
	922_000_000,
	922_200_000,
	922_400_000,
	922_600_000,
	922_800_000,
	923_000_000,
	923_200_000,
	923_400_000,
	923_600_000,
	923_800_000,
	924_000_000,
	924_200_000,
	924_400_000,
	924_600_000,
	924_800_000,
	925_000_000,
	925_200_000,
	925_400_000,
	925_600_000,
	925_800_000,
	926_000_000,
	926_200_000,
	926_400_000,
	926_600_000,
	926_800_000,
	927_000_000,
	927_200_000,
	927_400_000,
	927_600_000,
	927_800_000,
	915_900_000,
	917_500_000,
	919_100_000,
	920_700_000,
	922_300_000,
	923_900_000,
	925_500_000,
	927_100_000,
}

var downfrequecies [8]uint32 = [...]uint32{
	923_300_000,
	923_900_000,
	924_500_000,
	925_100_000,
	925_700_000,
	926_300_000,
	926_900_000,
	927_500_000,
}

func RX1(freq uint32, dr, rx1droffset uint8) (frequency uint32, datarate uint8) {
	for i, f := range upfrequencies {
		if freq == f {
			frequency = downfrequecies[i%8]
		}
	}
	if 0 <= dr && dr <= 4 {
		dr += 10
		if 8 >= dr-rx1droffset {
			datarate = 8
		} else if 13 <= dr-rx1droffset {
			datarate = 13
		} else {
			datarate = dr - rx1droffset
		}
	} else {
		if 8 >= dr-rx1droffset {
			datarate = 8
		} else if 13 <= dr-rx1droffset {
			datarate = 13
		} else {
			datarate = dr - rx1droffset
		}
	}
	return
}

func RX2() (frequency uint32, datarate uint8) {
	frequency = 923_300_000
	datarate = 8
	return
}

func RECEIVE_DELAY1() uint {
	return 1
}

func RECEIVE_DELAY2() uint {
	return 1 + RECEIVE_DELAY1()
}

func JOIN_ACCEPT_DELAY1() uint {
	return 5
}

func JOIN_ACCEPT_DELAY2() uint {
	return 6
}

func MAX_FCNT_GAP() uint {
	return 16384
}

func ADR_ACK_LIMIT() uint {
	return 64
}

func ADR_ACK_DELAY() uint {
	return 32
}

func ACK_TIMEOUT() float64 {

	return 1 + (rand.Float64() * 2)
}

func ChannelMaskCheck(definedchannels []byte, enabledchannels []byte, channelmask [2]byte, channelmaskctrl uint8) (bool, []byte, error) {
	if enabledchannels == nil {
		return false, nil, nil
	}
	switch channelmaskctrl {
	case 0:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[0] && m2 == enabledchannels[1] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 1:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[2] && m2 == enabledchannels[3] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 2:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[4] && m2 == enabledchannels[5] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 3:
		m1 := bits.Reverse8(channelmask[0])
		m2 := bits.Reverse8(channelmask[1])
		if m1 == enabledchannels[6] && m2 == enabledchannels[7] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 4:
		m1 := bits.Reverse8(channelmask[0])
		if m1 == enabledchannels[8] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	case 6:
		for i := 0; i < 8; i++ {
			if enabledchannels[i] != 0xFF {
				return false, enabledchannels, nil
			}
		}
		m1 := bits.Reverse8(channelmask[0])
		if m1 == enabledchannels[8] {
			return true, enabledchannels, nil
		}
		return false, enabledchannels, nil
	default:
		return false, nil, fmt.Errorf("invalid channel mask control: %d", channelmaskctrl)
	}
}

//M
var maxmacpayloadnofoptrepeater [14]uint = [...]uint{
	19, 61, 137, 250, 250, 0, 0, 0, 41, 117, 230, 230, 230, 230,
}

//N
var maxmacpayloadfoptrepeater [14]uint = [...]uint{
	11, 53, 129, 242, 242, 0, 0, 0, 33, 109, 222, 222, 222, 222,
}

//M
var maxmacpayloadnofoptnorepeater [14]uint = [...]uint{
	19, 61, 137, 250, 250, 0, 0, 0, 61, 137, 250, 250, 250, 250,
}

//N
var maxmacpayloadfoptnorepeater [14]uint = [...]uint{
	11, 53, 129, 242, 242, 0, 0, 0, 53, 129, 242, 242, 242, 242,
}

func MaxPayload(fopt, repeater bool) [14]uint {
	if fopt {
		if repeater {
			return maxmacpayloadfoptrepeater
		}
		return maxmacpayloadfoptnorepeater
	}
	if repeater {
		return maxmacpayloadnofoptrepeater
	}
	return maxmacpayloadnofoptnorepeater
}

func ParseCFList(cflist [16]byte) ([]region.Channel, error) {
	if cftype := cflist[15]; cftype != 0x01 {
		return nil, fmt.Errorf("cflisttype must be one but %X", cftype)
	}
	channels := []region.Channel{
		{Frequency: 915_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 916_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 917_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 918_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 919_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 920_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 921_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 922_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 923_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 924_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 925_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 926_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_000_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_200_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_400_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_600_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 927_800_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 915_900_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 917_500_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 919_100_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 920_700_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 922_300_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 923_900_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 925_500_000, MinimumDataRate: 6, MaximumDataRate: 6},
		{Frequency: 927_100_000, MinimumDataRate: 6, MaximumDataRate: 6},
	}
	for j, cfmask := range cflist {
		if j > 8 {
			break
		}
		for i := 0; i < 8; i++ {
			if (cfmask>>i)&0x01 != 0x01 {
				channels[(j*8)+i].Frequency = 0
			}
		}
	}
	return channels, nil
}
