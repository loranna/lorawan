package US902_928

import (
	"fmt"
	"math/rand"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func SyncWordLoRa() byte {
	return 0x34
}

func PreambleLengthLoRaSymbol() uint {
	return 8
}

func DefaultChannels() []region.Channel {
	return []region.Channel{
		{Frequency: 470_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 470_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 470_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 470_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 471_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 471_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{Frequency: 478_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 478_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 478_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 478_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 478_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 479_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{Frequency: 485_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 486_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 486_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 486_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 486_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 486_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 487_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 487_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 487_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 487_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 487_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 488_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 488_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 488_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 488_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 488_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 489_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
		{Frequency: 489_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
	}
}

func DefaultTxPower() int {
	return 14
}

var datarates [6]radio.DataRate = [6]radio.DataRate{
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
	radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
}

var txpowers [8]int = [8]int{
	17, 16, 14, 12, 10, 7, 5, 2,
}

func DataRates() [6]radio.DataRate {
	return datarates
}

func DataRate2SFBW(datarate uint8) (radio.DataRate, error) {
	if !(0 <= datarate && datarate <= uint8(len(datarates)-1)) {
		return radio.DataRate{}, fmt.Errorf("illegal datarate: %d", datarate)
	}
	return datarates[datarate], nil
}

func SFBW2Datarate(mod radio.Modulation, sf uint8, bw uint32) (uint8, error) {
	for i, dr := range datarates {
		if dr.Bandwidth == bw && dr.Modulation == mod && dr.SpreadingFactor == sf {
			return uint8(i), nil
		}
	}
	return 0, fmt.Errorf("datarate is not found")
}

func Txpowers() [8]int {
	return txpowers
}

func Txpower2dBm(txpower int) (int, error) {
	if !(0 <= txpower && txpower <= (len(txpowers)-1)) {
		return 0, fmt.Errorf("illegal txpower: %d", txpower)
	}
	return txpowers[txpower], nil
}

var upfrequencies [96]uint32 = [...]uint32{
	470_300_000,
	470_500_000,
	470_700_000,
	470_900_000,
	471_100_000,
	471_300_000,
	471_500_000,
	471_700_000,
	471_900_000,
	472_100_000,
	472_300_000,
	472_500_000,
	472_700_000,
	472_900_000,
	473_100_000,
	473_300_000,
	473_500_000,
	473_700_000,
	473_900_000,
	474_100_000,
	474_300_000,
	474_500_000,
	474_700_000,
	474_900_000,
	475_100_000,
	475_300_000,
	475_500_000,
	475_700_000,
	475_900_000,
	476_100_000,
	476_300_000,
	476_500_000,
	476_700_000,
	476_900_000,
	477_100_000,
	477_300_000,
	477_500_000,
	477_700_000,
	477_900_000,
	478_100_000,
	478_300_000,
	478_500_000,
	478_700_000,
	478_900_000,
	479_100_000,
	479_300_000,
	479_500_000,
	479_700_000,
	479_900_000,
	480_100_000,
	480_300_000,
	480_500_000,
	480_700_000,
	480_900_000,
	481_100_000,
	481_300_000,
	481_500_000,
	481_700_000,
	481_900_000,
	482_100_000,
	482_300_000,
	482_500_000,
	482_700_000,
	482_900_000,
	483_100_000,
	483_300_000,
	483_500_000,
	483_700_000,
	483_900_000,
	484_100_000,
	484_300_000,
	484_500_000,
	484_700_000,
	484_900_000,
	485_100_000,
	485_300_000,
	485_500_000,
	485_700_000,
	485_900_000,
	486_100_000,
	486_300_000,
	486_500_000,
	486_700_000,
	486_900_000,
	487_100_000,
	487_300_000,
	487_500_000,
	487_700_000,
	487_900_000,
	488_100_000,
	488_300_000,
	488_500_000,
	488_700_000,
	488_900_000,
	489_100_000,
	489_300_000,
}

var downfrequecies [48]uint32 = [...]uint32{
	500_300_000,
	500_500_000,
	500_700_000,
	500_900_000,
	501_100_000,
	501_300_000,
	501_500_000,
	501_700_000,
	501_900_000,
	502_100_000,
	502_300_000,
	502_500_000,
	502_700_000,
	502_900_000,
	503_100_000,
	503_300_000,
	503_500_000,
	503_700_000,
	503_900_000,
	504_100_000,
	504_300_000,
	504_500_000,
	504_700_000,
	504_900_000,
	505_100_000,
	505_300_000,
	505_500_000,
	505_700_000,
	505_900_000,
	506_100_000,
	506_300_000,
	506_500_000,
	506_700_000,
	506_900_000,
	507_100_000,
	507_300_000,
	507_500_000,
	507_700_000,
	507_900_000,
	508_100_000,
	508_300_000,
	508_500_000,
	508_700_000,
	508_900_000,
	509_100_000,
	509_300_000,
	509_500_000,
	509_700_000,
}

func RX1(freq uint32, dr, rx1droffset uint8) (frequency uint32, datarate uint8) {
	for i, f := range upfrequencies {
		if freq == f {
			frequency = downfrequecies[i%48]
		}
	}
	if rx1droffset > dr {
		datarate = 0
	} else {
		datarate = dr - rx1droffset
	}
	return
}

func RX2() (frequency uint32, datarate uint8) {
	frequency = 505_300_000
	datarate = 0
	return
}

func RECEIVE_DELAY1() uint {
	return 1
}

func RECEIVE_DELAY2() uint {
	return 1 + RECEIVE_DELAY1()
}

func JOIN_ACCEPT_DELAY1() uint {
	return 5
}

func JOIN_ACCEPT_DELAY2() uint {
	return 6
}

func MAX_FCNT_GAP() uint {
	return 16384
}

func ADR_ACK_LIMIT() uint {
	return 64
}

func ADR_ACK_DELAY() uint {
	return 32
}

func ACK_TIMEOUT() float64 {

	return 1 + (rand.Float64() * 2)
}

//M
var maxmacpayloadnofoptrepeater [6]uint = [...]uint{
	59, 59, 59, 123, 230, 230,
}

//N
var maxmacpayloadfoptrepeater [6]uint = [...]uint{
	51, 51, 51, 115, 222, 222,
}

func MaxPayload(fopt, repeater bool) [6]uint {
	if fopt {
		if repeater {
			return maxmacpayloadfoptrepeater
		}
		return [6]uint{}
	}
	if repeater {
		return maxmacpayloadnofoptrepeater
	}
	return [6]uint{}
}
