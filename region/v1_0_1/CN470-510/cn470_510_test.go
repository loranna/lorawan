package US902_928

import (
	"reflect"
	"testing"

	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

func TestSyncWordLoRa(t *testing.T) {
	tests := []struct {
		name string
		want byte
	}{
		{
			"default",
			0x34,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SyncWordLoRa(); got != tt.want {
				t.Errorf("SyncWordLoRa() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPreambleLengthLoRaSymbol(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PreambleLengthLoRaSymbol(); got != tt.want {
				t.Errorf("PreambleLengthLoRaSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultChannels(t *testing.T) {
	tests := []struct {
		name string
		want []region.Channel
	}{
		{
			"default",
			[]region.Channel{
				{Frequency: 470_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 470_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 470_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 470_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 471_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 471_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{Frequency: 478_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 478_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 478_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 478_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 478_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 479_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{},
				{Frequency: 485_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 486_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 486_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 486_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 486_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 486_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 487_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 487_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 487_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 487_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 487_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 488_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 488_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 488_500_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 488_700_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 488_900_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 489_100_000, MinimumDataRate: 0, MaximumDataRate: 5},
				{Frequency: 489_300_000, MinimumDataRate: 0, MaximumDataRate: 5},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultChannels(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DefaultChannels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDefaultTxPower(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{
		{
			"default",
			14,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DefaultTxPower(); got != tt.want {
				t.Errorf("DefaultTxPower() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRates(t *testing.T) {
	tests := []struct {
		name string
		want [6]radio.DataRate
	}{
		{
			"default",
			[6]radio.DataRate{
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 12, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 11, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 10, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 8, Bandwidth: 125_000},
				radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 7, Bandwidth: 125_000},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DataRates(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRates() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataRate2SFBW(t *testing.T) {
	type args struct {
		datarate uint8
	}
	tests := []struct {
		name    string
		args    args
		want    radio.DataRate
		wantErr bool
	}{
		{
			"default",
			args{datarate: 3},
			radio.DataRate{Modulation: radio.ModulationLoRa, SpreadingFactor: 9, Bandwidth: 125_000},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DataRate2SFBW(tt.args.datarate)
			if (err != nil) != tt.wantErr {
				t.Errorf("DataRate2SFBW() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataRate2SFBW() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpowers(t *testing.T) {
	tests := []struct {
		name string
		want [8]int
	}{
		{
			"default",
			[8]int{17, 16, 14, 12, 10, 7, 5, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Txpowers(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Txpowers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTxpower2dBm(t *testing.T) {
	type args struct {
		txpower int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"default",
			args{txpower: 4},
			10,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Txpower2dBm(tt.args.txpower)
			if (err != nil) != tt.wantErr {
				t.Errorf("Txpower2dBm() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Txpower2dBm() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRX1(t *testing.T) {
	type args struct {
		freq        uint32
		dr          uint8
		rx1droffset uint8
	}
	tests := []struct {
		name          string
		args          args
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			args{470_300_000, 0, 1},
			500_300_000,
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX1(tt.args.freq, tt.args.dr, tt.args.rx1droffset)
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX1() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX1() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRX2(t *testing.T) {
	tests := []struct {
		name          string
		wantFrequency uint32
		wantDatarate  uint8
	}{
		{
			"default",
			505_300_000,
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFrequency, gotDatarate := RX2()
			if gotFrequency != tt.wantFrequency {
				t.Errorf("RX2() gotFrequency = %v, want %v", gotFrequency, tt.wantFrequency)
			}
			if gotDatarate != tt.wantDatarate {
				t.Errorf("RX2() gotDatarate = %v, want %v", gotDatarate, tt.wantDatarate)
			}
		})
	}
}

func TestRECEIVE_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY1(); got != tt.want {
				t.Errorf("RECEIVE_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRECEIVE_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RECEIVE_DELAY2(); got != tt.want {
				t.Errorf("RECEIVE_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY1(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY1(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJOIN_ACCEPT_DELAY2(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := JOIN_ACCEPT_DELAY2(); got != tt.want {
				t.Errorf("JOIN_ACCEPT_DELAY2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMAX_FCNT_GAP(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			16384,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MAX_FCNT_GAP(); got != tt.want {
				t.Errorf("MAX_FCNT_GAP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_LIMIT(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			64,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_LIMIT(); got != tt.want {
				t.Errorf("ADR_ACK_LIMIT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestADR_ACK_DELAY(t *testing.T) {
	tests := []struct {
		name string
		want uint
	}{
		{
			"default",
			32,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ADR_ACK_DELAY(); got != tt.want {
				t.Errorf("ADR_ACK_DELAY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestACK_TIMEOUT(t *testing.T) {
	tests := []struct {
		name string
		want float64
	}{
		{
			"min",
			1,
		},
		{
			"max",
			3,
		},
	}
	if got := ACK_TIMEOUT(); got < tests[0].want || got > tests[1].want {
		t.Errorf("ACK_TIMEOUT() = %v, want <%v and >%v", got, tests[0].want, tests[1].want)
	}
}

func TestMaxPayload(t *testing.T) {
	type args struct {
		fopt     bool
		repeater bool
	}
	tests := []struct {
		name string
		args args
		want [6]uint
	}{
		{
			"default",
			args{false, false},
			[6]uint{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MaxPayload(tt.args.fopt, tt.args.repeater); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MaxPayload() = %v, want %v", got, tt.want)
			}
		})
	}
}
