package region

import "strings"

//go:generate stringer -type=Region

type Region byte

const (
	Unknown Region = iota
	EU863_870
	US902_928
	CN779_787
	EU433
	AU915_928
)

func Parse(r string) Region {
	switch strings.ToUpper(r) {
	case "EU868", "EU863_870":
		return EU863_870
	case "US915", "US902_928":
		return US902_928
	case "AU915", "AU915_928":
		return AU915_928
	default:
		return Unknown
	}
}
