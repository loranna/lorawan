# bash completion for loranna                              -*- shell-script -*-

__loranna_debug()
{
    if [[ -n ${BASH_COMP_DEBUG_FILE} ]]; then
        echo "$*" >> "${BASH_COMP_DEBUG_FILE}"
    fi
}

# Homebrew on Macs have version 1.3 of bash-completion which doesn't include
# _init_completion. This is a very minimal version of that function.
__loranna_init_completion()
{
    COMPREPLY=()
    _get_comp_words_by_ref "$@" cur prev words cword
}

__loranna_index_of_word()
{
    local w word=$1
    shift
    index=0
    for w in "$@"; do
        [[ $w = "$word" ]] && return
        index=$((index+1))
    done
    index=-1
}

__loranna_contains_word()
{
    local w word=$1; shift
    for w in "$@"; do
        [[ $w = "$word" ]] && return
    done
    return 1
}

__loranna_handle_reply()
{
    __loranna_debug "${FUNCNAME[0]}"
    case $cur in
        -*)
            if [[ $(type -t compopt) = "builtin" ]]; then
                compopt -o nospace
            fi
            local allflags
            if [ ${#must_have_one_flag[@]} -ne 0 ]; then
                allflags=("${must_have_one_flag[@]}")
            else
                allflags=("${flags[*]} ${two_word_flags[*]}")
            fi
            COMPREPLY=( $(compgen -W "${allflags[*]}" -- "$cur") )
            if [[ $(type -t compopt) = "builtin" ]]; then
                [[ "${COMPREPLY[0]}" == *= ]] || compopt +o nospace
            fi

            # complete after --flag=abc
            if [[ $cur == *=* ]]; then
                if [[ $(type -t compopt) = "builtin" ]]; then
                    compopt +o nospace
                fi

                local index flag
                flag="${cur%=*}"
                __loranna_index_of_word "${flag}" "${flags_with_completion[@]}"
                COMPREPLY=()
                if [[ ${index} -ge 0 ]]; then
                    PREFIX=""
                    cur="${cur#*=}"
                    ${flags_completion[${index}]}
                    if [ -n "${ZSH_VERSION}" ]; then
                        # zsh completion needs --flag= prefix
                        eval "COMPREPLY=( \"\${COMPREPLY[@]/#/${flag}=}\" )"
                    fi
                fi
            fi
            return 0;
            ;;
    esac

    # check if we are handling a flag with special work handling
    local index
    __loranna_index_of_word "${prev}" "${flags_with_completion[@]}"
    if [[ ${index} -ge 0 ]]; then
        ${flags_completion[${index}]}
        return
    fi

    # we are parsing a flag and don't have a special handler, no completion
    if [[ ${cur} != "${words[cword]}" ]]; then
        return
    fi

    local completions
    completions=("${commands[@]}")
    if [[ ${#must_have_one_noun[@]} -ne 0 ]]; then
        completions=("${must_have_one_noun[@]}")
    fi
    if [[ ${#must_have_one_flag[@]} -ne 0 ]]; then
        completions+=("${must_have_one_flag[@]}")
    fi
    COMPREPLY=( $(compgen -W "${completions[*]}" -- "$cur") )

    if [[ ${#COMPREPLY[@]} -eq 0 && ${#noun_aliases[@]} -gt 0 && ${#must_have_one_noun[@]} -ne 0 ]]; then
        COMPREPLY=( $(compgen -W "${noun_aliases[*]}" -- "$cur") )
    fi

    if [[ ${#COMPREPLY[@]} -eq 0 ]]; then
		if declare -F __loranna_custom_func >/dev/null; then
			# try command name qualified custom func
			__loranna_custom_func
		else
			# otherwise fall back to unqualified for compatibility
			declare -F __custom_func >/dev/null && __custom_func
		fi
    fi

    # available in bash-completion >= 2, not always present on macOS
    if declare -F __ltrim_colon_completions >/dev/null; then
        __ltrim_colon_completions "$cur"
    fi

    # If there is only 1 completion and it is a flag with an = it will be completed
    # but we don't want a space after the =
    if [[ "${#COMPREPLY[@]}" -eq "1" ]] && [[ $(type -t compopt) = "builtin" ]] && [[ "${COMPREPLY[0]}" == --*= ]]; then
       compopt -o nospace
    fi
}

# The arguments should be in the form "ext1|ext2|extn"
__loranna_handle_filename_extension_flag()
{
    local ext="$1"
    _filedir "@(${ext})"
}

__loranna_handle_subdirs_in_dir_flag()
{
    local dir="$1"
    pushd "${dir}" >/dev/null 2>&1 && _filedir -d && popd >/dev/null 2>&1
}

__loranna_handle_flag()
{
    __loranna_debug "${FUNCNAME[0]}: c is $c words[c] is ${words[c]}"

    # if a command required a flag, and we found it, unset must_have_one_flag()
    local flagname=${words[c]}
    local flagvalue
    # if the word contained an =
    if [[ ${words[c]} == *"="* ]]; then
        flagvalue=${flagname#*=} # take in as flagvalue after the =
        flagname=${flagname%=*} # strip everything after the =
        flagname="${flagname}=" # but put the = back
    fi
    __loranna_debug "${FUNCNAME[0]}: looking for ${flagname}"
    if __loranna_contains_word "${flagname}" "${must_have_one_flag[@]}"; then
        must_have_one_flag=()
    fi

    # if you set a flag which only applies to this command, don't show subcommands
    if __loranna_contains_word "${flagname}" "${local_nonpersistent_flags[@]}"; then
      commands=()
    fi

    # keep flag value with flagname as flaghash
    # flaghash variable is an associative array which is only supported in bash > 3.
    if [[ -z "${BASH_VERSION}" || "${BASH_VERSINFO[0]}" -gt 3 ]]; then
        if [ -n "${flagvalue}" ] ; then
            flaghash[${flagname}]=${flagvalue}
        elif [ -n "${words[ $((c+1)) ]}" ] ; then
            flaghash[${flagname}]=${words[ $((c+1)) ]}
        else
            flaghash[${flagname}]="true" # pad "true" for bool flag
        fi
    fi

    # skip the argument to a two word flag
    if [[ ${words[c]} != *"="* ]] && __loranna_contains_word "${words[c]}" "${two_word_flags[@]}"; then
			  __loranna_debug "${FUNCNAME[0]}: found a flag ${words[c]}, skip the next argument"
        c=$((c+1))
        # if we are looking for a flags value, don't show commands
        if [[ $c -eq $cword ]]; then
            commands=()
        fi
    fi

    c=$((c+1))

}

__loranna_handle_noun()
{
    __loranna_debug "${FUNCNAME[0]}: c is $c words[c] is ${words[c]}"

    if __loranna_contains_word "${words[c]}" "${must_have_one_noun[@]}"; then
        must_have_one_noun=()
    elif __loranna_contains_word "${words[c]}" "${noun_aliases[@]}"; then
        must_have_one_noun=()
    fi

    nouns+=("${words[c]}")
    c=$((c+1))
}

__loranna_handle_command()
{
    __loranna_debug "${FUNCNAME[0]}: c is $c words[c] is ${words[c]}"

    local next_command
    if [[ -n ${last_command} ]]; then
        next_command="_${last_command}_${words[c]//:/__}"
    else
        if [[ $c -eq 0 ]]; then
            next_command="_loranna_root_command"
        else
            next_command="_${words[c]//:/__}"
        fi
    fi
    c=$((c+1))
    __loranna_debug "${FUNCNAME[0]}: looking for ${next_command}"
    declare -F "$next_command" >/dev/null && $next_command
}

__loranna_handle_word()
{
    if [[ $c -ge $cword ]]; then
        __loranna_handle_reply
        return
    fi
    __loranna_debug "${FUNCNAME[0]}: c is $c words[c] is ${words[c]}"
    if [[ "${words[c]}" == -* ]]; then
        __loranna_handle_flag
    elif __loranna_contains_word "${words[c]}" "${commands[@]}"; then
        __loranna_handle_command
    elif [[ $c -eq 0 ]]; then
        __loranna_handle_command
    elif __loranna_contains_word "${words[c]}" "${command_aliases[@]}"; then
        # aliashash variable is an associative array which is only supported in bash > 3.
        if [[ -z "${BASH_VERSION}" || "${BASH_VERSINFO[0]}" -gt 3 ]]; then
            words[c]=${aliashash[${words[c]}]}
            __loranna_handle_command
        else
            __loranna_handle_noun
        fi
    else
        __loranna_handle_noun
    fi
    __loranna_handle_word
}

_loranna_create_cflist()
{
    last_command="loranna_create_cflist"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_join_accept()
{
    last_command="loranna_create_join_accept"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--cflist=")
    two_word_flags+=("--cflist")
    local_nonpersistent_flags+=("--cflist=")
    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_join_request()
{
    last_command="loranna_create_join_request"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_join()
{
    last_command="loranna_create_join"

    command_aliases=()

    commands=()
    commands+=("accept")
    commands+=("request")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_keys()
{
    last_command="loranna_create_keys"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_mac_newchannelans()
{
    last_command="loranna_create_mac_newchannelans"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")
    flags+=("--split")
    flags+=("-s")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_mac_newchannelreq()
{
    last_command="loranna_create_mac_newchannelreq"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")
    flags+=("--split")
    flags+=("-s")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_mac()
{
    last_command="loranna_create_mac"

    command_aliases=()

    commands=()
    commands+=("newchannelans")
    commands+=("newchannelreq")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--split")
    flags+=("-s")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_message_downlink()
{
    last_command="loranna_create_message_downlink"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--appskey=")
    two_word_flags+=("--appskey")
    flags+=("--confirmed")
    flags+=("--fopts=")
    two_word_flags+=("--fopts")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--msbminus1ofcnt=")
    two_word_flags+=("--msbminus1ofcnt")
    flags+=("--msbofcnt=")
    two_word_flags+=("--msbofcnt")
    flags+=("--nwkskey=")
    two_word_flags+=("--nwkskey")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_message_uplink()
{
    last_command="loranna_create_message_uplink"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--appskey=")
    two_word_flags+=("--appskey")
    flags+=("--confirmed")
    flags+=("--fopts=")
    two_word_flags+=("--fopts")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--msbminus1ofcnt=")
    two_word_flags+=("--msbminus1ofcnt")
    flags+=("--msbofcnt=")
    two_word_flags+=("--msbofcnt")
    flags+=("--nwkskey=")
    two_word_flags+=("--nwkskey")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create_message()
{
    last_command="loranna_create_message"

    command_aliases=()

    commands=()
    commands+=("downlink")
    commands+=("uplink")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--appskey=")
    two_word_flags+=("--appskey")
    flags+=("--confirmed")
    flags+=("--fopts=")
    two_word_flags+=("--fopts")
    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--msbminus1ofcnt=")
    two_word_flags+=("--msbminus1ofcnt")
    flags+=("--msbofcnt=")
    two_word_flags+=("--msbofcnt")
    flags+=("--nwkskey=")
    two_word_flags+=("--nwkskey")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_create()
{
    last_command="loranna_create"

    command_aliases=()

    commands=()
    commands+=("cflist")
    commands+=("join")
    commands+=("keys")
    commands+=("mac")
    commands+=("message")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse_downlink()
{
    last_command="loranna_parse_downlink"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--base64")
    flags+=("-b")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse_join_accept()
{
    last_command="loranna_parse_join_accept"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--base64")
    flags+=("-b")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse_join_request()
{
    last_command="loranna_parse_join_request"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--base64")
    flags+=("-b")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse_join()
{
    last_command="loranna_parse_join"

    command_aliases=()

    commands=()
    commands+=("accept")
    commands+=("request")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--base64")
    flags+=("-b")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse_uplink()
{
    last_command="loranna_parse_uplink"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--base64")
    flags+=("-b")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_parse()
{
    last_command="loranna_parse"

    command_aliases=()

    commands=()
    commands+=("downlink")
    commands+=("join")
    commands+=("uplink")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--base64")
    flags+=("-b")
    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_region_datarate()
{
    last_command="loranna_region_datarate"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_region_txpower()
{
    last_command="loranna_region_txpower"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_region()
{
    last_command="loranna_region"

    command_aliases=()

    commands=()
    commands+=("datarate")
    commands+=("txpower")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_version()
{
    last_command="loranna_version"

    command_aliases=()

    commands=()

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

_loranna_root_command()
{
    last_command="loranna"

    command_aliases=()

    commands=()
    commands+=("create")
    commands+=("parse")
    commands+=("region")
    commands+=("version")

    flags=()
    two_word_flags=()
    local_nonpersistent_flags=()
    flags_with_completion=()
    flags_completion=()

    flags+=("--help")
    flags+=("-h")
    local_nonpersistent_flags+=("--help")
    flags+=("--lorawan=")
    two_word_flags+=("--lorawan")
    flags+=("--region=")
    two_word_flags+=("--region")
    flags+=("--regionversion=")
    two_word_flags+=("--regionversion")

    must_have_one_flag=()
    must_have_one_noun=()
    noun_aliases=()
}

__start_loranna()
{
    local cur prev words cword
    declare -A flaghash 2>/dev/null || :
    declare -A aliashash 2>/dev/null || :
    if declare -F _init_completion >/dev/null 2>&1; then
        _init_completion -s || return
    else
        __loranna_init_completion -n "=" || return
    fi

    local c=0
    local flags=()
    local two_word_flags=()
    local local_nonpersistent_flags=()
    local flags_with_completion=()
    local flags_completion=()
    local commands=("loranna")
    local must_have_one_flag=()
    local must_have_one_noun=()
    local last_command
    local nouns=()

    __loranna_handle_word
}

if [[ $(type -t compopt) = "builtin" ]]; then
    complete -o default -F __start_loranna loranna
else
    complete -o default -o nospace -F __start_loranna loranna
fi

# ex: ts=4 sw=4 et filetype=sh
