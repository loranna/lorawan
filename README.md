Loranna is both a library and a CLI tool to create and parse messages according to the LoRaWAN specification governed by the [LoRa Alliance](https://lora-alliance.org/).

Available commands can be found [here](available_commands.md).