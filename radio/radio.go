package radio

//go:generate stringer -type=Modulation
//go:generate stringer -type=CodeRate

type Modulation byte

const (
	ModulationUndefined Modulation = iota
	ModulationLoRa
	ModulationFSK
)

type CodeRate uint32

const (
	CodeRateUndefined CodeRate = iota
	CodeRate4_5
	CodeRate4_6
	CodeRate4_7
	CodeRate4_8
)

type DataRate struct {
	Modulation      Modulation
	SpreadingFactor uint8
	Bandwidth       uint32
}
